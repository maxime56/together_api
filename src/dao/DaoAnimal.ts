import { Animal } from "../models/Animal";
import Database from "../models/Database"

export class AnimalDao extends Database {
    public sqlToAnimal(res): Animal {
        if (res) {
            return new Animal(
                res.name,
                res.description,
                res.age,
                res.gender,
                res.birthDate,
                res.isActive,
                res.documentRef,
                res.Center_id,
                res.Species_id,
                res.Race_id,
                res.id
            );
        }
    }
    /**
     * @param {Number} id 
     * @returns {Animal}
     */
    public async selectOne(id: number): Promise<Animal> {
        const query = `CALL select_animal(${id})`;
        const res = await this.request(query);
        return this.sqlToAnimal(JSON.parse(JSON.stringify(res[0]))[0]);
    }
    /**
     * @param {Number} centerId 
     * @param {Number} limit 
     * @param {Number} offset 
     * @returns {Animal} Should return all animals from one center
     */
    public async getAnimalsByCenter(centerId: number, limit: number, offset: number) {
        const query = `CALL select_animals(${centerId}, ${limit}, ${offset})`;
        const res = await this.request(query);
        const result = [];
        res[0].forEach(element => {
            let i = this.sqlToAnimal(JSON.parse(JSON.stringify(element)));
            result.push(i);
        });
        return result;
    }
    /**
     * @param {Animal} animal 
     * @returns {Animal}
     */
    public async create(animal: Animal): Promise<Animal> {
        let y = animal.getBirthDate().toLocaleString("FR").slice(6, 10);
        let m = animal.getBirthDate().toLocaleString("FR").slice(3, 5);
        let d = animal.getBirthDate().toLocaleString("FR").slice(0, 2);
        let query = `INSERT INTO Animal (name, description, age, gender, birthDate, isActive, documentRef,Center_id, Race_id, Species_id) VALUES
         ("${animal.getName()}", "${animal.getDescription()}", ${animal.getAge()}, "${animal.getGender()}", ("${y}-${m}-${d}"),
         ${animal.getIsActive()}, "${animal.getDocumentRef()}", ${animal.getCenterId()}, ${animal.getRace()}, ${animal.getSpecies()});`;
        let res = await this.request(query);
        return this.selectOne(res.insertId);
    }
    /**
     * @param {Animal} animal 
     * @returns {Animal}
     */
    public async edit(animal: Animal): Promise<Animal> {
        let y = animal.getBirthDate().toLocaleString("FR").slice(6, 10);
        let m = animal.getBirthDate().toLocaleString("FR").slice(3, 5);
        let d = animal.getBirthDate().toLocaleString("FR").slice(0, 2);
        let query = `CALL update_animal("${animal.getName()}", "${animal.getDescription()}", "${animal.getAge()}","${animal.getGender()}", 
        ("${y}-${m}-${d}"), ${animal.getIsActive()}, "${animal.getDocumentRef()}", ${animal.getCenterId()}, 
        ${animal.getRace()}, ${animal.getSpecies()}, ${animal.getId()})`;
        await this.request(query);
        return this.selectOne(animal.getId());
    }
    /**
     * @param {Animal} animalId 
     * @returns {Boolean}
     */
    public async delete(animalId: number) {
        const query = `CALL delete_animal(${animalId})`;
        const res = await this.request(query);
        const deleteSuccess = res.affectedRows;
        return deleteSuccess === 1 ? true : false;
    }
    /**
    * @param {number} centerId 
    * @returns {Boolean} and also removes vaccines from the animals in the centres
    */
    public async deleteAllAnimalsInAnCenter(centerId: number) {
        const query = `CALL delete_center_animals(${centerId})`;
        const res = await this.request(query);
        const deleteSuccess = res.affectedRows;
        return deleteSuccess > 0 ? true : false;
    }
}
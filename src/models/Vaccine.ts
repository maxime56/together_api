export class Vaccine {
    private name:string;
    private id:number;
    public constructor (name:string, id?:number) {
        this.name = name;
        this.id = id;
    }
    public getName(): string {
        return this.name;
    }
    public setName(name:string):void{
        this.name = name;
    }
    public getId():number {
        return this.id;
    }
}
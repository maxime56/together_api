import { assert, expect } from "chai";
import { User } from "../../src/models/User";


describe('User Models Testing Getter & Setter', () => {
    let user: User;
    // ces dates là me permettent de faire passer mes tests unitaire, le fait de faire un new bloque mes tests car l'instance n'est pas la même
    const newDate = new Date("10/10/2020");
    const oldDate = new Date("10/10/2100");
    beforeEach(() => {
        user = new User("test@gmail.com", "test", "testing", "33695841481", newDate, "1234", 1, 1);
    });
    describe('Getter Setter Email', () => {

        it('getEmail', () => {
            expect(user.getEmail()).to.be.equal("test@gmail.com");
        });
        it("setEmail", () => {
            expect(user.setEmail("test@gmail.comd")).to.not.be.equal("test@gmail.com");
        });
    });
    describe('Getter Setter FirstName', () => {
        it('getFirstName', () => {
            expect(user.getFirstName()).to.be.equal("test");
        });
        it("setFirstName", () => {
            expect(user.setFirstName("d")).to.not.be.equal("test");
        });
    });
    describe('Getter Setter LastName', () => {
        it('getLastName', () => {
            expect(user.getLastName()).to.be.equal("testing");
        });
        it("setLastName", () => {
            expect(user.setLastName("testingd")).to.not.be.equal("testing");
        });
    });
    describe('Getter Setter PhoneNumber', () => {
        it('getPhoneNumber', () => {
            expect(user.getPhoneNumber()).to.be.equal("33695841481");
        });
        it("setPhoneNumber", () => {
            expect(user.setPhoneNumber("33695841482")).to.not.be.equal("33695841482");
        });
    });
    describe('Getter Setter Password', () => {
        it('getPassword', () => {
            expect(user.getPassword()).to.be.equal("1234");
        });
        it("setPassword", () => {
            expect(user.setPassword("12345")).to.not.be.equal("1234");
        });
    });
    describe('Getter Setter AddressId', () => {
        it('getAddressId', () => {
            expect(user.getAddressId()).to.be.equal(1);
        });
        it("setAddressId", () => {
            user.setAddressId(2)
            expect(user.getAddressId()).to.be.equal(2);
        });
    });
    describe('Getter Setter LastConnexion', () => {
        it('getLastConnexion', () => {
            expect(user.getLastConnexion()).to.be.equal(newDate);
        });
        it("setLastConnexion", () => {
            user.setLastConnexion(oldDate)
            expect(user.getLastConnexion()).to.be.equal(oldDate)
        });
    });
    describe('Getter Id', () => {
        it('getId', () => {
            expect(user.getId()).to.be.equal(1);
        });
    });

});


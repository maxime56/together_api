import { expect } from "chai";
import { Center } from "../../src/models/Center";
import { CenterDao } from "../../src/dao/DaoCenter";
import { DaoHandler } from "../../src/dao/DaoHandler";

describe("Center DAO", () => {
    let deleteId: number;
    // Test 1
    describe("SQL response to center", () => {
        let centerDao = new CenterDao();
        // Données fictives renvoyés par la base de données.
        const res = {
            name: "test Name",
            email: "testmail@gmail.com",
            phoneNumber: "33625548459",
            isActive: true,
            schedule: "ddddd",
            Address_id: 1,
            Country_id:1,
            id: 1,
        };
        const center: Center = centerDao.sqlToCenter(res);
        it("Id", () => {
            expect(center.getId()).to.be.equal(1);
        });
        it("Name", () => {
            expect(center.getName()).to.be.equal("test Name");
        });
        it("Email", () => {
            expect(center.getEmail()).to.be.equal("testmail@gmail.com");
        });
        it("PhoneNumber", () => {
            expect(center.getPhoneNumber()).to.be.equal("33625548459");
        });
        it("isActive", () => {
            expect(center.getIsActive()).to.be.equal(true);
        });
        it("CountryId", () => {
            expect(center.getCountryId()).to.be.equal(1);
        });
        it("AddressId", () => {
            expect(center.getAddressId()).to.be.equal(1);
        });
    });

    // Test de l'intégration du DAO avec la base de données.
    describe("DAO integration", () => {
        it("New", async () => {
            const newCenter: Center = new Center("new", "mailedit@gmail.com", "625620152", true, "dddd", 1, 1);
            const centerDao = new CenterDao();
            const center = await centerDao.create(newCenter);
            // récupération de l'id qui vient d'être créé pour le supprimer
            deleteId = center.getId();
            expect(center).to.be.instanceOf(Center);
            expect(center.getName()).to.be.equal("new");
            expect(center.getEmail()).to.be.equal("mailedit@gmail.com");
            expect(center.getPhoneNumber()).to.be.equal("625620152");
            expect(center.getSchedule()).to.be.equal("dddd");
            expect(center.getIsActive()).to.be.equal(1);
            expect(center.getAddressId()).to.be.equal(1);
            expect(center.getCountryId()).to.be.equal(1);
        });
        it("Get", async () => {
            const centerDao = new CenterDao();
            const center = await centerDao.selectOne(deleteId);
            expect(center).to.be.instanceOf(Center);
            expect(center.getName()).to.be.equal("new");
            expect(center.getEmail()).to.be.equal("mailedit@gmail.com");
            expect(center.getPhoneNumber()).to.be.equal("625620152");
            expect(center.getSchedule()).to.be.equal("dddd");
            expect(center.getIsActive()).to.be.equal(1);
            expect(center.getAddressId()).to.be.equal(1);
            expect(center.getCountryId()).to.be.equal(1);
        });
        it("GetAll", async () => {
            const centerDao = new CenterDao();
            const center = await centerDao.selectAll(5,0);
            expect(center[0]).to.be.instanceOf(Center);
        });
        it("Edit", async () => {
            const editedCenter: Center = new Center("edit", "mailedit@gmail.com", "5555555", false, "iiiiii", 1, 3, deleteId);
            const centerDao = new CenterDao();
            const center = await centerDao.edit(editedCenter);
            expect(center).to.be.instanceOf(Center);
            expect(center.getName()).to.be.equal("edit");
            expect(center.getEmail()).to.be.equal("mailedit@gmail.com");
            expect(center.getPhoneNumber()).to.be.equal("5555555");
            expect(center.getAddressId()).to.be.equal(1);
            expect(center.getSchedule()).to.be.equal("iiiiii");
            expect(center.getIsActive()).to.be.equal(0);
            expect(center.getCountryId()).to.be.equal(3);
        });
        it("Delete", async () => {
            const result = await new CenterDao().delete(4);
            expect(result).to.be.equal(true);
        });
        it("linkAddressId", async () => {
            const centerDao = new CenterDao();
            const deleteSuccess = await centerDao.linkAddressId(15, 5);
            expect(deleteSuccess).to.be.true;
        });
    });
});

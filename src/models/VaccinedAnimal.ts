export class VaccinedAnimal {
    private Vaccine_id: number;
    private Animal_id:number;
    private vaccinedDate: Date;
    private nextVaccination: Date;
    public constructor (Vaccine_id: number, Animal_id:number, vaccinedDate: Date, nextVaccination: Date) {
        this.Vaccine_id = Vaccine_id;
        this.Animal_id = Animal_id;
        this.vaccinedDate = vaccinedDate;
        this.nextVaccination = nextVaccination;
    }
    public getVaccineId(): number {
        return this.Vaccine_id;
    }
    public setVaccineId(Vaccine_id:number):void{
        this.Vaccine_id = Vaccine_id;
    }
    public getAnimalId(): number {
        return this.Animal_id;
    }
    public setAnimalId(Animal_id:number):void{
        this.Animal_id = Animal_id;
    }
    public getVaccinedDate(): Date {
        return this.vaccinedDate;
    }
    public setVaccinedDate(vaccinedDate:Date):void{
        this.vaccinedDate = vaccinedDate;
    }
    public getNextVaccination(): Date {
        return this.nextVaccination;
    }
    public setNextVaccination(nextVaccination:Date):void{
        this.nextVaccination = nextVaccination;
    }
}
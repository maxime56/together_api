import { createCenterAddress, createUserAddress, deleting, updateCenterAddress, updateUserAddress } from "../controller/AddressController";

const express = require("express");
const router = express.Router();
const checkToken = require("../middleware/tchekToken");
const centerActionHandler = require("../middleware/centerActionHandler");
const verifEmail = require("../middleware/verifEmail");


router.route("/createUserAddress/").post(verifEmail, checkToken, createUserAddress); // user login
router.route("/delete/").post(checkToken, deleting); // user login
router.route("/updateUserAddress/").post(verifEmail, checkToken, updateUserAddress); // user login
router.route("/createCenterAddress/").post(centerActionHandler, checkToken, createCenterAddress); // user login with a high role
router.route("/updateCenterAddress/").post(centerActionHandler, checkToken, updateCenterAddress); // user login with a high role

module.exports = router;
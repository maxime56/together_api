import { expect } from "chai";
import { Address } from "../../src/models/Address";
import { User } from "../../src/models/User"
import { Picture } from '../../src/models/Picture';
import { Adoption } from '../../src/models/Adoption';
import { UserManager } from '../../src/manager/UserManager';
import { UserDao } from "../../src/dao/DaoUser";
import { Candidat } from "../../src/models/Candidat";

describe("User Manager", () => {
    const logic = new UserManager();
    const userDao = new UserDao();

    describe("CreateAnUser", () => {
        it("should return an object with an user", async () => {

            let user = new User("frlomTesting", "userManager", "oui", "çamarche", new Date(Date.now()), "c'estsecret", null);
            let result = await logic.createAnUser(user);
            expect(result.user).to.be.instanceOf(User);
            expect(result.user.getEmail()).to.be.equal("frlomTesting");
            expect(result.user.getFirstName()).to.be.equal("userManager");
            expect(result.user.getLastName()).to.be.equal("oui");
            expect(result.user.getPhoneNumber()).to.be.equal("çamarche");

            await userDao.delete(result.user.getId());
        });
    });
    describe("UpdateAnUser", () => {
        it("should return an object with an user an address and an profil picture", async () => {
            // les infos créé pour être modifié
            let user = new User("fromTesting", "userManager", "oui", "çamarche", new Date(Date.now()), "c'estsecret", null);

            // les modifications
            let nUser = new User("edit", "th", "aze", "azeaze", new Date(Date.now()), "c'estsecret", null, 6);

            let nResult = await logic.updateAnUser(nUser);

            expect(nResult.user).to.be.instanceOf(User);
            expect(nResult.user.getEmail()).to.be.equal("edit");
            expect(nResult.user.getFirstName()).to.be.equal("th");
            expect(nResult.user.getLastName()).to.be.equal("aze");
            expect(nResult.user.getPhoneNumber()).to.be.equal("azeaze");
        });
    });
    describe("getAnUser", () => {
        it("should return a user with an address and a profil picture", async () => {
            const result = await logic.getAnUser(1);
            expect(result.user).to.be.instanceOf(User);
            expect(result.picture[0]).to.be.instanceOf(Picture);
            expect(result.address).to.be.instanceOf(Address);
            expect(result.adoption[0]).to.be.instanceOf(Adoption);
        });
        it("should return a user with a address and without profil picture", async () => {
            const result = await logic.getAnUser(7);
            expect(result.user).to.be.instanceOf(User);
            expect(result.picture[0]).to.be.equal(undefined);
            expect(result.address).to.be.instanceOf(Address);
            expect(result.adoption[0]).to.be.equal(undefined);
        });
        it("should return an user without an address, profil picture and adoption", async () => {
            const result = await logic.getAnUser(8);
            expect(result.user).to.be.instanceOf(User);
            expect(result.picture[0]).to.be.equal(undefined);
            expect(result.address).to.be.equal(null);
            expect(result.adoption[0]).to.be.equal(undefined);
        });
        it("shouldn't return an user because he doesn't exist", async () => {
            const result = await logic.getAnUser(5000);
            expect(result.user).to.be.equal(undefined);
            expect(result.picture).to.be.equal(undefined);
            expect(result.address).to.be.equal(undefined);
            expect(result.adoption).to.be.equal(undefined);
        });
    });
    describe("deleteAnUser", () => {
        it("should delete an user with an address and an profil picture", async () => {
            const result = await logic.deleteAnUser(new User("dériazeaeazemiuus", "ddd", "dddd", "ddd", new Date("2021-08-24"), "$2b$10$wV0KAcr7/PpSP0jsUHTWPuh5IE5pRZd8AOO35fiUk2PznVeBwmXla", 7, 6));
            expect(result).to.be.equal(true);
        });
    });
    describe("deleteAnUserAddress", () => {
        it("should delete an user address", async () => {
            const result = await logic.deleteAnUserAddress(2, 7);
            expect(result).to.be.equal(true);
        });
        it("shouldn't delete an user an address because this address don't belong to the user", async () => {
            const result = await logic.deleteAnUserAddress(1, 3);
            expect(result).to.be.equal(false);
        });
        it("shouldn't delete an user address because this address don't exist", async () => {
            const result = await logic.deleteAnUserAddress(80000, 3);
            expect(result).to.be.equal(false);
        });
        it("shouldn't delete an user address because this user don't exist", async () => {
            const result = await logic.deleteAnUserAddress(1, 800000);
            expect(result).to.be.equal(false);
        });
    });
    describe("createAnUserAddress", () => {
        it("should create a address linked to a user", async () => {
            const result = await logic.createAnUserAddress(new Address("createAnUserAddress", "oui", "non", 1), 9);
            const user = await userDao.selectOne(9);
            expect(result.Address.getPostalCode()).to.be.equal("createAnUserAddress");
            expect(result.Address.getStreetName()).to.be.equal("oui");
            expect(result.Address.getCity()).to.be.equal("non");
            expect(result.Address.getCountryId()).to.be.equal(1);
            expect(user.getAddressId()).to.be.equal(result.Address.getId());;
        });
        it("shouldn't create a address linked to a user because the user doesn't exist", async () => {
            const result = await logic.createAnUserAddress(new Address("shouldn't work", "oui", "non", 1), 5500);
            expect(result.Address).to.be.equal(undefined);
        });
    });
    describe("updateAnUserAddress", () => {
        it("should update a address linked to a user", async () => {
            const result = await logic.updateAnUserAddress(new Address("updateAnUserAddress", "non", "oui", 1, 19), 9);
            const user = await userDao.selectOne(9);
            expect(result.Address.getPostalCode()).to.be.equal("updateAnUserAddress");
            expect(result.Address.getStreetName()).to.be.equal("non");
            expect(result.Address.getCity()).to.be.equal("oui");
            expect(result.Address.getCountryId()).to.be.equal(1);
            expect(user.getAddressId()).to.be.equal(result.Address.getId());;
        });
        it("shouldn't update a address linked to a user because the user doesn't exist", async () => {
            const result = await logic.updateAnUserAddress(new Address("shouldn't update", "aaaee", "eeee", 1, 18), 5500);
            expect(result.Address).to.be.equal(undefined);
        });
        it("shouldn't update a address linked to a user because the address doesn't exist", async () => {
            const result = await logic.updateAnUserAddress(new Address("shouldn't work", "oui", "non", 1, 500), 1);
            expect(result.Address).to.be.equal(undefined);
        });
    });
    describe("applyForAnOffer", () => {
        it("should create a candidat", async () => {
            const result = await logic.applyForAnOffer(new Candidat(8, 1, 1, new Date(Date.now()), false, true), 8);
            expect(result.Offer.getUserId()).to.be.equal(8);
            expect(result.Offer.getOfferId()).to.be.equal(1);
            expect(result.Offer.getOfferCenterId()).to.be.equal(1);
            expect(result.Offer.getIsActive()).to.be.equal(1);
            expect(result.Offer.getIsAccepted()).to.be.equal(0);
        });
        it("shouldn't create a candidat because is not the same user in the candidat and the UserId", async () => {
            const result = await logic.applyForAnOffer(new Candidat(8, 1, 1, new Date(Date.now()), false, true), 3);
            expect(result.Offer).to.be.equal(undefined);
        });
        it("should create a candidat with the validation in false & the activation in true by default", async () => {
            const result = await logic.applyForAnOffer(new Candidat(9, 1, 1, new Date(Date.now()), true, false), 9);
            expect(result.Offer.getUserId()).to.be.equal(9);
            expect(result.Offer.getOfferId()).to.be.equal(1);
            expect(result.Offer.getOfferCenterId()).to.be.equal(1);
            expect(result.Offer.getIsActive()).to.be.equal(1);
            expect(result.Offer.getIsAccepted()).to.be.equal(0);
        });
    });
    describe("removeAppliedOffer", () => {
        it("should delete a candidat", async () => {
            const result = await logic.removeAppliedOffer(8, 1);
            expect(result).to.be.equal(true);
        });
        it("shouldn't delete a candidat because it don't exist", async () => {
            const result = await logic.removeAppliedOffer(80000, 1);
            expect(result).to.be.equal(false);
        });
    });
});

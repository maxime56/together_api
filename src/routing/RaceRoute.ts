import { create, deleteLinkedCenterRace, deleting, getAllRaces, linkCenterRace, update } from "../controller/RaceController";
const express = require("express");
const router = express.Router();
const checkToken = require("../middleware/tchekToken");
const centerActionHandler = require("../middleware/centerActionHandler");

router.route("/create/").post(centerActionHandler, checkToken, create); // admin
router.route("/linkCenterRace/").post(centerActionHandler, checkToken, linkCenterRace); // admin
router.route("/deleteLinkedCenterRace/").delete(centerActionHandler, checkToken, deleteLinkedCenterRace); // admin
router.route("/delete/").delete(centerActionHandler, checkToken, deleting); // admin
router.route("/update/").post(centerActionHandler, checkToken, update); // admin
router.route("/getRaces/").get(centerActionHandler, checkToken, getAllRaces); // admin

module.exports = router;
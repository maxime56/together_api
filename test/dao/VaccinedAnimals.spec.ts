import { expect } from "chai";
import { VaccinedAnimal } from "../../src/models/VaccinedAnimal";
import { VaccinedAnimalDao } from "../../src/dao/DaoVaccinedAnimals";

describe("VaccinedAnimals DAO", () => {
    let VaccineId: number;
    let AnimalId: number;
    // Test 1
    describe("SQL response to Vaccine", () => {
        let vaccineDao = new VaccinedAnimalDao();
        // Données fictives renvoyés par la base de données.
        const res = {
            Vaccine_id: 1,
            Animal_id: 1,
            vaccinationDate: new Date("2000/12/20"),
            nextVaccination: new Date("2000/12/20")
        };
        const animalVaccined: VaccinedAnimal = vaccineDao.sqlToVaccinedAnimal(res);
        it("getAnimalId", () => {
            expect(animalVaccined.getAnimalId()).to.be.equal(1);
        });
        it("getVaccineId", () => {
            expect(animalVaccined.getVaccineId()).to.be.equal(1);
        });
        it("getVaccinedDate", () => {
            expect(animalVaccined.getVaccinedDate()).to.be.instanceOf(Date);
        });
        it("getNextVaccination", () => {
            expect(animalVaccined.getNextVaccination()).to.be.instanceOf(Date);
        });
    });

    // Test de l'intégration du DAO avec la base de données.
    describe("DAO integration", () => {
        it("New", async () => {
            const newVaccine: VaccinedAnimal = new VaccinedAnimal(1,3, new Date("2000/12/20"), new Date("2000/12/20"));
            const vaccineDao = new VaccinedAnimalDao();
            const vaccine = await vaccineDao.create(newVaccine);
            expect(vaccine).to.be.instanceOf(VaccinedAnimal);            
            expect(vaccine.getVaccineId()).to.be.equal(1);
            expect(vaccine.getAnimalId()).to.be.equal(3);
            expect(vaccine.getNextVaccination().toString().slice(0,10)).to.be.equal("2000-12-19");  
            expect(vaccine.getVaccinedDate().toString().slice(0,10)).to.be.equal("2000-12-19");  
            // récupérer le dernier id pour l'edit et le delete par la suite
            VaccineId = vaccine.getVaccineId();
            AnimalId = vaccine.getAnimalId();
        });
        it("SelectOne", async () => {
            const vaccinedAnimalDao = new VaccinedAnimalDao();
            const vaccinedAnimal = await vaccinedAnimalDao.selectOne(1,3);
            expect(vaccinedAnimal).to.be.instanceOf(VaccinedAnimal);
        });
        it("selectAnimalsVaccinedByCenter", async () => {
            const vaccineDao = new VaccinedAnimalDao();
            const vaccine = await vaccineDao.selectAnimalsVaccinedByCenter(1); 
            expect(vaccine[0]).to.be.instanceOf(VaccinedAnimal);
        });
        it("Edit", async () => {
            const updateVaccine: VaccinedAnimal = new VaccinedAnimal(1,2, new Date("1500/12/20"), new Date("2100/12/20"));
            const vaccineDao = new VaccinedAnimalDao();
            const vaccine = await vaccineDao.edit(updateVaccine);
            
            expect(vaccine).to.be.instanceOf(VaccinedAnimal);            
            expect(vaccine.getVaccineId()).to.be.equal(1);
            expect(vaccine.getAnimalId()).to.be.equal(2);
            expect(vaccine.getVaccinedDate().toString().slice(0,10)).to.be.equal("1500-12-19"); 
            expect(vaccine.getNextVaccination().toString().slice(0,10)).to.be.equal("2100-12-19");  
        });
        it("Delete", async () => {
            const vaccineDao = new VaccinedAnimalDao();
            const deleteSuccess = await vaccineDao.delete(VaccineId, AnimalId);
            expect(deleteSuccess).to.be.true;
        });
        it("DeleteAllVaccinations", async () => {
            const vaccineDao = new VaccinedAnimalDao();
            const deleteSuccess = await vaccineDao.deleteAllVaccinations(2);
            expect(deleteSuccess).to.be.true;
        });
    });
});

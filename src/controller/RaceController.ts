import { Request, Response } from "express";
import { AdminManager } from "../manager/AdminManager";
import { CenterManager } from "../manager/CenterManager";
import { Race } from "../models/Race";

const adminManager = new AdminManager();
const centerManager = new CenterManager();

export const create = async (req: Request, res: Response) => {
    const reqRace = req.body.Race;

    const result = await adminManager.createAnRace(new Race(reqRace.name));
    if (result) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to create a race"});
};

export const linkCenterRace = async (req: Request, res: Response) => {
    const reqCenterId:any = req.query.CenterId;
    const reqRaceId:any = req.query.RaceId;

    const result = await adminManager.createAnCenterRace(reqCenterId, reqRaceId);
    if (result) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to create a linked race"});
};

export const deleteLinkedCenterRace = async (req: Request, res: Response) => {
    const reqCenterId:any = req.query.CenterId;
    const reqRaceId:any = req.query.RaceId;

    const result = await adminManager.deleteCenterRace(reqCenterId, reqRaceId);
    if (result) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to delete a linked race"});
};

export const deleting = async (req: Request, res: Response) => {
    const reqRaceId:any = req.query.RaceId;

    const result = await adminManager.deleteAnRace(new Race("randomFill", reqRaceId));
    if (result) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to delete race"});
};

export const update = async (req: Request, res: Response) => {
    const result = await adminManager.updateAnRace(new Race(req.body.Race.name, req.body.Race.id));
    if (result) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to update race"});
};

export const getAllRaces = async (req: Request, res: Response) => {
    const result = await centerManager.getRaces();
    if (result[0]) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to get all races"});
};
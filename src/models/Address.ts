export class Address {
    private id:number;
    private postalCode:string;
    private streetName:string;
    private city:string;
    private Country_id:number
    constructor(postalCode: string, streetName: string, city: string, Country_id:number,id?: number) {
        this.id = id;
        this.postalCode = postalCode;
        this.streetName = streetName;
        this.city = city;
        this.Country_id = Country_id;
    }
    public getId(): number {
        return this.id;
    }
    public setId(id: number): void {
        this.id = id;
    }
    public getPostalCode(): string {
        return this.postalCode;
    }
    public setPostalCode(postalCode: string): void {
        this.postalCode = postalCode;
    }
    public getStreetName(): string {
        return this.streetName;
    }
    public setStreetName(streetName: string): void {
        this.streetName = streetName;
    }
    public getCity(): string {
        return this.city;
    }
    public setCity(city: string): void {
        this.city = city;
    }
    public getCountryId(): number {
        return this.Country_id;
    }
    public setCountryId(Country_id: number): void {
        this.Country_id = Country_id;
    }
}

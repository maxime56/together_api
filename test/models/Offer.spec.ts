import { expect } from "chai";
import { Offer } from "../../src/models/Offer";


describe('Offer Models Testing Getter & Setter', () => {
    let offer: Offer;
    // ces dates là me permettent de faire passer mes tests unitaire, le fait de faire un new bloque mes tests car l'instance n'est pas la même
    const newDate = new Date("10/10/2020");
    const oldDate = new Date("10/10/2100");
    beforeEach(() => {
        offer = new Offer("test", "oui", newDate, newDate, 18, 1, 1);
    });
    describe('Getter Id', () => {
        it('getId', () => {
            expect(offer.getId()).to.be.equal(1);
        });
    });
    describe('Getter Setter Name', () => {
        it('getName', () => {
            expect(offer.getName()).to.be.equal("test");
        });
        it("setName", () => {
            expect(offer.setName("Covidus")).to.not.be.equal("test");
        });
    });
    describe('Getter Setter Description', () => {
        it('getDescription', () => {
            expect(offer.getDescription()).to.be.equal("oui");
        });
        it("setDescription", () => {
            offer.setDescription("Covidus");
            expect(offer.getDescription()).to.be.equal("Covidus");
        });
    });
    describe('Getter Setter StartingDate', () => {
        it('getStartingDate', () => {
            expect(offer.getStartingDate()).to.be.equal(newDate);
        });
        it("setStartingDate", () => {
            offer.setStartingDate(oldDate);
            expect(offer.getStartingDate()).to.be.equal(oldDate);
        });
    });
    describe('Getter Setter EndingDate', () => {
        it('getEndingDate', () => {
            expect(offer.getEndingDate()).to.be.equal(newDate);
        });
        it("setEndingDate", () => {
            offer.setEndingDate(oldDate);
            expect(offer.getEndingDate()).to.be.equal(oldDate);
        });
    });
    describe('Getter Setter AvailablePlace', () => {
        it('getAvailablePlace', () => {
            expect(offer.getAvailablePlace()).to.be.equal(18);
        });
        it("setAvailablePlace", () => {
            offer.setAvailablePlace(17);
            expect(offer.getAvailablePlace()).to.be.equal(17);
        });
    });
    describe('Getter Setter CenterId', () => {
        it('getCenterId', () => {
            expect(offer.getCenterId()).to.be.equal(1);
        });
        it("setCenterId", () => {
            offer.setCenterId(2);
            expect(offer.getCenterId()).to.be.equal(2);
        });
    });
});


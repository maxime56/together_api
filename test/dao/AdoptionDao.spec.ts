import { expect } from "chai";
import { Adoption } from "../../src/models/Adoption";
import { AdoptionDao } from "../../src/dao/DaoAdoption";

describe("Adoption DAO", () => {
    let deleteId: number;
    // Test 1
    describe("SQL response to Adoption", () => {
        let adoptionDao = new AdoptionDao();
        // Données fictives renvoyés par la base de données.
        const res = {
            User_id: 1,
            Animal_id: 1,
            Animal_Center_id: 1,
            adoptionDate: new Date("2000/12/20"),
            isActive: true,
            id: 1
        };
        const adoption: Adoption = adoptionDao.sqlToAdoption(res);
        it("UserId", () => {
            expect(adoption.getUserId()).to.be.equal(1);
        });
        it("AnimalId", () => {
            expect(adoption.getAnimalId()).to.be.equal(1);
        });
        it("AnimalCenterId", () => {
            expect(adoption.getAnimalCenterId()).to.be.equal(1);
        });
        it("AdoptionDate", () => {
            expect(adoption.getAdoptionDate()).to.be.instanceOf(Date);
        });
        it("isActive", () => {
            expect(adoption.getIsActive()).to.be.equal(true);
        });
        it("id", () => {
            expect(adoption.getId()).to.be.equal(1);
        });
    });

    // Test de l'intégration du DAO avec la base de données.
    describe("DAO integration", () => {
        it("New", async () => {
            const newAdoption: Adoption = new Adoption(1, 3, 1, true, new Date("12/12/2000"));
            const adoptionDao = new AdoptionDao();
            const adoption = await adoptionDao.create(newAdoption);
            // récupération de l'id qui vient d'être créé pour le supprimer
            deleteId = adoption.getId();
            expect(adoption).to.be.instanceOf(Adoption);
            expect(adoption.getUserId()).to.be.equal(1);
            expect(adoption.getAnimalId()).to.be.equal(3);
            expect(adoption.getAnimalCenterId()).to.be.equal(1);
            expect(adoption.getIsActive()).to.be.equal(1);
            expect(adoption.getAdoptionDate().toString().slice(0,10)).to.be.equal("2000-12-11");
        });

        it("selectOne", async () => {
            const adoptionDao = new AdoptionDao();
            const adoption = await adoptionDao.selectOne(deleteId);
            expect(adoption).to.be.instanceOf(Adoption);
        });

        it("edit", async () => {
            const editAdoption: Adoption = new Adoption(1, 2, 1, false, new Date("12/12/8500"), deleteId);
            const adoptionDao = new AdoptionDao();
            const adoption = await adoptionDao.update(editAdoption);
            // récupération de l'id qui vient d'être créé pour le supprimer
            expect(adoption).to.be.instanceOf(Adoption);
            expect(adoption.getUserId()).to.be.equal(1);
            expect(adoption.getAnimalId()).to.be.equal(2);
            expect(adoption.getAnimalCenterId()).to.be.equal(1);
            expect(adoption.getIsActive()).to.be.equal(0);
            expect(adoption.getAdoptionDate().toString().slice(0,10)).to.be.equal("8500-12-11");
        });
        it("getAdoptionByCenter", async () => {
            const adoptionDao = new AdoptionDao();
            const adoption = await adoptionDao.getAdoptionByCenter(1);
            expect(adoption).to.be.instanceOf(Array);
        });
        it("getAdoptionByUser", async () => {
            const adoptionDao = new AdoptionDao();
            const adoption = await adoptionDao.getAdoptionByUser(1);
            expect(adoption).to.be.instanceOf(Array);
        });
        it("deleteAnimalAdoptions", async () => {
            const adoptionDao = new AdoptionDao();
            const deleteSuccess = await adoptionDao.deleteAnimalAdoptions(4);
            expect(deleteSuccess).to.be.true;
        });
        it("deleteAllAdoptions", async () => {
            const adoptionDao = new AdoptionDao();
            const deleteSuccess = await adoptionDao.deleteAllAdoptions(5);
            expect(deleteSuccess).to.be.true;
        });
    });
});

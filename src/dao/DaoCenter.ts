import { Center } from "../models/Center";
import Database from "../models/Database"

export class CenterDao extends Database {
    public sqlToCenter(res): Center {
        if (res) {
            return new Center(
                res.name,
                res.email,
                res.phoneNumber,
                res.isActive,
                res.schedule,
                res.Address_id,
                res.Country_id,
                res.id
            );
        }
    }
    /**
     * @param {Number} centerId 
     * @returns {Center}
     */
    public async selectOne(centerId: number): Promise<Center> {
        // this.getConnection();
        const query = `CALL get_one_center(${centerId})`;
        const res = await this.request(query);
        return this.sqlToCenter(JSON.parse(JSON.stringify(res[0]))[0]);
    }
    /**
     * @param {Number} limit 
     * @param {Number} offset 
     * @returns {Center}
     */
    public async selectAll(limit: number, offset: number) {
        const query = `CALL select_every_centers(${limit},${offset})`;
        const res = await this.request(query);
        const result = [];
        res[0].forEach(element => {
            let i = this.sqlToCenter(JSON.parse(JSON.stringify(element)));
            result.push(i);
        });
        return result;
    }
    /**
     * @param {Number} limit 
     * @param {Number} offset 
     * @param {Number} countryId 
     * @returns {Center}
     */
    public async selectCenterByCountry(limit: number, offset: number, countryId: number) {
        const query = `CALL select_country_centers(${limit},${offset}, ${countryId})`;
        const res = await this.request(query);
        const result = [];
        res[0].forEach(element => {
            let i = this.sqlToCenter(JSON.parse(JSON.stringify(element)));
            result.push(i);
        });
        return result;
    }
    /**
     * @param {Center} center 
     * @returns {Center}
     */
    public async create(center: Center): Promise<Center> {
        // à régler les procédures ne renvoie pas le dernier id
        let query = `INSERT INTO Center (email, name, phoneNumber, isActive, schedule, Address_id, Country_id) VALUES 
        ("${center.getEmail()}", "${center.getName()}", "${center.getPhoneNumber()}", ${center.getIsActive()}, "${center.getSchedule()}",
        ${center.getAddressId()}, ${center.getCountryId()});`;
        let res = await this.request(query);
        return this.selectOne(res.insertId);
    }
    /**
     * @param {Center} center 
     * @returns {Center}
     */
    public async edit(center: Center): Promise<Center> {
        let query = `CALL update_centre( "${center.getEmail()}", "${center.getName()}", "${center.getPhoneNumber()}", ${center.getIsActive()},
        "${center.getSchedule()}", ${center.getAddressId()}, ${center.getCountryId()}, ${center.getId()})`;
        await this.request(query);
        return this.selectOne(center.getId());
    }
    /**
     * @param {Number} centerId 
     * @returns {Boolean}
     */
    public async delete(centerId: number) {
        const query = `CALL delete_center(${centerId})`;
        const res = await this.request(query);
        const deleteSuccess = res.affectedRows;
        return deleteSuccess === 1 ? true : false;
    }
    /**
 * 
 * @param {number} AddresId 
 * @param {number} id
 * @returns {Boolean} should update the row of addressId for link a address to a center
 */
    public async linkAddressId(AddresId: number, id: number) {
        const query = `CALL link_center_addressId(${AddresId}, ${id})`;
        const res = await this.request(query);
        const deleteSuccess = res.affectedRows;
        return deleteSuccess === 1 ? true : false;
    }
}

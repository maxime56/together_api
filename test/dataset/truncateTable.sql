-- le script va vider l'intégraliter des données de notre base pour y insérer un jeu de donnée "tout propre"
use boegbjzol9xmzfhwchc7;


-- TRUNCATE TABLE Picture;
-- TRUNCATE TABLE Vaccine_animal;
-- TRUNCATE TABLE Vaccine_center;
-- TRUNCATE TABLE Vaccine;
-- TRUNCATE TABLE Country;
-- TRUNCATE TABLE User_role;
-- TRUNCATE TABLE Center_role;
-- TRUNCATE TABLE Role;
-- TRUNCATE TABLE Center_race;
-- TRUNCATE TABLE Race;
-- TRUNCATE TABLE Center_species;
-- TRUNCATE TABLE Species;
-- TRUNCATE TABLE Adoption;
-- TRUNCATE TABLE Candidat;
-- TRUNCATE TABLE User;
-- TRUNCATE TABLE Animal;
-- TRUNCATE TABLE Offer;
-- TRUNCATE TABLE Center;
-- TRUNCATE TABLE Address;


-- ALTER TABLE Picture AUTO_INCREMENT = 1;
-- ALTER TABLE Vaccine_animal AUTO_INCREMENT = 1;
-- ALTER TABLE Vaccine_center AUTO_INCREMENT = 1;
-- ALTER TABLE Vaccine AUTO_INCREMENT = 1;
-- ALTER TABLE Country AUTO_INCREMENT = 1;
-- ALTER TABLE User_role AUTO_INCREMENT = 1;
-- ALTER TABLE Center_role AUTO_INCREMENT = 1;
-- ALTER TABLE Role AUTO_INCREMENT = 1;
-- ALTER TABLE Center_race AUTO_INCREMENT = 1;
-- ALTER TABLE Race AUTO_INCREMENT = 1;
-- ALTER TABLE Center_species AUTO_INCREMENT = 1;
-- ALTER TABLE Species AUTO_INCREMENT = 1;
-- ALTER TABLE Adoption AUTO_INCREMENT = 1;
-- ALTER TABLE Candidat AUTO_INCREMENT = 1;
-- ALTER TABLE User AUTO_INCREMENT = 1;
-- ALTER TABLE Animal AUTO_INCREMENT = 1;
-- ALTER TABLE Offer AUTO_INCREMENT = 1;
-- ALTER TABLE Center AUTO_INCREMENT = 1;
-- ALTER TABLE Address AUTO_INCREMENT = 1;

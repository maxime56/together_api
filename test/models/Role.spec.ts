import { expect } from "chai";
import { Role } from "../../src/models/Role";

describe('Role Models Testing Getter & Setter', () => {
    let role: Role;
    beforeEach(() => {
        role = new Role("Gérant",1);
    });
    describe('Getter Setter Name', () => {
        it('getName', () => {
            expect(role.getName()).to.be.equal("Gérant");
        });
        it("setName", () => {
            role.setName("Gérante");
            expect(role.getName()).to.be.equal("Gérante");
        });
        it("getId", () => {
            expect(role.getId()).to.be.equal(1);
        });
    });
});


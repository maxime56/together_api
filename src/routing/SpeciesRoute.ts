import { create, deleteLinkedCenterSpecies, deleting, getAllSpecies, linkCenterSpecies, update } from "../controller/SpeciesController";
const express = require("express");
const router = express.Router();
const checkToken = require("../middleware/tchekToken");
const centerActionHandler = require("../middleware/centerActionHandler");

router.route("/create/").post(centerActionHandler, checkToken, create); // admin
router.route("/linkCenterSpecies/").post(centerActionHandler, checkToken, linkCenterSpecies); // admin
router.route("/deleteLinkedCenterSpecies/").delete(centerActionHandler, checkToken, deleteLinkedCenterSpecies); // admin
router.route("/delete/").delete(centerActionHandler, checkToken, deleting); // admin
router.route("/update/").post(centerActionHandler, checkToken, update); // admin
router.route("/getSpecies/").get(centerActionHandler, checkToken, getAllSpecies); // admin

module.exports = router;
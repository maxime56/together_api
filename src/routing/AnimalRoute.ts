import { create, getAnimalsByCenter, update, deleting } from "../controller/AnimalController";
const express = require("express");
const router = express.Router();
const checkToken = require("../middleware/tchekToken");
const centerActionHandler = require("../middleware/centerActionHandler");

router.route("/create/").post(centerActionHandler, checkToken, create); // admin
router.route("/update/").post(centerActionHandler, checkToken, update);  // admin
router.route("/delete/").delete(centerActionHandler, checkToken, deleting);  // admin
router.route("/getAnimalsByCenter/").get(getAnimalsByCenter);  // non connected


module.exports = router;
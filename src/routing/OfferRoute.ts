import { create, update, deleting, getOffers } from "../controller/OfferController";
const express = require("express");
const router = express.Router();
const checkToken = require("../middleware/tchekToken");
const centerActionHandler = require("../middleware/centerActionHandler");

router.route("/create/").post(centerActionHandler, checkToken, create); // admin
router.route("/update/").post(centerActionHandler, checkToken, update); // admin
router.route("/delete/").delete(centerActionHandler, checkToken, deleting); // admin
router.route("/getOffers/").get(getOffers); // non user

module.exports = router;

import { UserRole } from "../models/UserRole";
import Database from "../models/Database"

export class UserRoleDao extends Database {
    public sqlToUserRole(res): UserRole {
        if (res) {
            return new UserRole(
                res.Role_id,
                res.User_id,
                res.active,
                res.expiration,
                res.Center_id
            );
        }
    }
    /**
     * 
     * @param {Number} roleId 
     * @param {Number} userId 
     * @returns {UserRole}
     */
    public async selectOne(roleId: number, userId: number): Promise<UserRole> {
        // this.getConnection();
        const query = `CALL select_user_role(${roleId}, ${userId})`;
        const res = await this.request(query);
        return this.sqlToUserRole(JSON.parse(JSON.stringify(res[0]))[0]);
    }
    /**
     * 
     * @param {Number} idUser 
     * @returns {UserRole} Should return the role from a user if they have one
     */
    public async select_user_with_role(idUser: number,) {
        const query = `CALL select_user_with_role(${idUser})`;
        const res = await this.request(query);
        const result = [];
        res[0].forEach(element => {
            let i = this.sqlToUserRole(JSON.parse(JSON.stringify(element)));
            result.push(i);
        });
        return result;
    }
    /**
     * 
     * @param {Number} idCenter 
     * @returns {UserRole} Should return the roles from a user from a center
     */
    // selectionne la table de jointure des utilisateurs qui possèdent un rôle dans un centre
    public async select_user_with_role_by_center(idCenter: number) {
        const query = `CALL select_user_with_role_by_center(${idCenter})`;
        const res = await this.request(query);
        const result = [];
        res[0].forEach(element => {
            let i = this.sqlToUserRole(JSON.parse(JSON.stringify(element)));
            result.push(i);
        });
        return result;
    }
    /**
     * 
     * @param {UserRole} u 
     * @returns {UserRole}
     */
    public async create(u: UserRole): Promise<UserRole> {
        let y = u.getActive().toLocaleString("FR").slice(6, 10);
        let m = u.getActive().toLocaleString("FR").slice(3, 5);
        let d = u.getActive().toLocaleString("FR").slice(0, 2);

        let ye = u.getExpiration().toLocaleString("FR").slice(6, 10);
        let mo = u.getExpiration().toLocaleString("FR").slice(3, 5);
        let da = u.getExpiration().toLocaleString("FR").slice(0, 2);
        let query = `INSERT INTO User_role (Role_id, User_id, active, expiration, Center_id) VALUES 
        (${u.getRoleId()}, ${u.getUserId()}, ("${y}-${m}-${d}"), ("${ye}-${mo}-${da}"), ${u.getCenterId()} );`;
        await this.request(query);
        return this.selectOne(u.getRoleId(), u.getUserId());
    }
    /**
     * 
     * @param {UserRole} u 
     * @returns {UserRole}
     */
    public async edit(u: UserRole): Promise<UserRole> {
        let y = u.getActive().toLocaleString("FR").slice(6, 10);
        let m = u.getActive().toLocaleString("FR").slice(3, 5);
        let d = u.getActive().toLocaleString("FR").slice(0, 2);

        let ye = u.getExpiration().toLocaleString("FR").slice(6, 10);
        let mo = u.getExpiration().toLocaleString("FR").slice(3, 5);
        let da = u.getExpiration().toLocaleString("FR").slice(0, 2);
        let query = `CALL update_user_role(${u.getRoleId()}, ${u.getUserId()}, ("${y}-${m}-${d}"), ("${ye}-${mo}-${da}"), ${u.getCenterId()});`;
        await this.request(query);
        return this.selectOne(u.getRoleId(), u.getUserId());
    }
    /**
     * 
     * @param {Number} roleId 
     * @param {Number} userId 
     * @returns {Boolean}
     */
    public async delete(roleId: number, userId: number) {
        const query = `CALL delete_userRole(${roleId}, ${userId})`;
        const res = await this.request(query);
        const deleteSuccess = res.affectedRows;
        return deleteSuccess === 1 ? true : false;
    }

    /**
     * @param {Number} centerId 
     * @returns {Boolean} Should delete all the role that it been linked in a center
     */
    public async deleteAllRolesInCenter(centerId: number) {
        const query = `CALL delete_center_userRole(${centerId})`;
        const res = await this.request(query);
        const deleteSuccess = res.affectedRows;
        return deleteSuccess > 0 ? true : false;
    }
}
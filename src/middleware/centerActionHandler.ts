import * as jwtx from "jsonwebtoken";
module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(' ')[1];
        const decodedToken = jwtx.verify(token, process.env.TOKEN);
        const roles = decodedToken.data.role;
        // allows to know if the user has a role in the center if the count is greater than 0
        let count = 0;
        roles.forEach(role => {
            if (Object.keys(role) == req.body.Center.id) count++;
            console.log(Object.keys(role), req.body.Center.id);
            
        });
        if (count > 0) {
            next();
        } else {
            throw "Invalid user role";
        }
    } catch (e) {
        console.log(e);
        res.status(403).json({
            error: "Forbidden"
        });
    }
};
#!/bin/bash

read_var(){
  echo $(grep -v '^#' .env | grep -e "$1" | sed -e 's/.*=//')
}

if [[ $(read_var "ENV") == "production" ]] 
then 
    npm run build
    node dist/index.js
else 
    npm run dev
fi


import { expect } from "chai";
import { Race } from "../../src/models/Race";
import { RaceDao } from "../../src/dao/DaoRace";
import { DaoHandler } from "../../src/dao/DaoHandler";

describe("Race DAO", () => {
    let idRace: number;
    // Test 1
    describe("SQL response to Race", () => {
        let raceDao = new RaceDao();
        // Données fictives renvoyés par la base de données.
        const res = {
            name: "test",
            id: 50
        };
        const race: Race = raceDao.sqlToRace(res);
        it("Name", () => {
            expect(race.getName()).to.be.equal("test");
        });
        it("id", () => {
            expect(race.getId()).to.be.equal(50);
        });
    });

    // Test de l'intégration du DAO avec la base de données.
    describe("DAO integration", () => {
        it("New", async () => {
            const newRace: Race = new Race("TDDUS",1);
            const raceDao = new RaceDao();
            const race = await raceDao.create(newRace);
            expect(race).to.be.instanceOf(Race);            
            expect(race.getName()).to.be.equal("TDDUS");
            // récupérer le dernier id pour l'edit et le delete par la suite
            idRace = race.getId();
        });
        it("Get", async () => {
            const raceDao = new RaceDao();
            const race = await raceDao.selectOne(1);
            expect(race).to.be.instanceOf(Race);
        });
        it("select_all_races_by_center", async () => {
            const raceDao = new RaceDao();
            const race = await raceDao.select_all_races_by_center(1);
            expect(race[0]).to.be.instanceOf(Race);
        });
        it("Edit", async () => {
            const editedRace: Race = new Race("EDDITDDUS", idRace);
            const raceDao = new RaceDao();
            const race = await raceDao.edit(editedRace);
            expect(race).to.be.instanceOf(Race);
            expect(race.getName()).to.be.equal("EDDITDDUS");
        });
        it("linkAnRaceInAnCenter", async () => {
            const raceDao = new RaceDao();
            const deleteSuccess = await raceDao.linkAnRaceInAnCenter(3, 2);
            expect(deleteSuccess).to.be.true;
        });
        it("deleteLinkedRaceInAnCenter", async () => {
            const raceDao = new RaceDao();
            const deleteSuccess = await raceDao.deleteLinkedRaceInAnCenter(3, 2);
            expect(deleteSuccess).to.be.true;
        });
        it("Delete", async () => {
            const raceDao = new RaceDao();
            const deleteSuccess = await raceDao.delete(idRace);
            expect(deleteSuccess).to.be.true;
        });
        it("deleteAllRaceByCenterId", async () => {
            const raceDao = new RaceDao();
            const deleteSuccess = await raceDao.deleteAllRaceByCenterId(5);
            expect(deleteSuccess).to.be.true;
        });
        it("deleteAllRaceByCenterId", async () => {
            const raceDao = new RaceDao();
            const result = await raceDao.select_all_races();
            expect(result[0]).to.be.instanceOf(Race);
        });
    });

});

import { expect } from "chai";
import { Animal } from "../../src/models/Animal";
import { AnimalDao } from "../../src/dao/DaoAnimal";
import { DaoHandler } from "../../src/dao/DaoHandler";


describe("Animal DAO", () => {
    let deleteId: number;
    // Test 1
    describe("SQL response to animal", () => {
        let animalDao = new AnimalDao();
        let date = new Date("2000-10-10")
        // Données fictives renvoyés par la base de données.
        const res = {
            name: "albus",
            description: "il est magicien",
            age: 90,
            gender: "Male",
            birthDate: date,
            isActive: true,
            documentRef: "DOCUCUJZ894",
            Center_id: 1,
            Species_id: 1,
            Race_id: 1,
            id: 100
        };
        const animal: Animal = animalDao.sqlToAnimal(res);
        it("Id", () => {
            expect(animal.getId()).to.be.equal(100);
        });
        it("Name", () => {
            expect(animal.getName()).to.be.equal("albus");
        });
        it("Age", () => {
            expect(animal.getAge()).to.be.equal(90);
        });
        it("BirthDate", () => {
            expect(animal.getBirthDate()).to.be.equal(date);
        });
        it("DocumentRef", () => {
            expect(animal.getDocumentRef()).to.be.equal("DOCUCUJZ894");
        });
        it("Gender", () => {
            expect(animal.getGender()).to.be.equal("Male");
        });
        it("IsActive", () => {
            expect(animal.getIsActive()).to.be.equal(true);
        });
        it("Descritpion", () => {
            expect(animal.getDescription()).to.be.equal("il est magicien");
        });
        it("CenterId", () => {
            expect(animal.getCenterId()).to.be.equal(1);
        });
        it("Species", () => {
            expect(animal.getSpecies()).to.be.equal(1);
        });
        it("Race", () => {
            expect(animal.getRace()).to.be.equal(1);
        });
    });

    // Test de l'intégration du DAO avec la base de données.
    describe("DAO integration", () => {
        it("New", async () => {
            const newAnimal: Animal = new Animal("testing", "seulement là quand ça marche", "10", "Male", new Date("10/10/2000"), true, "dzdf", 1, 1, 1);
            const animalDao = new AnimalDao();
            const animal = await animalDao.create(newAnimal);
            // récupération de l'id qui vient d'être créé pour le supprimer
            deleteId = animal.getId();
            expect(animal).to.be.instanceOf(Animal);
            expect(animal.getName()).to.be.equal("testing");
            expect(animal.getAge()).to.be.equal("10");
            expect(animal.getGender()).to.be.equal("Male");
            expect(animal.getBirthDate().toString().slice(0,10)).to.be.equal("2000-10-09");
            expect(animal.getIsActive()).to.be.equal(1);
            expect(animal.getDocumentRef()).to.be.equal("dzdf");
            expect(animal.getDescription()).to.be.equal("seulement là quand ça marche");
            expect(animal.getCenterId()).to.be.equal(1);
            expect(animal.getSpecies()).to.equal(1);
            expect(animal.getRace()).to.be.equal(1);
        });
        it("GetAll", async () => {
            const animalDao = new AnimalDao();
            const animal = await animalDao.getAnimalsByCenter(1,5,0);
            expect(animal[0]).to.be.instanceOf(Animal);
        });
        it("Get", async () => {
            const animalDao = new AnimalDao();
            const animal = await animalDao.selectOne(1);
            expect(animal).to.be.instanceOf(Animal);
        });
        it("Edit", async () => {
            const editedAnimal: Animal = new Animal("update", "test", "10", "Femmelle", new Date("10/10/1999"), false, "ddddd", 1, 1, 1,deleteId);
            const animalDao = new AnimalDao();
            const animal = await animalDao.edit(editedAnimal);
            expect(animal).to.be.instanceOf(Animal);
            expect(animal.getName()).to.be.equal("update");
            expect(animal.getAge()).to.be.equal("10");
            expect(animal.getDescription()).to.be.equal("test");
            expect(animal.getGender()).to.be.equal("Femmelle");
            expect(animal.getIsActive()).to.be.equal(0);
            expect(animal.getDocumentRef()).to.be.equal("ddddd");
            expect(animal.getBirthDate().toString().slice(0,10)).to.be.equal("1999-10-09");
            expect(animal.getCenterId()).to.be.equal(1);
            expect(animal.getSpecies()).to.be.equal(1);
            expect(animal.getRace()).to.be.equal(1);
        });
        it("Delete", async () => {
            const animalDao = new AnimalDao();
            const deleteSuccess = await animalDao.delete(deleteId);
            expect(deleteSuccess).to.be.true;
        });
        it("deleteAllAnimalsInAnCenter", async () => {
            const animalDao = new AnimalDao();
            const deleteSuccess = await animalDao.deleteAllAnimalsInAnCenter(6);
            expect(deleteSuccess).to.be.true;
        });
    });
});

import { Request, Response } from "express";
import { AdminManager } from "../manager/AdminManager";
import { CenterManager } from "../manager/CenterManager";
import { Offer } from "../models/Offer";


const adminManager = new AdminManager();
const centerManager = new CenterManager();

export const getOffers = async (req: Request, res: Response) => {
    let CenterId: any = req.query.centerId;
    const result =  await centerManager.getCenterOffers(CenterId);
    res.status(200).send(result); 
}

export const create = async (req: Request, res: Response) => {
    const nOffer = req.body.Offer;
    const nCenter = req.body.Center;
    const result =  await adminManager.createAnOffer(new Offer(nOffer.name, nOffer.description, nOffer.startingDate, nOffer.endingDate, nOffer.availablePlace, nOffer.Centerid), nCenter.id);
    res.status(200).send(result); 
}

export const update = async (req: Request, res: Response) => {
    const nOffer = req.body.Offer;
    const nCenter = req.body.Center;
    const result =  await adminManager.updateAnOffer(new Offer(nOffer.name, nOffer.description, nOffer.startingDate, nOffer.endingDate, nOffer.availablePlace, nOffer.Centerid, nOffer.id), nCenter.id);
    res.status(200).send(result); 
}

export const deleting = async (req: Request, res: Response) => {
    const offerId: any = req.query.offerId;
    const result =  await adminManager.deleteAnOffer(offerId);
    res.status(200).send(result); 
}
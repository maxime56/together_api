import { Candidat } from "../models/Candidat";
import Database from "../models/Database"


export class CandidatDao extends Database {
    public sqlToCandidat(res): Candidat {
        if (res) {
            return new Candidat(
                res.User_id,
                res.Offer_id,
                res.Offer_Center_id,
                res.landedDate,
                res.isAccepted,
                res.isActive
            );
        }
    }
    /**
     * @param {Number} candidatId 
     * @param {Number} offerId 
     * @returns {Candidat}
     */
    public async selectOne(candidatId: number, offerId: number): Promise<Candidat> {
        const query = `CALL select_candidat(${candidatId}, ${offerId});`;
        const res = await this.request(query);
        return this.sqlToCandidat(JSON.parse(JSON.stringify(res[0]))[0]);
    }
    /**
     * @param {Candidat} candidat 
     * @returns {Candidat}
     */
    public async create(candidat: Candidat): Promise<Candidat> {
        // landedDate        
        let y_landed = candidat.getLandedDate().toLocaleString("FR").slice(6, 10);
        let m_landed = candidat.getLandedDate().toLocaleString("FR").slice(3, 5);
        let d_landed = candidat.getLandedDate().toLocaleString("FR").slice(0, 2);
        let query = `INSERT INTO Candidat (User_id, Offer_id, Offer_Center_id, landedDate, isAccepted, isActive) VALUES
        (${candidat.getUserId()}, ${candidat.getOfferId()}, ${candidat.getOfferCenterId()}, ("${y_landed}-${m_landed}-${d_landed}"), ${candidat.getIsAccepted()}, ${candidat.getIsActive()});`;
        await this.request(query);
        return this.selectOne(candidat.getUserId(), candidat.getOfferId());
    }
    /**
     * @param {Candidat} candidat 
     * @returns {Candidat}
     */
    public async edit(candidat: Candidat): Promise<Candidat> {
        // landedDate
        let y_landed = candidat.getLandedDate().toLocaleString("FR").slice(6, 10);
        let m_landed = candidat.getLandedDate().toLocaleString("FR").slice(3, 5);
        let d_landed = candidat.getLandedDate().toLocaleString("FR").slice(0, 2);
        let query = `CALL update_candidat(${candidat.getUserId()}, ${candidat.getOfferId()}, ${candidat.getOfferCenterId()}, ("${y_landed}-${m_landed}-${d_landed}"), 
        ${candidat.getIsAccepted()}, ${candidat.getIsActive()});`;
        await this.request(query);
        return this.selectOne(candidat.getUserId(), candidat.getOfferId());
    }
    /**
     * @param {Number} userId 
     * @param {Number} offerId 
     * @returns {Boolean}
     */
    public async delete(userId: number, offerId: number) {
        const query = `CALL delete_candidat(${userId}, ${offerId});`;
        const res = await this.request(query);
        const deleteSuccess = res.affectedRows;
        return deleteSuccess === 1 ? true : false;
    }
    /**
     * @param {Number} offerId 
     * @returns {Candidat} Should return users who have applied for a job  
     */
    public async selectCandidatByOffer(offerId: number) {
        const query = `CALL select_candidats_by_offer(${offerId});`;
        const res = await this.request(query);
        const result = [];
        res[0].forEach(element => {
            let i = this.sqlToCandidat(JSON.parse(JSON.stringify(element)));
            result.push(i);
        });
        return result;
    }
    /**
     * @param {Number} userId 
     * @returns {Boolean} Should return all apply from one user
     */
    public async deleteUserAllApplications(userId: number) {
        const query = `CALL delete_all_user_application(${userId});`;
        const res = await this.request(query);
        const deleteSuccess = res.affectedRows;
        return deleteSuccess !== 0 ? true : false;
    }
    /**
     * @param {Number} centerId 
     * @returns {Boolean} Should removes candidates who have applied for jobs at a centre & return a boolean
     */
    public async deleteAllCandidats(centerId: number) {
        const query = `CALL delete_center_candidat(${centerId});`;
        const res = await this.request(query);
        const deleteSuccess = res.affectedRows;
        return deleteSuccess > 0 ? true : false;
    }
}
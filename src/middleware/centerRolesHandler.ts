import { DaoRole } from "../dao/DaoRole";


// get the name of the user role
module.exports = async (idRole: number, next) => {
    const roleDao = new DaoRole();
    try {
        return (await roleDao.selectOne(idRole)).getName();
    } catch (e) {
        console.log(e);
    }
};
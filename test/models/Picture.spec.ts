import { expect } from "chai";
import { Picture } from "../../src/models/Picture";

describe('Picture Models Testing Getter & Setter', () => {
    let picture: Picture;
    // ces dates là me permettent de faire passer mes tests unitaire, le fait de faire un new bloque mes tests car l'instance n'est pas la même
    beforeEach(() => {
        picture = new Picture("test", 18, 1, 1, 1, 10);
    });
    describe('Getter Id', () => {
        it('getId', () => {
            expect(picture.getId()).to.be.equal(10);
        });
    });
    describe('Getter Setter Path', () => {
        it('getPath', () => {
            expect(picture.getPath()).to.be.equal("test");
        });
        it("setPath", () => {
            expect(picture.setPath("Covidus")).to.not.be.equal("test");
        });
    });
    describe('Getter Setter AnimalId', () => {
        it('getAnimalId', () => {
            expect(picture.getAnimalId()).to.be.equal(18);
        });
        it("setAnimalId", () => {
            picture.setAnimalId(10);
            expect(picture.getAnimalId()).to.be.equal(10);
        });
    });
    describe('Getter Setter UserId', () => {
        it('getUserId', () => {
            expect(picture.getUserId()).to.be.equal(1);
        });
        it("setUserId", () => {
            picture.setUserId(10);
            expect(picture.getUserId()).to.be.equal(10);
        });
    });
    describe('Getter Setter CenterId', () => {
        it('getCenterId', () => {
            expect(picture.getCenterId()).to.be.equal(1);
        });
        it("setCenterId", () => {
            picture.setCenterId(100);
            expect(picture.getCenterId()).to.be.equal(100);
        });
    });
    describe('Getter Setter OfferId', () => {
        it('getOfferId', () => {
            expect(picture.getOfferId()).to.be.equal(1);
        });
        it("setOfferId", () => {
            picture.setOfferId(100);
            expect(picture.getOfferId()).to.be.equal(100);
        });
    });
});


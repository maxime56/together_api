import { expect } from "chai";
import { Candidat } from "../../src/models/Candidat";
import { CandidatDao } from "../../src/dao/DaoCandidat";
import { DaoHandler } from "../../src/dao/DaoHandler";

describe("Candidat DAO", () => {
    let deleteId: number;
    let deletedID: number;
    // Test 1
    describe("SQL response to Candidat", () => {
        let candidatDao = new CandidatDao();
        // Données fictives renvoyés par la base de données.
        const res = {
            User_id: 1,
            Offer_id: 1,
            Offer_Center_id: 1,
            landedDate: new Date("2000/10/10"),
            isAccepted: true,
            isActive: false
        };
        const candidat: Candidat = candidatDao.sqlToCandidat(res);
        it("UserId", () => {
            expect(candidat.getUserId()).to.be.equal(1);
        });
        it("OfferId", () => {
            expect(candidat.getOfferId()).to.be.equal(1);
        });
        it("OfferCenterId", () => {
            expect(candidat.getOfferCenterId()).to.be.equal(1);
        });
        it("LandedDate", () => {
            expect(candidat.getLandedDate()).to.be.instanceOf(Date);
        });
        it("isAccepted", () => {
            expect(candidat.getIsAccepted()).to.be.equal(true);
        });
        it("isActive", () => {
            expect(candidat.getIsActive()).to.be.equal(false);
        });
    });
//     // Test de l'intégration du DAO avec la base de données.
    describe("DAO integration", () => {
        it("New", async () => {
            const newCandidat: Candidat = new Candidat(4,1, 1 ,new Date("10/10/1999"),true, false);
            const candidatDao = new CandidatDao();
            const candidat = await candidatDao.create(newCandidat);
            // récupération de l'id qui vient d'être créé pour le supprimer
            deleteId = candidat.getUserId();
            deletedID = candidat.getOfferId();
            expect(candidat).to.be.instanceOf(Candidat);
            expect(candidat.getUserId()).to.be.equal(4);
            expect(candidat.getOfferId()).to.be.equal(1);
            expect(candidat.getOfferCenterId()).to.be.equal(1);
            expect(candidat.getLandedDate().toString().slice(0,10)).to.be.equal("1999-10-09");   
            expect(candidat.getIsAccepted()).to.be.equal(1);
            expect(candidat.getIsActive()).to.be.equal(0);
        });
        it("Get", async () => {
            const candidatDao = new CandidatDao();
            const candidat = await candidatDao.selectOne(deleteId,deletedID);
            expect(candidat).to.be.instanceOf(Candidat);
        });

        it("GetCandidatByOffer", async () => {
            const candidatDao = new CandidatDao();
            const candidat = await candidatDao.selectCandidatByOffer(1)
            expect(candidat[0]).to.be.instanceOf(Candidat);
        });
        it("Edit", async () => {
            const editedCenter: Candidat = new Candidat(4, 1, 1,new Date("10/10/1999"), false,false);
            const candidatDao = new CandidatDao();
            const candidat = await candidatDao.edit(editedCenter);
            expect(candidat).to.be.instanceOf(Candidat);
            expect(candidat.getUserId()).to.be.equal(4);
            expect(candidat.getOfferId()).to.be.equal(1);
            expect(candidat.getOfferCenterId()).to.be.equal(1);
            expect(candidat.getLandedDate().toString().slice(0,10)).to.be.equal("1999-10-09");  
            expect(candidat.getIsAccepted()).to.be.equal(0);
            expect(candidat.getIsActive()).to.be.equal(0);
        });
        it("delete", async () => {
            const candidatDao = new CandidatDao();
            const deleteSuccess = await candidatDao.delete(deleteId,deletedID);
            expect(deleteSuccess).to.be.true;
        });
        it("delete by user id", async () => {
            const candidatDao = new CandidatDao();
            const deleteSuccess = await candidatDao.deleteUserAllApplications(2);
            expect(deleteSuccess).to.be.true;
        });
        it("delete by center id", async () => {
            const candidatDao = new CandidatDao();
            const deleteSuccess = await candidatDao.deleteAllCandidats(5);
            expect(deleteSuccess).to.be.true;
        });
    });
});

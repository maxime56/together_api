import { expect } from "chai";
import { Adoption } from "../../src/models/Adoption";


describe('Adoption Models Testing Getter & Setter', () => {
    let adoption: Adoption;
    // ces dates là me permettent de faire passer mes tests unitaire, le fait de faire un new bloque mes tests car l'instance n'est pas la même
    const newDate = new Date("10/10/2020");
    const oldDate = new Date("10/10/2100");
    beforeEach(() => {
        adoption = new Adoption(1, 2, 1, true,newDate,4);
    });
    describe('Getter Setter UserId', () => {
        it('getUserId', () => {
            expect(adoption.getUserId()).to.be.equal(1);
        });
        it("setUserId", () => {
            adoption.setUserId(2)
            expect(adoption.getUserId()).to.be.equal(2);
        });
    });
    describe('Getter Setter AnimalId', () => {
        it('getAnimalId', () => {
            expect(adoption.getAnimalId()).to.be.equal(2);
        });
        it("setAnimalId", () => {
            adoption.setAnimalId(1)
            expect(adoption.getAnimalId()).to.be.equal(1);
        });
    });
    describe('Getter Setter AnimalCenterId', () => {
        it('getAnimalCenterId', () => {
            expect(adoption.getAnimalCenterId()).to.be.equal(1);
        });
        it("setAnimalCenterId", () => {
            adoption.setAnimalCenterId(3)
            expect(adoption.getAnimalCenterId()).to.be.equal(3);
        });
    });
    describe('Getter Setter AdoptionDate', () => {
        it('getAdoptionDate', () => {
            expect(adoption.getAdoptionDate()).to.be.equal(newDate);
        });
        it("setAdoptionDate", () => {
            adoption.setAdoptionDate(oldDate)
            expect(adoption.getAdoptionDate()).to.be.equal(oldDate);
        });
    });
    describe('Getter Setter isActive', () => {
        it('getIsActive', () => {
            expect(adoption.getIsActive()).to.be.equal(true);
        });
        it("setIsActive", () => {
            adoption.setIsActive(false)
            expect(adoption.getIsActive()).to.be.equal(false);
        });
    });
    describe('Getter Setter Id', () => {
        it('getId', () => {
            expect(adoption.getId()).to.be.equal(4);
        });
    });
});


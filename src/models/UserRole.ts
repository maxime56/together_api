export class UserRole{
    private Role_id:number;
    private User_id:number;
    private active:Date;
    private expiration:Date;
    private Center_id:number;

    public constructor (roleId:number, userId:number, active:Date, expiration:Date, centerId:number) {
        this.Role_id = roleId;
        this.User_id = userId;
        this.active = active;
        this.expiration = expiration;
        this.Center_id = centerId;
    }
    public getRoleId(): number {
        return this.Role_id;
    }
    public setRoleId(roleId:number):void{
        this.Role_id = roleId;
    }
    public getUserId(): number {
        return this.User_id;
    }
    public setUserId(userId:number):void{
        this.User_id = userId;
    }
    public getActive(): Date {
        return this.active;
    }
    public setActive(active:Date):void{
        this.active = active;
    }
    public getExpiration(): Date {
        return this.expiration;
    }
    public setExpiration(expiration:Date):void{
        this.expiration = expiration;
    }
    public getCenterId(): number {
        return this.Center_id;
    }
    public setCenterId(Center_id:number):void{
        this.Center_id = Center_id;
    }
}
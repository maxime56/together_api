import { expect } from "chai";
import { Race } from "../../src/models/Race";

describe('Race Models Testing Getter & Setter', () => {
    let race: Race;
    beforeEach(() => {
        race = new Race("Bulldog",1);
    });
    describe('Getter Setter Name', () => {
        it('getName', () => {
            expect(race.getName()).to.be.equal("Bulldog");
        });
        it("setName", () => {
            expect(race.setName("Covidus")).to.not.be.equal("Bulldog");
        });
        it("getId", () => {
            expect(race.getId()).to.be.equal(1);
        });
    });
});


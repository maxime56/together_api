import { Request, response, Response } from "express";
import { AdminManager } from "../manager/AdminManager";
import { Picture } from "../models/Picture";

const multer = require('multer');
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, "./public/uploads"); // path to image stockage
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname); // the name of the picture
    }
});

const whitelist = [
    'image/png',
    'image/jpeg',
    'image/jpg'
];
const adminManager = new AdminManager();


const upload = multer({
    storage: storage, fileFilter: (req, file, cb) => {
        if (!whitelist.includes(file.mimetype)) return cb(new Error('file is not allowed'));
        cb(null, true);
    }
}).single('picture');


const multiUpload = multer({
    storage: storage, fileFilter: (req, file, cb) => {
        if (!whitelist.includes(file.mimetype)) return cb(new Error('file is not allowed'));
        cb(null, true);
    }
}).array('pictures', 6);


export const createUserPicture = async (req, res: Response) => {
    await upload(req, res, async function (err) {
        const file = req.file;
        if (err) return res.status(400).send(err);
        const reqPicture = req.body.Picture;
        const result = await adminManager.createAnUserPicture(new Picture("fill", null, reqPicture.UserId, null, null), file);
        if (result) return res.status(200).send(result);
        return res.status(400).send({Error: "Failed to create a user picture"});
    });
};

export const createAnimalPicture = async (req, res: Response) => {
    await multiUpload(req, res, async function (err) {
        const file = req.files;
        if (err) return res.status(400).send(err);
        const reqPicture = req.body.Picture;
        const result = await adminManager.createAnAnimalPicture(new Picture("fill", reqPicture.AnimalId, null, null, null), file);
        if (result[0] !== undefined || result[0] !== null) return res.status(200).send(result);
        return res.status(400).send({Error: "Failed to create a animal picture"});
    });
};

export const createOfferPicture = async (req, res: Response) => {
    await multiUpload(req, res, async function (err) {
        const file = req.files;
        if (err) return res.status(400).send(err);
        const reqPicture = req.body.Picture;
        const result = await adminManager.createAnOfferPicture(new Picture("fill", null, null, reqPicture.OfferId, null), file);
        if (result[0] !== undefined || result[0] !== null) return res.status(200).send(result);
        return res.status(400).send({Error: "Failed to create a offer picture"});
    });
};

export const createCenterPicture = async (req, res: Response) => {
    await multiUpload(req, res, async function (err) {
        const file = req.files;
        if (err) return res.status(400).send(err);
        const reqPicture = req.body.Picture;
        const result = await adminManager.createAnCenterPicture(new Picture("fill", null, null, null, reqPicture.CenterId), file);
        if (result[0] !== undefined || result[0] !== null) return res.status(200).send(result);
        return res.status(400).send({Error: "Failed to create a center picture"});
    });
};

export const deleteAnimalPicture = async (req, res: Response) => {
    const reqPicture = req.body.Picture;
    await checkPath(reqPicture.path, res);
    const result = await adminManager.deleteAnAnimalPicture(new Picture(reqPicture.path, reqPicture.AnimalId, null, null, null, reqPicture.id));
    if (result) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to delete a animal picture"});
};

export const deleteCenterPicture = async (req, res: Response) => {
    const reqPicture = req.body.Picture;
    await checkPath(reqPicture.path, res);
    const result = await adminManager.deleteAnCenterPicture(new Picture(reqPicture.path, null, null, null, reqPicture.CenterId, reqPicture.id));
    if (result) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to delete a center picture"});
};

export const deleteOfferPicture = async (req, res: Response) => {
    const reqPicture = req.body.Picture;
    await checkPath(reqPicture.path, res);
    const result = await adminManager.deleteAnOfferPicture(new Picture(reqPicture.path, null, null, reqPicture.OfferId, null, reqPicture.id));
    if (result) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to delete a offer picture"});
};

export const deleteUserPicture = async (req, res: Response) => {
    const reqPicture = req.body.Picture;
    await checkPath(reqPicture.path, res);
    const result = await adminManager.deleteAnUserPicture(new Picture(reqPicture.path, null, reqPicture.UserId, null, null, reqPicture.id));
    if (result) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to delete a user picture"});
};


const checkPath = async (path, res: Response) => {
    if (path.substr(0, 9) !== "/uploads/") {
        return res.status(403).send("Forbbiden");
    }
}
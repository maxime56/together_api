import { UserRoleDao } from "../dao/DaoUserRole";
const roleHandler = require("./roleHandler");

// permet de chercher les ou un rôle d'un utilisateur (si il en possède),  
module.exports = async (userId: number, next) => {
    const userRoleDao = new UserRoleDao();
    let rolePromise = [];
    let role = [];
    try {
        const resultUserRole = await userRoleDao.select_user_with_role(userId);
        if (resultUserRole.length !== 0) {
            resultUserRole.forEach(userRole => {
                if (new Date(Date.now()).getTime() >= new Date(userRole.active).getTime() &&
                    new Date(Date.now()).getTime() <= new Date(userRole.expiration).getTime()) {
                    // j'ai l'id des rôles et je veux chercher l'id des centres
                    // je fais un middleware qui va me retourner l'id des centres
                    rolePromise.push(roleHandler(userRole.Role_id, userRole.Center_id));
                } else {
                    //console.log("je suis pas actif");
                }
            });
            await Promise.all(rolePromise).then(value => {
                role.push(value);
            });
            return role[0];
        } else {
            //console.log("je n'ai pas de rôle");
        }
    } catch (e) {
        console.log(e);
    }
};
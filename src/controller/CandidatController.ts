import { Request, Response } from "express";
import { AdminManager } from "../manager/AdminManager";
import { CenterManager } from "../manager/CenterManager";
import { UserManager } from "../manager/UserManager";
import { Candidat } from "../models/Candidat";

const adminManager = new AdminManager();
const centerManager = new CenterManager();
const userManager = new UserManager();

export const update = async (req: Request, res: Response) => {
    const reqCandidat = req.body.Candidat;
    const reqCenter = req.body.Center;      
    const result = await adminManager.updateAnCandidat(new Candidat(reqCandidat.UserId, reqCandidat.OfferId, reqCandidat.OfferCenterId, reqCandidat.landedDate, reqCandidat.isAccepted, reqCandidat.isActive), reqCenter.id);
    if (result) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to update a candidat"});
}

export const deleteUserOffer = async (req: Request, res: Response) => {
    const userId:any = req.query.UserId;
    const offerId:any = req.query.offerId;
    const result = await userManager.removeAppliedOffer(userId, offerId);
    if (result) return res.status(200).send(result);
    return res.status(400).send(false);
}

export const deleteCenterOffer = async (req: Request, res: Response) => {
    const userId:any = req.query.UserId;
    const offerId:any = req.query.offerId;
    const result = await adminManager.deleteAnCandidat(userId, offerId);
    if (result) return res.status(200).send(result);
    return res.status(400).send(false);
}

export const getAllCandidatsFromOffer = async (req: Request, res: Response) => {
    const offerId:any = req.query.offerId;    
    const result = await centerManager.getCandidatsFromOffer(offerId);
    if (result) return res.status(200).send(result);
    return res.status(404).send({Error: "Failed to get all the candidats"});
}
const express = require("express");
const router = express.Router();

import { create, update, getCenters, deleting, getRacesAndSpeciesByCenter, getVaccineByCenter, getAllCenters, getOneCenter } from "../controller/CenterController";
const checkToken = require("../middleware/tchekToken");
const centerActionHandler = require("../middleware/centerActionHandler");
const verifEmail = require("../middleware/verifEmail");
const verifPassword = require("../middleware/verifPassword");

router.route("/create/").post(centerActionHandler, verifPassword, verifEmail, checkToken, create); // admin
router.route("/update/").post(centerActionHandler, verifPassword,  verifEmail,  checkToken, update); // admin
router.route("/delete/").post(centerActionHandler, checkToken, deleting); // admin
router.route("/getByCountry/").get(getCenters); // non user
router.route("/getRacesSpecies/").get(getRacesAndSpeciesByCenter); // non user
router.route("/getVaccines/").get(getVaccineByCenter); // non user
router.route("/getRoles/").get(getVaccineByCenter); // non user
router.route("/getEveryCenters/").get(getAllCenters); // non user
router.route("/getOneCenter/").get(getOneCenter); // non user

module.exports = router;
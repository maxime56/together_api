import { expect } from "chai";
import { Species } from "../../src/models/Species";

describe('species Models Testing Getter & Setter', () => {
    let species: Species;
    beforeEach(() => {
        species = new Species("AntiCovidus",1);
    });
    describe('Getter Setter Name', () => {
        it('getName', () => {
            expect(species.getName()).to.be.equal("AntiCovidus");
        });
        it("setName", () => {
            expect(species.setName("AntiCovidous")).to.not.be.equal("AntiCovidus");
        });
        it("getId", () => {
            expect(species.getId()).to.be.equal(1);
        });
    });
});


import { Request, Response } from "express";
import { AdminManager } from "../manager/AdminManager";
import { UserManager } from "../manager/UserManager";
import { Address } from "../models/Address";


const userManager = new UserManager();
const adminManager = new AdminManager();

export const createCenterAddress = async (req: Request, res: Response) => {
    const reqAddress = req.body.Address;
    const reqCenter = req.body.Center;
    const result = await adminManager.createAnCenterAddress(new Address(reqAddress.postalCode, reqAddress.streetName, reqAddress.city, reqAddress.CountryId), reqCenter.id);
    if (result.Address) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to create a address"});
};

export const updateCenterAddress = async (req: Request, res: Response) => {
    const reqAddress = req.body.Address;
    const reqCenter = req.body.Center;
    const result = await adminManager.updateAnCenterAddress(new Address(reqAddress.postalCode, reqAddress.streetName, reqAddress.city, reqAddress.CountryId, reqAddress.id), reqCenter.id);
    if (result.Address) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to update a address"});
};

export const createUserAddress = async (req: Request, res: Response) => {
    const reqAddress = req.body.Address;
    const reqUser = req.body.User;
    const result = await userManager.createAnUserAddress(new Address(reqAddress.postalCode, reqAddress.streetName, reqAddress.city, reqAddress.CountryId), reqUser.id);
    if (result.Address.getId() > 0) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to create a address"});
};

export const updateUserAddress = async (req: Request, res: Response) => {
    const reqUser = req.body.User;
    const reqAddress = req.body.Address;    
    const result = await userManager.updateAnUserAddress(new Address(reqAddress.postalCode, reqAddress.streetName, reqAddress.city, reqAddress.CountryId, reqAddress.id), reqUser.id);
    if (result.Address.getId() > 0) return res.status(200).send(result);
    if (result == null || undefined) return res.status(403).send({Error: "Forbbiden"});
    return res.status(500).send({Error: "Failed to update a address"});
};

export const deleting = async (req: Request, res: Response) => {
    const reqUser = req.body.User;
    const reqAddress = req.body.Address;
    const result = await userManager.deleteAnUserAddress(reqAddress.id, reqUser.id);
    if (result) return res.status(200).send(result);
    return res.status(500).send({Error: "Failed to delete a address"});
};
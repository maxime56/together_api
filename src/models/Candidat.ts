export class Candidat {
    private User_id :number;
    private Offer_id :number;
    private Offer_Center_id: number;
    private landedDate :Date;
    private dateStart :Date;
    private isAccepted :boolean;
    private isActive: boolean;
    public constructor(User_id:number, Offer_id:number, OfferCenterId:number, landedDate:Date,isAccepted:boolean, isActive:boolean) {
        this.User_id = User_id;
        this.Offer_id = Offer_id;
        this.landedDate = landedDate;
        this.isAccepted = isAccepted;
        this.isActive = isActive;
        this.Offer_Center_id = OfferCenterId;
    }
    public getUserId(): number {
        return this.User_id;
    }
    public setEmail(User_id:number):void {
        this.User_id = User_id;
    }
    public getOfferId(): number {
        return this.Offer_id;
    }
    public setOfferId(Offer_id:number):void {
        this.Offer_id = Offer_id;
    }
    public getLandedDate(): Date {
        return this.landedDate;
    }
    public setLandedDate(landedDate:Date):void {
        this.landedDate = landedDate;
    }
    public getIsAccepted(): boolean {
        return this.isAccepted;
    }
    public setIsAccepted(isAccepted:boolean):void {
        this.isAccepted = isAccepted;
    }
    public getIsActive(): boolean {
        return this.isActive;
    }
    public setIsActive(isActive:boolean):void {
        this.isActive = isActive;
    }
    public getOfferCenterId(): number {
        return this.Offer_Center_id;
    }
    public setOfferCenterId(Offer_Center_id: number):void {
        this.Offer_Center_id = Offer_Center_id;
    }
}

const jwtt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(' ')[1];
        const decodedToken = jwtt.verify(token, process.env.TOKEN);
        const userId = decodedToken.data.id;
        

        if ( userId == req.body.User.id &&decodedToken) {
            const refreshToken = jwtt.sign(
                {data: {...decodedToken.data}},
                process.env.TOKEN,
                { expiresIn: '2h' },

            );
            res.status(200).json({RefreshToken: refreshToken});
            next();
        } else {
            throw 'Invalid user ID';
        }
    } catch (e) {
        console.log(e);

        res.status(403).json({
            error: "Token invalid"
        });
    }
};

import { Country } from "../models/Country";
import Database from "../models/Database"

export class CountryDao extends Database {
    public sqlToCountry(res): Country {
        if (res) {
            return new Country(
                res.name,
                res.id
            );
        }
    }

    /**
     * @returns {Country}
     */
    public async selectAll() {
        const query = `CALL get_countrys()`;
        const res = await this.request(query);
        const result = [];
        res[0].forEach(element => {
            let i = this.sqlToCountry(JSON.parse(JSON.stringify(element)));
            result.push(i);
        });
        return result;
    }

    /**
     * @returns {Country}
     */
     public async selectCenterCountrys() {
        const query = `CALL get_center_countrys()`;
        const res = await this.request(query);
        const result = [];
        res[0].forEach(element => {
            let i = this.sqlToCountry(JSON.parse(JSON.stringify(element)));
            result.push(i);
        });
        return result;
    }
}

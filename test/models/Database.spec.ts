import { expect } from "chai";
import Database from "../../src/models/Database";
describe("Database", () => {
   const database = new Database();
  // Test de la connexion avec la base de données.
  it("Connection to database", async function () {
    const res = await database.test();
    expect(res).to.be.equal("OK");
  });
});

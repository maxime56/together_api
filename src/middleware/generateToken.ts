const jwttt = require('jsonwebtoken');

module.exports = async (userId: number, userEmail: string, roleArray: any, next) => {
    try {
        return await jwttt.sign({
            data: { id: userId, username: userEmail, role: roleArray},
        }, process.env.TOKEN, { expiresIn: '2h' });
    } catch (e) {
        console.log(e);
    }
};
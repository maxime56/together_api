import rateLimit from "express-rate-limit";
import { create, deleting, getOne, login, offerApply, update } from "../controller/UserController";


const checkToken = require("../middleware/tchekToken");
const express = require("express");
const router = express.Router();
const verifEmail = require("../middleware/verifEmail");
const verifPassword = require("../middleware/verifPassword");
const refreshToken = require("../middleware/refreshToken");

const loginAttempt = rateLimit({
	windowMs: 15 * 60 * 1000, // 15 mins
	max: 5, // Limit each IP to 5 create account requests per `window`
	message:
		'Too many attempt, please try again after 15 mins',
	standardHeaders: true, // Return rate limit info in the `RateLimit-*` headers
	legacyHeaders: false, // Disable the `X-RateLimit-*` headers
});


router.route("/create/").post(verifEmail, verifPassword, create);  // non user 
router.route("/update/").post(verifEmail, verifPassword, checkToken, update); // user connected
router.route("/getOne/").get(checkToken, getOne);  // admin or user connected
router.route("/login/").post(loginAttempt, login); // non user
router.route("/deleting/").post(checkToken, deleting); // user connected
router.route("/apply/").post(checkToken, offerApply); // user connected
router.route("/refreshToken").post(refreshToken);

module.exports = router;


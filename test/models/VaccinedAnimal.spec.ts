import { expect } from "chai";
import { VaccinedAnimal } from "../../src/models/VaccinedAnimal";
  let animalVaccined: VaccinedAnimal;
describe('AnimalVaccined Models Testing Getter & Setter', () => {
    // ces dates là me permettent de faire passer mes tests unitaire, le fait de faire un new bloque mes tests car l'instance n'est pas la même
    const newDate = new Date("10/10/2020");
    const oldDate = new Date("10/10/2100");
    beforeEach(() => {
        animalVaccined = new VaccinedAnimal(1, 1, newDate, newDate);
    });
    describe('Getter Setter AnimalId', () => {
        it('getAnimalId', () => {
            expect(animalVaccined.getAnimalId()).to.be.equal(1);
        });
        it("setAnimalId", () => {
            animalVaccined.setAnimalId(2);
            expect(animalVaccined.getAnimalId()).to.be.equal(2);
        });
    });
    describe('Getter Setter Vaccine', () => {
        it('getVaccine', () => {
            expect(animalVaccined.getVaccineId()).to.be.equal(1);
        });
        it("setVaccine", () => {
            animalVaccined.setVaccineId(2);
            expect(animalVaccined.getVaccineId()).to.be.equal(2);
        });
    });
    describe('Getter Setter VaccinedDate', () => {
        it('getVaccinedDate', () => {
            expect(animalVaccined.getVaccinedDate()).to.be.equal(newDate);
        });
        it("setVaccinedDate", () => {
            animalVaccined.setVaccinedDate(oldDate);
            expect(animalVaccined.getVaccinedDate()).to.be.equal(oldDate);
        });
    });
    describe('Getter Setter NextVaccination', () => {
        it('getNextVaccination', () => {
            expect(animalVaccined.getNextVaccination()).to.be.equal(newDate);
        });
        it("setNextVaccination", () => {
            animalVaccined.setNextVaccination(oldDate)
            expect(animalVaccined.getNextVaccination()).to.be.equal(oldDate);
        });
    });
});


import { DaoRole } from "../dao/DaoRole";

// permet de chercher les ou un rôle d'un utilisateur (si il en possède),  
module.exports = async (idRole: number, centerId: number, next) => {
    const roleDao = new DaoRole();
    try {
        // retourne l'id du centre en index et le role en valeur
        return { [centerId]: (await roleDao.selectOne(idRole)).getName() };
    } catch (e) {
        console.log(e);
    }
};
import { Request, Response } from "express";
import { AdminManager } from "../manager/AdminManager";
import { CenterManager } from "../manager/CenterManager";
import { Vaccine } from "../models/Vaccine";
import { VaccinedAnimal } from "../models/VaccinedAnimal";


const adminManager = new AdminManager();
const centerManager = new CenterManager();


export const create = async (req: Request, res: Response) => {
    const reqVaccine = req.body.Vaccine;

    const result = await adminManager.createAnVaccine(new Vaccine(reqVaccine.name));
    if (result) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to create a vaccine"});
};


export const linkVaccinedAnimal = async (req: Request, res: Response) => {
    const reqVaccinedAnimal = req.body.VaccinedAnimal;

    const result = await adminManager.createAnVaccinedAnimal(new VaccinedAnimal(reqVaccinedAnimal.VaccineId, reqVaccinedAnimal.AnimalId, reqVaccinedAnimal.vaccinedDate, reqVaccinedAnimal.nextVaccination));
    if (result) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to linked a vaccine"});
};


export const deleteLinkedVaccinedAnimal = async (req: Request, res: Response) => {
    const reqAnimalId:any = req.query.AnimalId;
    const reqVaccineId:any = req.query.VaccineId;

    const result = await adminManager.deleteAnCenterVaccine(reqAnimalId, reqVaccineId);
    if (result) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to delete a vaccine linked"});
};


export const deleting = async (req: Request, res: Response) => {
    const reqVaccineId:any = req.query.VaccineId;

    const result = await adminManager.deleteAnVaccine(new Vaccine("randomFill", reqVaccineId));
    if (result) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to delete a vaccine"});
};

export const updateLinkedVaccinedAnimal = async (req: Request, res: Response) => {
    const reqVaccinedAnimal = req.body.VaccinedAnimal;

    const result = await adminManager.updateAnVaccinedAnimal(new VaccinedAnimal(reqVaccinedAnimal.VaccineId, reqVaccinedAnimal.AnimalId, reqVaccinedAnimal.vaccinedDate, reqVaccinedAnimal.nextVaccination));
    if (result) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to update a vaccine linked"});
};

export const update = async (req: Request, res: Response) => {
    const result = await adminManager.updateAnVaccine(new Vaccine(req.body.Vaccine.name, req.body.Vaccine.id));
    if (result) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to update a vaccine"});
};

export const getAllVaccines = async (req: Request, res: Response) => {
    const result = await centerManager.getVaccines();
    if (result.Vaccines[0]) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to get all vaccines"});
};
import { expect } from "chai";
import { User } from "../../src/models/User";
import { UserDao } from "../../src/dao/DaoUser";

import { DaoHandler } from "../../src/dao/DaoHandler";

describe("User DAO", () => {
    let deleteId: number;
    // Test 1
    describe("SQL response to User", () => {
        let userDao = new UserDao();

        // Données fictives renvoyés par la base de données.
        const res = {
            id: 1,
            email: "mail@gmai.com",
            firstName: "dd",
            lastName: "dddaz",
            phoneNumber: "6060060",
            password: "1234",
            lastConnexion: new Date("10/02/1999"),
            Address_id: 1
        };

        const user: User = userDao.sqlToUser(res);
        it("Id", () => {
            expect(user.getId()).to.be.equal(1);
        });
        it("Email", () => {
            expect(user.getEmail()).to.be.equal("mail@gmai.com");
        });
        it("First Name", () => {
            expect(user.getFirstName()).to.be.equal("dd");
        });
        it("Last Name", () => {
            expect(user.getLastName()).to.be.equal("dddaz");
        });
        it("PhoneNumber", () => {
            expect(user.getPhoneNumber()).to.be.equal("6060060");
        });
        it("Password", () => {
            expect(user.getPassword()).to.be.equal("1234");
        });
        it("lastConnexion", () => {
            expect(user.getLastConnexion()).to.be.instanceOf(Date);
        });
        it("AddressId", () => {
            expect(user.getAddressId()).to.be.equal(1);
        });
    });

    // Test de l'intégration du DAO avec la base de données.
    describe("DAO integration", () => {

        it("New", async () => {
            const newUser: User = new User(`tesddddtupdate@gmail.com`, "oui", "non", "66", new Date("1999/10/02"), "1234", 16);
            const userDao = new UserDao();
            const user = await userDao.create(newUser);
            // récupération de l'id qui vient d'être créé pour le supprimer
            deleteId = user.getId();
            expect(user).to.be.instanceOf(User);
            expect(user.getEmail()).to.be.equal(`tesddddtupdate@gmail.com`);
            expect(user.getFirstName()).to.be.equal("oui");
            expect(user.getLastName()).to.be.equal("non");
            expect(user.getAddressId()).to.be.equal(16);
            expect(user.getPhoneNumber()).to.be.equal("66");
        });

        it("GetOne", async () => {
            const userDao = new UserDao();
            const user = await userDao.selectOne(deleteId);
            expect(user).to.be.instanceOf(User);
        });
        it("SelectUser", async () => {
            const userDao = new UserDao();
            const user = await userDao.selectUsersWhoHaveRoles(1);
            expect(user[0]).to.be.instanceOf(User);
        });
        it("Edit", async () => {
            const editedUser: User = new User(`testupdate@gmail.com`, "aa", "nzzon", "621451", new Date("800/10/02"), "12345", null, deleteId);
            const userDao = new UserDao();
            const user = await userDao.update(editedUser);
            expect(user).to.be.instanceOf(User);
            expect(user.getEmail()).to.be.equal(`testupdate@gmail.com`);
            expect(user.getFirstName()).to.be.equal("aa");
            expect(user.getLastName()).to.be.equal("nzzon");
            expect(user.getPhoneNumber()).to.be.equal("621451");
            expect(user.getAddressId()).to.be.equal(null);
        });
        it("delete", async () => {
            const userDao = new UserDao();
            const deleteSuccess = await userDao.delete(deleteId);
            expect(deleteSuccess).to.be.true;
        });
        it("linkAddressId", async () => {
            const userDao = new UserDao();
            const deleteSuccess = await userDao.linkAddressId(14, 5);
            expect(deleteSuccess).to.be.true;
        });
    });
});

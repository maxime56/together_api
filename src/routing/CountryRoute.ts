import { getCountrys, getCenterCountrys } from "../controller/CountryContorller";

const express = require("express");
const router = express.Router();

router.route("/getAll/").get(getCountrys);
router.route("/getCenterCountry/").get(getCenterCountrys);

module.exports = router;

USE boegbjzol9xmzfhwchc7;

INSERT INTO Address(postalCode, streetName, city, Country_id)
VALUES ("6811", "oui du chêne", 'Lyon', 1);

INSERT INTO User (email, firstName, lastName, phoneNumber, password, lastConnexion, Address_id)
VALUES ("camarchebien@gmail.com", "test", "oui", "8474844", "secret", NOW(), 1),
("azeazeaze@gmail.com", "azeaze", "azeaze", "azeaze", "zzzz", NOW(), 1);

INSERT INTO Center (email, name, phoneNumber, isActive, schedule, Address_id, Country_id) 
VALUES ("testmail", "Meilleur centre", "929292292", true, "19h50", 1, 1),
("testmail", "Meilleur centre", "929292292", true, "19h50", 1, 1);

INSERT INTO Offer (name, description, startDate, endDate, place, Center_id) VALUES
("testingOffer", "oui", NOW(), NOW(), 10, 1);

INSERT INTO Candidat (User_id, Offer_id, Offer_Center_id, landedDate, isAccepted, isActive) VALUES
(2,1,1,NOW(),true, true);

INSERT INTO Race (name)
VALUES ("humain");

INSERT INTO Species (name)
VALUES ("Chienne");

INSERT INTO Role (name)
VALUES ("Président"), ("Bénévole"), ("Vétérinaire");

INSERT INTO User_role (Role_id, User_id, active, expiration, Center_id)
VALUES (1, 1, NOW(), NOW(), 1),
(2, 1, NOW(), NOW(), 1),
(3, 1, NOW(), NOW(), 1);

INSERT INTO Center_role (Role_id, Center_id)
VALUES (1, 1), (2, 1), (3, 1);

INSERT INTO Animal (name, description, age, gender, birthDate, isActive, documentRef, Center_id, Race_id, Species_id) VALUES
("chien", "méchant", 10, "Male", NOW(), true, "ddde", 1, 1, 1),
("chien", "méchant", 10, "Male", NOW(), true, "ddde", 1, 1, 1),
("chien", "gentil", 10, "Femelle", NOW(), false, "azaa", 1, 1, 1);

INSERT INTO Picture (path, Animal_id, User_id, Offer_id, Center_id)
VALUES ("www/url", 1, null, null , null),
("www/url", null, 1, null , null),
("www/url", null, null, null , 1);

INSERT INTO Adoption (User_id, Animal_id, Animal_Center_id, isActive, adoptionDate) VALUES
(1, 1, 1, true, NOW()),
(1, 2, 1, false, NOW()),
(1, 3, 1, true, NOW());

INSERT INTO Center_role (Center_id, Species_id) VALUES (1, 1);
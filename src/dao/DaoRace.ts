
import { Race } from "../models/Race";
import Database from "../models/Database"

export class RaceDao extends Database {
    public sqlToRace(res): Race {
        if (res) {
            return new Race(
                res.name,
                res.id
            );
        }
    }
    /**
     * 
     * @param {Number} id 
     * @returns {Race}
     */
    public async selectOne(id: number): Promise<Race> {
        // this.getConnection();
        const query = `CALL select_race(${id})`;
        const res = await this.request(query);
        return this.sqlToRace(JSON.parse(JSON.stringify(res[0]))[0]);
    }
    /**
     * 
     * @param {Number} centerId 
     * @returns {Race}
     */
    public async select_all_races_by_center(centerId: number) {
        const query = `CALL select_all_races_by_center(${centerId})`;
        const res = await this.request(query);
        const result = [];
        res[0].forEach(element => {
            let i = this.sqlToRace(JSON.parse(JSON.stringify(element)));
            result.push(i);
        });
        return result;
    }
    /**
     * 
     * @param {Race} race 
     * @returns {Race}
     */
    public async create(race: Race): Promise<Race> {
        // à régler les procédures ne renvoie pas le dernier id
        let query = `INSERT INTO Race (name) VALUES ("${race.getName()}");`;
        let res = await this.request(query);
        return this.selectOne(res.insertId);
    }
    /**
    * 
    * @param {Race} race 
    * @returns {Race}
    */
    public async edit(race: Race): Promise<Race> {
        let query = `CALL update_race( "${race.getName()}", ${race.getId()})`;
        await this.request(query);
        return this.selectOne(race.getId());
    }
    /**
     * 
     * @param {Number} id 
     * @returns {Boolean}
     */
    public async delete(id: number) {
        const query = `CALL delete_race(${id})`;
        const res = await this.request(query);
        const deleteSuccess = res.affectedRows;
        return deleteSuccess === 1 ? true : false;
    }
    /**
     * 
     * @param {Number} centerId 
     * @returns {Boolean} Should delete all the races that it been add in a center & return a boolean 
     */
    public async deleteAllRaceByCenterId(centerId: number) {
        const query = `CALL delete_center_races(${centerId})`;
        const res = await this.request(query);
        const deleteSuccess = res.affectedRows;
        return deleteSuccess > 0 ? true : false;
    }
    /**
     * @param {Number} race 
     * @param {Number} centerId 
     * @return {Boolean} allows you to create a join table between centres and races
     */
    public async linkAnRaceInAnCenter(raceId: Number, centerId: Number): Promise<Boolean> {
        let query = `INSERT INTO Center_race (Center_id, Race_id) VALUES (${centerId}, ${raceId});`;
        let res = await this.request(query);
        return res.affectedRows > 0 ? true : false;
    }
    /**
     * @param {Number} race 
     * @param {Number} centerId 
     * @return {Boolean} 
     */
    public async deleteLinkedRaceInAnCenter(raceId: Number, centerId: Number): Promise<Boolean> {
        let query = `CALL delete_one_center_race(${centerId}, ${raceId});`;
        let res = await this.request(query);
        return res.affectedRows > 0 ? true : false;
    }
    /**
     * @return {[Race]} 
     */
    public async select_all_races(): Promise<Array<Race>> {
        let query = `CALL select_all_races();`;
        const res = await this.request(query);
        const result = [];
        res[0].forEach(element => {
            let i = this.sqlToRace(JSON.parse(JSON.stringify(element)));
            result.push(i);
        });
        return result;
    }
}
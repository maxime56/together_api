import { expect } from "chai";
import { AdminManager } from '../../src/manager/AdminManager';
import { Address } from "../../src/models/Address";
import { Adoption } from "../../src/models/Adoption";
import { Animal } from "../../src/models/Animal";
import { Candidat } from "../../src/models/Candidat";
import { Center } from "../../src/models/Center";
import { Offer } from "../../src/models/Offer";
import { Race } from "../../src/models/Race";
import { Role } from "../../src/models/Role";
import { Species } from "../../src/models/Species";
import { UserRole } from "../../src/models/UserRole";
import { Vaccine } from "../../src/models/Vaccine";
import { VaccinedAnimal } from "../../src/models/VaccinedAnimal";



describe("AdminManager", () => {
    const logic = new AdminManager();

    describe("CreateAnCenter", () => {
        it("Should create a center", async () => {
            const result = await logic.createAnCenter(
                new Center("fromCreateAnCenter", "AdminManager", "12345", true, "16h88", null, 1, null)
            );
            expect(result.Center).to.be.instanceOf(Center);
            expect(result.Center.getName()).to.be.equal("fromCreateAnCenter");
            expect(result.Center.getPhoneNumber()).to.be.equal("12345");
            expect(result.Center.getEmail()).to.be.equal("AdminManager");
            expect(result.Center.getIsActive()).to.be.equal(1);
        });
    });
    describe("createAnAnimal", () => {
        it("Should create a animal", async () => {
            const result = await logic.createAnAnimal(
                new Animal("name", "description", "10", "Male", new Date(Date.now()), true, "documentRef", 1, 1, 1)
            );
            expect(result.Animal).to.be.instanceOf(Animal);
            expect(result.Animal.getName()).to.be.equal("name");
            expect(result.Animal.getDescription()).to.be.equal("description");
            expect(result.Animal.getAge()).to.be.equal("10");
            expect(result.Animal.getGender()).to.be.equal("Male");
            expect(result.Animal.getIsActive()).to.be.equal(1);
            expect(result.Animal.getDocumentRef()).to.be.equal("documentRef");
            expect(result.Animal.getCenterId()).to.be.equal(1);
            expect(result.Animal.getRace()).to.be.equal(1);
            expect(result.Animal.getSpecies()).to.be.equal(1);
        });
    });
    describe("createAnAdoption", () => {
        it("Should create a adoption", async () => {
            const result = await logic.createAnAdoption(
                new Adoption(9, 5, 1, true, new Date(Date.now())), 1
            );
            expect(result.Adoption).to.be.instanceOf(Adoption);
            expect(result.Adoption.getAnimalId()).to.be.to.be.equal(5);
            expect(result.Adoption.getUserId()).to.be.to.be.equal(9);
        });
        it("Shouldn't create a adoption because the center id is not the same", async () => {
            const result = await logic.createAnAdoption(
                new Adoption(9, 5, 1, true, new Date(Date.now())), 5
            );
            expect(result.Adoption).to.be.equal(undefined);
        });
    });
    describe("createAnVaccinedAnimal", () => {
        it("Should create a VaccinedAnimal", async () => {
            const result = await logic.createAnVaccinedAnimal(
                new VaccinedAnimal(1, 8, new Date(Date.now()), new Date(Date.now()))
            );
            expect(result.VaccinedAnimal).to.be.instanceOf(VaccinedAnimal);
            expect(result.VaccinedAnimal.getAnimalId()).to.be.equal(8);
            expect(result.VaccinedAnimal.getVaccineId()).to.be.equal(1);
        });
    });
    describe("createAnRace", () => {
        it("Should create a createAnRace", async () => {
            const result = await logic.createAnRace(
                new Race("createAnRace")
            );
            expect(result.Race).to.be.instanceOf(Race);
            expect(result.Race.getName()).to.be.equal("createAnRace");
        });
    });
    describe("createAnSpecies", () => {
        it("Should create a createAnSpecies", async () => {
            const result = await logic.createAnSpecies(
                new Species("createAnSpecies")
            );
            expect(result.Species).to.be.instanceOf(Species);
            expect(result.Species.getName()).to.be.equal("createAnSpecies");
        });
    });
    describe("createAnCenterRace", () => {
        it("Should create a CenterRace", async () => {
            const result = await logic.createAnCenterRace(
                3, 2
            );
            expect(result).to.be.equal(true);
        });
        it("Shouldn't create a CenterRace because the centerid don't exist", async () => {
            const result = await logic.createAnCenterRace(
                50000, 2
            );
            expect(result).to.be.equal(false);
        });
        it("Shouldn't create a CenterRace because the raceId don't exist", async () => {
            const result = await logic.createAnCenterRace(
                1, 5000
            );
            expect(result).to.be.equal(false);
        });
    });
    describe("createAnCenterSpecies", () => {
        it("Should create a CenterSpecies", async () => {
            const result = await logic.createAnCenterSpecies(
                6, 2
            );
            expect(result).to.be.equal(true);
        });
        it("Shouldn't create a CenterSpecies because the centerid don't exist", async () => {
            const result = await logic.createAnCenterSpecies(
                50000, 2
            );
            expect(result).to.be.equal(false);
        });
        it("Shouldn't create a CenterSpecies because the speciesId don't exist", async () => {
            const result = await logic.createAnCenterSpecies(
                1, 2500
            );
            expect(result).to.be.equal(false);
        });
    });
    describe("createAnVaccine", () => {
        it("Should create a createAnVaccine", async () => {
            const result = await logic.createAnVaccine(
                new Vaccine("dddd")
            );
            expect(result.Vaccine).to.be.instanceOf(Vaccine);
            expect(result.Vaccine.getName()).to.be.equal("dddd");
        });
    });
    describe("createAnCenterVaccine", () => {
        it("Should create a createAnCenterVaccine", async () => {
            const result = await logic.createAnCenterVaccine(
                2, 5
            );
            expect(result).to.be.equal(true);
        });
        it("Shouldn't create a createAnCenterVaccine because the center id don't exist", async () => {
            const result = await logic.createAnCenterVaccine(
                500, 1
            );
            expect(result).to.be.equal(false);
        });
        it("Shouldn't create a createAnCenterVaccine because the vaccine id don't exist", async () => {
            const result = await logic.createAnCenterVaccine(
                1, 500
            );
            expect(result).to.be.equal(false);
        });
    });
    describe("createAnRole", () => {
        it("Should create a createAnRole", async () => {
            const result = await logic.createAnRole(
                new Role("createAnRole")
            );
            expect(result.Role).to.be.instanceOf(Role);
            expect(result.Role.getName()).to.be.equal("createAnRole");
        });
    });
    describe("createAnUserRole", () => {
        it("Should create a createAnUserRole", async () => {
            const result = await logic.createAnUserRole(
                new UserRole(2, 5, new Date(Date.now()), new Date(Date.now()), 1), 1
            );
            expect(result.UserRole).to.be.instanceOf(UserRole);
            expect(result.UserRole.getCenterId()).to.be.equal(1);
            expect(result.UserRole.getUserId()).to.be.equal(5);
            expect(result.UserRole.getRoleId()).to.be.equal(2);
        });
        it("Shouldn't create a AnUserRole because the center don't exist", async () => {
            const result = await logic.createAnUserRole(
                new UserRole(1, 5, new Date(Date.now()), new Date(Date.now()), 500), 500
            );
            expect(result.UserRole).to.be.equal(undefined);
        });
        it("Shouldn't create a UserRole because the CenterId is not the same", async () => {
            const result = await logic.createAnUserRole(
                new UserRole(1, 5, new Date(Date.now()), new Date(Date.now()), 1), 6
            );
            expect(result.UserRole).to.be.equal(undefined);
        });
    });
    describe("createAnOffer", () => {
        it("Should create a Offer", async () => {
            const result = await logic.createAnOffer(
                new Offer("createAnOffer", "createAnOffer", new Date(Date.now()), new Date(Date.now()), 10, 1), 1
            );
            expect(result.Offer).to.be.instanceOf(Offer);
        });
        it("Shouldn't create a Offer because the center don't exist", async () => {
            const result = await logic.createAnOffer(
                new Offer("createAnOffer", "createAnOffer", new Date(Date.now()), new Date(Date.now()), 10, 500), 500
            );
            expect(result.Offer).to.be.equal(undefined);
        });
        it("Shouldn't create a Offer because the CenterId is not the same", async () => {
            const result = await logic.createAnOffer(
                new Offer("createAnOffer", "createAnOffer", new Date(Date.now()), new Date(Date.now()), 10, 1), 500
            );
            expect(result.Offer).to.be.equal(undefined);
        });
    });
    describe("createAnCenterAddress", () => {
        it("Should create a CenterAddress", async () => {
            const result = await logic.createAnCenterAddress(
                new Address("createAnCenterAddress", "createAnCenterAddress", "city", 1), 7
            );
            expect(result.Address).to.be.instanceOf(Address);
            expect(result.Address.getCity()).to.be.equal("city");
            expect(result.Address.getPostalCode()).to.be.equal("createAnCenterAddress");
            expect(result.Address.getStreetName()).to.be.equal("createAnCenterAddress");
        });
        it("Shouldn't create a Address because the center don't exist", async () => {
            const result = await logic.createAnCenterAddress(
                new Address("createAnCenterAddress", "createAnCenterAddress", "city", 1), 8000
            );
            expect(result.Address).to.be.equal(undefined);
        });
        it("Shouldn't create a Address because the center has already a address", async () => {
            const result = await logic.createAnCenterAddress(
                new Address("createAnCenterAddress", "createAnCenterAddress", "city", 1), 1
            );
            expect(result.Address).to.be.equal(undefined);
        });
    });
    describe("CreateAnAnimal", async () => {
        it("Should create a animal", async () => {
            const result = await logic.createAnAnimal(
                new Animal("tracteur", "description zertour", "14", "Male", new Date(Date.now()), true, "ddd", 1, 1, 1)
            );
            expect(result.Animal.getName()).to.be.equal("tracteur");
            expect(result.Animal.getDescription()).to.be.equal("description zertour");
            expect(result.Animal.getAge()).to.be.equal("14");
            expect(result.Animal.getGender()).to.be.equal("Male");
            expect(result.Animal.getIsActive()).to.be.equal(1);
            expect(result.Animal.getDocumentRef()).to.be.equal("ddd");
            expect(result.Animal.getCenterId()).to.be.equal(1);
            expect(result.Animal.getSpecies()).to.be.equal(1);
            expect(result.Animal.getRace()).to.be.equal(1);
        });
    });

    describe("updateAnCenterAddress", () => {
        it("Should update a center linked", async () => {
            const result = await logic.updateAnCenterAddress(
                new Address("updateAnCenterAddress", "updateAnCenterAddress", "Lyon", 1, 13), 6
            );
            expect(result.Address).to.be.instanceOf(Address);
            expect(result.Address.getPostalCode()).to.be.equal("updateAnCenterAddress");
            expect(result.Address.getCity()).to.be.equal("Lyon");
            expect(result.Address.getStreetName()).to.be.equal("updateAnCenterAddress");
            expect(result.Address.getCountryId()).to.be.equal(1);
        });
        it("Shouldn't update a center linked because the center don't exist", async () => {
            const result = await logic.updateAnCenterAddress(
                new Address("updateAnCenterAddress", "updateAnCenterAddress", "Lyon", 1, 13), 800
            );
            expect(result.Address).to.be.equal(undefined);
        });
        it("Shouldn't update a center linked because the address don't exist", async () => {
            const result = await logic.updateAnCenterAddress(
                new Address("updateAnCenterAddress", "updateAnCenterAddress", "Lyon", 1, 500), 6
            );
            expect(result.Address).to.be.equal(undefined);
        });
    });
    describe("updateAnOffer", () => {
        it("Should update a center linked", async () => {
            const result = await logic.updateAnOffer(
                new Offer("updateAnOffer", "updateAnOffer", new Date(Date.now()), new Date(Date.now()), 1, 1, 1), 1
            );
            expect(result.Offer).to.be.instanceOf(Offer);
            expect(result.Offer.getName()).to.be.equal("updateAnOffer");
            expect(result.Offer.getDescription()).to.be.equal("updateAnOffer");
            expect(result.Offer.getAvailablePlace()).to.be.equal(1);
        });
        it("Shouldn't update a center linked because the center don't exist", async () => {
            const result = await logic.updateAnOffer(
                new Offer("updateAnOffer", "updateAnOffer", new Date(Date.now()), new Date(Date.now()), 1, 500, 1), 500
            );
            expect(result.Offer).to.be.equal(undefined);
        });
        it("Shouldn't update a center linked because the Offer don't exist", async () => {
            const result = await logic.updateAnOffer(
                new Offer("updateAnOffer", "updateAnOffer", new Date(Date.now()), new Date(Date.now()), 1, 1, 500), 1
            );
            expect(result.Offer).to.be.equal(undefined);
        });
        it("Shouldn't update a center linked because the Offer and the center Id is not the same ", async () => {
            const result = await logic.updateAnOffer(
                new Offer("updateAnOffer", "updateAnOffer", new Date(Date.now()), new Date(Date.now()), 1, 1, 1), 2
            );
            expect(result.Offer).to.be.equal(undefined);
        });
    });
    describe("updateAnCandidat", () => {
        it("Should update a candidat", async () => {
            const result = await logic.updateAnCandidat(
                new Candidat(3, 2, 1, new Date("2020/10/10"), false, true), 1
            );
            expect(result.Candidat).to.be.instanceOf(Candidat);
            expect(result.Candidat.getUserId()).to.be.equal(3);
            expect(result.Candidat.getOfferId()).to.be.equal(2);
            expect(result.Candidat.getIsAccepted()).to.be.equal(0);
            expect(result.Candidat.getIsActive()).to.be.equal(1);
            expect(result.Candidat.getLandedDate().toString().slice(0,10)).to.be.equal("2020-10-09");   
        });
        it("Shouldn't update a candidat because the center don't exist", async () => {
            const result = await logic.updateAnCandidat(
                new Candidat(8, 1, 500, new Date(Date.now()), false, true), 500
            );
            expect(result.Candidat).to.be.equal(undefined);
        });
        it("Shouldn't update a candidat because the user don't exist", async () => {
            const result = await logic.updateAnCandidat(
                new Candidat(3000, 1, 1, new Date(Date.now()), false, true), 1
            );
            expect(result.Candidat).to.be.equal(undefined);
        });
        it("Shouldn't update a candidat because the Candidat and the center Id is not the same ", async () => {
            const result = await logic.updateAnCandidat(
                new Candidat(8, 1, 500, new Date(Date.now()), false, true), 350
            );
            expect(result.Candidat).to.be.equal(undefined);
        });
    });
    describe("UpdateAnCenter", () => {
        it("Should update a center", async () => {
            const result = await logic.updateAnCenter(
                new Center("fromUpdateAnCenter", "AdminManager", "12345", true, "16h88", 4, 1, 2)
            );
            expect(result.Center.getName()).to.be.equal("fromUpdateAnCenter");
            expect(result.Center.getEmail()).to.be.equal("AdminManager");
            expect(result.Center.getPhoneNumber()).to.be.equal("12345");
            expect(result.Center.getIsActive()).to.be.equal(1);
            expect(result.Center.getSchedule()).to.be.equal("16h88");
            expect(result.Center.getAddressId()).to.be.equal(4);
            expect(result.Center.getId()).to.be.equal(2);
        });

    });
    describe("UpdateAnAnimal", async () => {
        it("Should update a animal", async () => {
            const result = await logic.updateAnAnimal(
                new Animal("tracteurr", "desecription zertour", "44", "Femelle", new Date(Date.now()), true, "ddd", 1, 1, 1, 8)
            );
            expect(result.Animal.getName()).to.be.equal("tracteurr");
            expect(result.Animal.getDescription()).to.be.equal("desecription zertour");
            expect(result.Animal.getAge()).to.be.equal("44");
            expect(result.Animal.getGender()).to.be.equal("Femelle");
            expect(result.Animal.getIsActive()).to.be.equal(1);
            expect(result.Animal.getDocumentRef()).to.be.equal("ddd");
            expect(result.Animal.getCenterId()).to.be.equal(1);
            expect(result.Animal.getSpecies()).to.be.equal(1);
            expect(result.Animal.getRace()).to.be.equal(1);
        });
    });
    describe("updateAnUserRole", async () => {
        it("Should update a UserRole", async () => {
            const result = await logic.updateAnUserRole(
                new UserRole(3, 1, new Date("2000/10/10"), new Date("2010/10/02"), 1)
            );
            expect(result.UserRole).to.be.instanceOf(UserRole);
            expect(result.UserRole.getUserId()).to.be.equal(1);
            expect(result.UserRole.getRoleId()).to.be.equal(3);
            expect(result.UserRole.getActive().toString().slice(0,10)).to.be.equal("2000-10-09");      
            expect(result.UserRole.getExpiration().toString().slice(0,10)).to.be.equal("2010-10-01");    
        });
        it("Shouldn't update a UserRole because it don't exist", async () => {
            const result = await logic.updateAnUserRole(
                new UserRole(200, 1, new Date("2000/10/10"), new Date("2010/10/02"), 1)
            );
            expect(result.UserRole).to.be.equal(undefined);
        });
    });
    describe("updateAnRole", async () => {
        it("Should update a Role", async () => {
            const result = await logic.updateAnRole(
                new Role("updateAnRole", 1)
            );
            expect(result.Role).to.be.instanceOf(Role);
            expect(result.Role.getName()).to.be.equal("updateAnRole");
        });
        it("Shouldn't update a Role because it don't exist", async () => {
            const result = await logic.updateAnRole(
                new Role("dddd", 5000)
            );
            expect(result.Role).to.be.equal(undefined);
        });
    });
    describe("updateAnVaccine", async () => {
        it("Should update a Vaccine", async () => {
            const result = await logic.updateAnVaccine(
                new Vaccine("updateAnVaccine", 1)
            );
            expect(result.Vaccine).to.be.instanceOf(Vaccine);
            expect(result.Vaccine.getName()).to.be.equal("updateAnVaccine");
        });
        it("Shouldn't update a Vaccine because it don't exist", async () => {
            const result = await logic.updateAnVaccine(
                new Vaccine("dddd", 5000)
            );
            expect(result.Vaccine).to.be.equal(undefined);
        });
    });
    describe("updateAnRace", async () => {
        it("Should update a Race", async () => {
            const result = await logic.updateAnRace(
                new Race("updateAnRace", 1)
            );
            expect(result.Race).to.be.instanceOf(Race);
            expect(result.Race.getName()).to.be.equal("updateAnRace");
        });
        it("Shouldn't update a Race because it don't exist", async () => {
            const result = await logic.updateAnRace(
                new Race("dddd", 5000)
            );
            expect(result.Race).to.be.equal(undefined);
        });
    });
    describe("updateAnSpecies", async () => {
        it("Should update a Species", async () => {
            const result = await logic.updateAnSpecies(
                new Species("updateAnSpecies", 1)
            );
            expect(result.Species).to.be.instanceOf(Species);
            expect(result.Species.getName()).to.be.equal("updateAnSpecies");
        });
        it("Shouldn't update a Species because it don't exist", async () => {
            const result = await logic.updateAnSpecies(
                new Species("dddd", 5000)
            );
            expect(result.Species).to.be.equal(undefined);
        });
    });
    describe("updateAnVaccinedAnimal", async () => {
        it("Should update a VaccinedAnimal", async () => {
            const result = await logic.updateAnVaccinedAnimal(
                new VaccinedAnimal(1, 8, new Date("2000/10/10"), new Date("2020/10/10"))
            );
            expect(result.VaccinedAnimal).to.be.instanceOf(VaccinedAnimal);
            expect(result.VaccinedAnimal.getVaccineId()).to.be.equal(1);
            expect(result.VaccinedAnimal.getAnimalId()).to.be.equal(8);
            expect(result.VaccinedAnimal.getVaccinedDate().toString().slice(0,10)).to.be.equal("2000-10-09");
            expect(result.VaccinedAnimal.getNextVaccination().toString().slice(0,10)).to.be.equal("2020-10-09");   
        });
        it("Shouldn't update a VaccinedAnimal because it don't exist", async () => {
            const result = await logic.updateAnVaccinedAnimal(
                new VaccinedAnimal(80, 8, new Date("2000/10/10"), new Date("2020/10/10"))
            );
            expect(result.VaccinedAnimal).to.be.equal(undefined);
        });
    });
    
    describe("DeleteAnCenter", async () => {
        it("Should delete a animal with his pictures, adoptions if he get some and the informations about his vaccines", async () => {
            const result = await logic.deleteAnCenter(new Center("fromUpdateAnCenter", "AdminManager", "12345", true, "16h88", 6, 1, 3));
            expect(result).to.be.equal(true);
        });
    });
    describe("DeleteAnAnimal", async () => {
        it("Should delete a animal with his pictures, adoptions if he get some and the informations about his vaccines", async () => {
            const result = await logic.deleteAnAnimal(new Animal("chien", "gentil", "10", "Femelle", new Date(Date.now()), false, "azaa", 1, 1, 1, 3));
            expect(result).to.be.equal(true);
        });
    });
});

import { Address } from "../models/Address";
import Database from "../models/Database";

export class AddressDao extends Database {

    public sqlToAddress(res): Address {
        if (res) {
            return new Address(
                res.postalCode,
                res.streetName,
                res.city,
                res.Country_id,
                res.id
            );
        }
    }
    /**
     * @param {number} addressId 
     * @returns {Address} Should return one address by his id
     */
    public async selectOne(addressId: number): Promise<Address> {
        // this.getConnection();
        const query = `CALL select_address(${addressId})`;
        const res = await this.request(query);
        return this.sqlToAddress(JSON.parse(JSON.stringify(res[0]))[0]);
    }
    /**
     * @param {Address} address 
     * @returns {Address} Should return the created Address
     */
    public async create(address: Address): Promise<Address> {
        // CALL new_address(${address.postalCode}, "${address.streetName}", "${address.city}" );
        let query = `INSERT INTO Address (postalCode, streetName, city, Country_id) VALUES ("${address.getPostalCode()}", "${address.getStreetName()}", "${address.getCity()}", ${address.getCountryId()});`;
        let res = await this.request(query);
        return this.selectOne(res.insertId);
    }
    /**
     * @param {Address} address 
     * @returns {Address} Should update one Address and return it
     */
    public async edit(address: Address): Promise<Address> {
        let query = `CALL update_address("${address.getPostalCode()}", "${address.getStreetName()}", "${address.getCity()}", ${address.getCountryId()}, ${address.getId()})`;
        await this.request(query);
        return this.selectOne(address.getId());
    }
    /**
     * @param {Number} addressId 
     * @returns {Boolean} 
     */
    public async delete(addressId: number) {
        const query = `CALL delete_address(${addressId})`;
        const res = await this.request(query);
        const deleteSuccess = res.affectedRows;
        return deleteSuccess <= 2 ? true : false;
    }
}
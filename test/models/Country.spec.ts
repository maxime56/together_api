import { expect } from "chai";
import { Country } from "../../src/models/Country";

describe('Country Models Testing Getter & Setter', () => {
    let country: Country;
    beforeEach(() => {
        country = new Country("France",1);
    });
    describe('Getter Setter Name', () => {
        it('getName', () => {
            expect(country.getName()).to.be.equal("France");
        });
        it("setName", () => {
            country.setName("Covidus")
            expect(country.getName()).to.be.equal("Covidus");
        });
        it("getId", () => {
            expect(country.getId()).to.be.equal(1);
        });
    });
});

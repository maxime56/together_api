export class Animal {
    private id: number;
    private name: string;
    private age: string;
    private gender: string;
    private birthDate: Date;   
    private isActive:boolean;
    private documentRef: string;
    private description: string;
    private CenterId: number;
    private SpeciesId: number;
    private RaceId: number;
    constructor(name: string, description: string, age: string, gender:string, birthDate:Date, isActive:boolean, documentRef:string, CenterId: number, Species: number, Race: number, id?: number) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.description = description;
        this.CenterId = CenterId;
        this.RaceId = Race;
        this.SpeciesId = Species;
        this.gender = gender;
        this.birthDate = birthDate;   
        this.isActive = isActive;
        this.documentRef = documentRef;
    }
    public getId(): number {
        return this.id;
    }
    public setId(id: number): void {
        this.id = id;
    }
    public getName(): string {
        return this.name;
    }
    public setName(name: string): void {
        this.name = name;
    }
    public getAge(): string {
        return this.age;
    }
    public setAge(age: string): void {
        this.age = age;
    }
    public getDescription(): string {
        return this.description;
    }
    public setDescription(description: string): void {
        this.description = description;
    }
    public getCenterId(): number {
        return this.CenterId;
    }
    public setCenterId(CenterId: number): void {
        this.CenterId = CenterId;
    }
    public getSpecies(): number {
        return this.SpeciesId;
    }
    public setSpecies(Species: number): void {
        this.SpeciesId = Species;
    }
    public getRace(): number {
        return this.RaceId;
    }
    public setRace(Race: number): void {
        this.RaceId = Race;
    }
    public getGender(): string {
        return this.gender;
    }
    public setGender(gender: string): void {
        this.gender = gender;
    }
    public getBirthDate(): Date {
        return this.birthDate;
    }
    public setBirthDate(birthDate: Date): void {
        this.birthDate = birthDate;
    }
    public getIsActive(): boolean {
        return this.isActive;
    }
    public setIsActive(isActive: boolean): void {
        this.isActive = isActive;
    }
    public getDocumentRef(): string {
        return this.documentRef;
    }
    public setDocumentRef(documentRef: string): void {
        this.documentRef = documentRef;
    }
}

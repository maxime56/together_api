
import { User } from "../models/User";
import Database from "../models/Database";
const bcrypt = require('bcrypt');
const saltRounds = 10;
const moment = require("moment");

export class UserDao extends Database {
    public sqlToUser(res): User {
        if (res) {
            return new User(
                res.email,
                res.firstName,
                res.lastName,
                res.phoneNumber,
                res.lastConnexion,
                res.password,
                res.Address_id,
                res.id,
            );
        }
    }
    /**
     * 
     * @param {number} userId 
     * @returns {User}
     */
    public async selectOne(userId: number): Promise<User> {
        // this.getConnection();
        const query = `CALL select_user(${userId})`;
        const res = await this.request(query);
        return this.sqlToUser(JSON.parse(JSON.stringify(res[0]))[0]);
    }
    /**
     * 
     * @param {Number} centerId 
     * @returns {User} Should return all users that have a role in the center
     */
    public async selectUsersWhoHaveRoles(centerId: number) {
        const query = `CALL select_user_with_role_by_center(${centerId})`;
        const res = await this.request(query);
        const result = [];
        res[0].forEach(element => {
            let i = this.sqlToUser(JSON.parse(JSON.stringify(element)));
            result.push(i);
        });
        return result;
    }
    /**
     * 
     * @param {User} user 
     * @returns {User}
     */
    public async create(user: User): Promise<User> {
        // hashage du mot de passe
        let hash = await bcrypt.hash(user.getPassword(), saltRounds);
        user.setPassword(hash);
        // fin du hash
        let query = `INSERT INTO User (email, firstName, lastName, phoneNumber, password, lastConnexion, Address_id) VALUES 
        ("${user.getEmail()}", "${user.getFirstName()}", "${user.getLastName()}", "${user.getPhoneNumber()}", "${user.getPassword()}", 
        ("${moment().format("YYYY/MM/DD")}"), ${user.getAddressId()});`;
        let res = await this.request(query);
        return this.selectOne(res.insertId);
    }
    /**
    * 
    * @param {User} user 
    * @returns {User}
    */
    public async update(user: User): Promise<User> {
        // hashage du mot de passe si le mot de passe est évélé (cela indique que le mot de passe a été hashé dans le cas contraire on le hash)
        if (user.getPassword().length <= 45) {
            let hash = await bcrypt.hash(user.getPassword(), saltRounds);
            user.setPassword(hash);
        }
        // fin du hash
        let query = `CALL update_user("${user.getEmail()}", "${user.getFirstName()}", "${user.getLastName()}", "${user.getPhoneNumber()}", 
        "${user.getPassword()}", ${user.getAddressId()}, ${user.getId()})`;
        try {
            await this.request(query);
        } catch (error) {

        }
        return this.selectOne(user.getId());
    }
    /**
     * 
     * @param {Number} id 
     * @returns {Boolean} 
     */
    public async delete(id: number) {
        const query = `CALL delete_user(${id})`;
        const res = await this.request(query);
        const deleteSuccess = res.affectedRows;
        return deleteSuccess === 1 ? true : false;
    }
    /**
     * 
     * @param {number} AddresId 
     * @param {number} id
     * @returns {Boolean} should update the row of addressId for link a address to a user
     */
    public async linkAddressId(AddresId: number, id: number) {
        const query = `CALL link_user_addressId(${AddresId}, ${id})`;
        const res = await this.request(query);
        const deleteSuccess = res.affectedRows;
        return deleteSuccess === 1 ? true : false;
    }
    /**
     * @param {string} email 
     * @returns 
     */
    async login(email: string) {
        const query = `SELECT * FROM User WHERE email = "${email}";`;
        const request = await this.request(query);
        return request;
    }
}
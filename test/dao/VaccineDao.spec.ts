import { expect } from "chai";
import { Vaccine } from "../../src/models/Vaccine";
import { VaccineDao } from "../../src/dao/DaoVaccine";


describe("Vaccine DAO", () => {
    let VaccineId: number;
    // Test 1
    describe("SQL response to Vaccine", () => {
        let vaccineDao = new VaccineDao();
        // Données fictives renvoyés par la base de données.
        const res = {
            name: "test",
            id: 50
        };
        const vaccine: Vaccine = vaccineDao.sqlToVaccine(res);
        it("Name", () => {
            expect(vaccine.getName()).to.be.equal("test");
        });
        it("id", () => {
            expect(vaccine.getId()).to.be.equal(50);
        });
    });

    // Test de l'intégration du DAO avec la base de données.
    describe("DAO integration", () => {
        it("New", async () => {
            const newVaccine: Vaccine = new Vaccine("TDDUS", 1);
            const vaccineDao = new VaccineDao();
            const vaccine = await vaccineDao.create(newVaccine);
            expect(vaccine).to.be.instanceOf(Vaccine);
            expect(vaccine.getName()).to.be.equal("TDDUS");
            // récupérer le dernier id pour l'edit et le delete par la suite
            VaccineId = vaccine.getId();
        });
        it("selectOne", async () => {
            const vaccineDao = new VaccineDao();
            const vaccine = await vaccineDao.selectOne(VaccineId);
            expect(vaccine).to.be.instanceOf(Vaccine);
        });
        it("selectVaccineByCenter", async () => {
            const vaccineDao = new VaccineDao();
            const vaccine = await vaccineDao.selectVaccineByCenter(1);
            expect(vaccine[0]).to.be.instanceOf(Vaccine);
        });
        it("selectVaccines", async () => {
            const vaccineDao = new VaccineDao();
            const vaccine = await vaccineDao.selectVaccines();
            expect(vaccine[0]).to.be.instanceOf(Vaccine);
        });
        it("Edit", async () => {
            const editedVaccine: Vaccine = new Vaccine("EDDITDDUS", VaccineId);
            const vaccineDao = new VaccineDao();
            const vaccine = await vaccineDao.edit(editedVaccine);
            expect(vaccine).to.be.instanceOf(Vaccine);
            expect(vaccine.getName()).to.be.equal("EDDITDDUS");
        });
        it("Delete", async () => {
            const vaccineDao = new VaccineDao();
            const deleteSuccess = await vaccineDao.delete(VaccineId);
            expect(deleteSuccess).to.be.true;
        });
        it("deleteCenterVaccine", async () => {
            const vaccineDao = new VaccineDao();
            const deleteSuccess = await vaccineDao.deleteCenterVaccine(5);
            expect(deleteSuccess).to.be.true;
        });
        it("linkAnVaccineInAnCenter", async () => {
            const vaccineDao = new VaccineDao();
            const deleteSuccess = await vaccineDao.linkAnVaccineInAnCenter(2, 3);
            expect(deleteSuccess).to.be.true;
        });
        it("deleteLinkedVaccineInAnCenter", async () => {
            const vaccineDao = new VaccineDao();
            const deleteSuccess = await vaccineDao.deleteLinkedVaccineInAnCenter(2, 3);
            expect(deleteSuccess).to.be.true;
        });
    });
});

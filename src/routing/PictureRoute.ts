import { createAnimalPicture, createCenterPicture, createOfferPicture, createUserPicture, deleteAnimalPicture, deleteCenterPicture, deleteOfferPicture, deleteUserPicture } from "../controller/PictureController";

const express = require("express");
const router = express.Router();
const checkToken = require("../middleware/tchekToken");
const centerActionHandler = require("../middleware/centerActionHandler");


router.route("/createUserPicture/").post(checkToken, createUserPicture); // user
router.route("/createOfferPicture/").post(centerActionHandler, checkToken, createOfferPicture); // admin
router.route("/createCenterPicture/").post(centerActionHandler, checkToken, createCenterPicture); // admin
router.route("/createAnimalPicture/").post(centerActionHandler, checkToken, createAnimalPicture); //admin

router.route("/deleteUserPicture/").post(checkToken, deleteUserPicture);    // user
router.route("/deleteOfferPicture/").post(centerActionHandler, checkToken, deleteOfferPicture);  //admin
router.route("/deleteCenterPicture/").post(centerActionHandler, checkToken, deleteCenterPicture);    //admin
router.route("/deleteAnimalPicture/").post(centerActionHandler, checkToken, deleteAnimalPicture);    //admin

module.exports = router;
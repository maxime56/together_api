import { expect } from "chai";
import { Offer } from "../../src/models/Offer";
import { OfferDao } from "../../src/dao/DaoOffer";
import { DaoHandler } from "../../src/dao/DaoHandler";

describe("Offer DAO", () => {
    let deleteId: number;
    // Test 1
    describe("SQL response to offer", () => {
        let offerDao = new OfferDao();

        // Données fictives renvoyés par la base de données.
        const res = {
            name: "Test Offer",
            description: "testmail@gmail.com",
            startDate: new Date("1/1/1999"),
            endDate: new Date("1/1/1999"),
            place: 1,
            Center_id: 2,
            id: 1
        };
        const offer: Offer = offerDao.sqlToOffer(res);
        it("Id", () => {
            expect(offer.getId()).to.be.equal(1);
        });
        it("Name", () => {
            expect(offer.getName()).to.be.equal("Test Offer");
        });
        it("Description", () => {
            expect(offer.getDescription()).to.be.equal("testmail@gmail.com");
        });
        it("DateStart", () => {
            expect(offer.getStartingDate()).to.be.instanceOf(Date);
        });
        it("DateEnd", () => {
            expect(offer.getEndingDate()).to.be.instanceOf(Date);
        });
        it("Place", () => {
            expect(offer.getAvailablePlace()).to.be.equal(1);
        });
        it("Center_id", () => {
            expect(offer.getCenterId()).to.be.equal(2);
        });
    });
    // Test de l'intégration du DAO avec la base de données.
    describe("DAO integration", () => {
        it("New", async () => {
            const newOffer: Offer = new Offer("new", "mailedit@gmail.com", new Date("10/02/1999"), new Date("10/02/1999"), 1, 3);
            const offerDao = new OfferDao();
            const offer = await offerDao.create(newOffer);
            // récupération de l'id qui vient d'être créé pour le supprimer
            deleteId = offer.getId();
            expect(offer).to.be.instanceOf(Offer);
            expect(offer.getName()).to.be.equal("new");
            expect(offer.getDescription()).to.be.equal("mailedit@gmail.com");
            expect(offer.getStartingDate().toString().slice(0,10)).to.be.equal("1999-10-01");  
            expect(offer.getEndingDate().toString().slice(0,10)).to.be.equal("1999-10-01");  
            expect(offer.getAvailablePlace()).to.be.equal(1);
            expect(offer.getCenterId()).to.be.equal(3);
        });
        it("Get", async () => {
            const offerDao = new OfferDao();
            const offer = await offerDao.selectOne(1);
            expect(offer).to.be.instanceOf(Offer);
        });
        it("GetAll", async () => {
            const offerDao = new OfferDao();
            const offer = await offerDao.selectAll(1);
            expect(offer[0]).to.be.instanceOf(Offer);
        });
        it("Get Offers by Center", async () => {
            const offerDao = new OfferDao();
            const offer = await offerDao.selectCenterOffers(1);
            expect(offer[0]).to.be.instanceOf(Offer);
        });
        it("Edit", async () => {
            const editedOffer: Offer = new Offer("edit", "mailedit@gmail.com",  new Date("10/02/1999"),  new Date("10/02/1999"), 7, 1, deleteId);
            const offerDao = new OfferDao();
            const offer = await offerDao.edit(editedOffer);
            expect(offer).to.be.instanceOf(Offer);
            expect(offer.getName()).to.be.equal("edit");
            expect(offer.getDescription()).to.be.equal("mailedit@gmail.com");
            expect(offer.getStartingDate().toString().slice(0,10)).to.be.equal("1999-10-01");  
            expect(offer.getEndingDate().toString().slice(0,10)).to.be.equal("1999-10-01");  
            expect(offer.getAvailablePlace()).to.be.equal(7);
            expect(offer.getCenterId()).to.be.equal(1);
        });
        it("delete", async () => {
            const offerDao = new OfferDao();
            const deleteSuccess = await offerDao.delete(deleteId);
            expect(deleteSuccess).to.be.true;
        });
        it("deleteAllOffersByCenter", async () => {
            const offerDao = new OfferDao();
            const deleteSuccess = await offerDao.deleteAllOffersByCenter(5);
            expect(deleteSuccess).to.be.true;
        });
    });
});

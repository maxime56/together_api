export class Center {
    private id:number;
    private name:string;
    private email:string;
    private phoneNumber: string
    private isActive:boolean;
    private schedule:string;
    private AddressId:number;
    private CountryId:number;
    constructor(name: string, email: string, phoneNumber:string, isActive:boolean, schedule:string,  AddressId?:number, CountryId?:number, id?: number) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.isActive = isActive;
        this.schedule = schedule;
        this.AddressId = AddressId;
        this.CountryId = CountryId;
    }
    public getId(): number {
        return this.id;
    }
    public getName(): string {
        return this.name;
    }
    public setName(name: string): void {
        this.name = name;
    }
    public getEmail(): string {
        return this.email;
    }
    public setEmail(email: string): void {
        this.email = email;
    }
    public getPhoneNumber(): string {
        return this.phoneNumber;
    }
    public setPhoneNumber(phoneNumber): void {
        this.phoneNumber = phoneNumber;
    }
    public getAddressId(): number {
        return this.AddressId;
    }
    public setAddressId(AddressId: number): void {
        this.AddressId = AddressId;
    }
    public getIsActive(): boolean {
        return this.isActive;
    }
    public setIsActive(isActive: boolean): void {
        this.isActive = isActive;
    }
    public getSchedule(): string {
        return this.schedule;
    }
    public setSchedule(schedule: string): void {
        this.schedule = schedule;
    }
    public getCountryId(): number {
        return this.CountryId;
    }
    public setCountryId(CountryId: number): void {
        this.CountryId = CountryId;
    }
}

import { expect } from "chai";
import { Vaccine } from "../../src/models/Vaccine";

describe('Vaccine Models Testing Getter & Setter', () => {
    let vaccine: Vaccine;
    beforeEach(() => {
        vaccine = new Vaccine("AntiCovidus",1);
    });
    describe('Getter Setter Name', () => {
        it('getName', () => {
            expect(vaccine.getName()).to.be.equal("AntiCovidus");
        });
        it("setName", () => {
            vaccine.setName("AntiCovidous");
            expect(vaccine.getName()).to.be.equal("AntiCovidous");
        });
        it("getId", () => {
            expect(vaccine.getId()).to.be.equal(1);
        });
    });
});


import { expect } from "chai";
import { Center } from "../../src/models/Center";

describe('Center Models Testing Getter & Setter', () => {
    let center: Center;
    beforeEach(() => {
        center = new Center("testName", "test@mail.com", "060505040", true, "on est bien ouvert",1, 1, 1);
    });
    describe('Getter Id', () => {
        it('getId', () => {
            expect(center.getId()).to.be.equal(1);
        });
    });
    describe('Getter Setter Name', () => {
        it('getName', () => {
            expect(center.getName()).to.be.equal("testName");
        });
        it("setName", () => {
            center.setName("testNamed");
            expect(center.getName()).to.be.equal("testNamed");
        });
    });
    describe('Getter Setter Email', () => {
        it('getEmail', () => {
            expect(center.getEmail()).to.be.equal("test@mail.com");
        });
        it("setEmail", () => {
            center.setEmail("testNamed");
            expect(center.getEmail()).to.be.equal("testNamed");
        });
    });
    describe('Getter Setter PhoneNumber', () => {
        it('getPhoneNumber', () => {
            expect(center.getPhoneNumber()).to.be.equal("060505040");
        });
        it("setPhoneNumber", () => {
            center.setPhoneNumber("515155");
            expect(center.getPhoneNumber()).to.be.equal("515155");
        });
    });
    describe('Getter Setter IsActive', () => {
        it('getIsActive', () => {
            expect(center.getIsActive()).to.be.equal(true);
        });
        it("setIsActive", () => {
            center.setIsActive(false);
            expect(center.getIsActive()).to.be.equal(false);
        });
    });
    describe('Getter Setter Schedule', () => {
        it('getSchedule', () => {
            expect(center.getSchedule()).to.be.equal("on est bien ouvert");
        });
        it("setSchedule", () => {
            center.setSchedule("on est pas bien ouvert");
            expect(center.getSchedule()).to.be.equal("on est pas bien ouvert");
        });
    });
    describe('Getter Setter AddressId', () => {
        it('getAddressId', () => {
            expect(center.getAddressId()).to.be.equal(1);
        });
        it("setIsAccepted", () => {
            center.setAddressId(2);
            expect(center.getAddressId()).to.be.equal(2);
        });
    });
});


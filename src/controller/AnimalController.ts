import { Request, Response } from "express";
import { AdminManager } from "../manager/AdminManager";
import { CenterManager } from "../manager/CenterManager";
import { Animal } from "../models/Animal";

const adminManager = new AdminManager();
const centerManager = new CenterManager();

export const create = async (req: Request, res: Response) => {
    const reqAnimal = req.body.Animal;
    const reqCenter = req.body.Center;
    const result = await adminManager.createAnAnimal(
        new Animal(reqAnimal.name, reqAnimal.description, reqAnimal.age, reqAnimal.gender, reqAnimal.birthDate, reqAnimal.isActive, reqAnimal.documentRef, reqCenter.id, reqAnimal.SpeciesId, reqAnimal.RaceId),
    );
    if (result.Animal) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to create a animal"});
};

export const update = async (req: Request, res: Response) => {
    const reqAnimal = req.body.Animal;
    const reqCenter = req.body.Center;
    const result = await adminManager.updateAnAnimal(
        new Animal(reqAnimal.name, reqAnimal.description, reqAnimal.age, reqAnimal.gender, reqAnimal.birthDate, reqAnimal.isActive, reqAnimal.documentRef, reqCenter.id, reqAnimal.SpeciesId, reqAnimal.RaceId, reqAnimal.id)
    );
    if (result.Animal) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to update a animal"});
};

export const getAnimalsByCenter = async (req: Request, res: Response) => {
    const result = await centerManager.getAnimalsByCenter(req.query.CenterId, req.query.offset, req.query.limit);
    return res.status(200).send(result);
};

export const deleting = async (req: Request, res: Response) => {
    const id: any = req.query.id;
    const result = await adminManager.deleteAnAnimal(new Animal("ddd", "ddd", "dd", "dd", new Date(new Date()), true, "dd", 1, 1, 1 , id));
    if (result) return res.status(200).send(true)
    return res.status(400).send({Error: "Failed to delete a animal"});
};


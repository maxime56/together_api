
import { Species } from "../models/Species";
import Database from "../models/Database"

export class SpeciesDao extends Database {
    public sqlToSpecies(res): Species {
        if (res) {
            return new Species(
                res.name,
                res.id
            );
        }
    }
    /**
     * 
     * @param {Number} id 
     * @returns {Species}
     */
    public async selectOne(id: number): Promise<Species> {
        // this.getConnection();
        const query = `CALL select_specie(${id})`;
        const res = await this.request(query);
        return this.sqlToSpecies(JSON.parse(JSON.stringify(res[0]))[0]);
    }
    /**
     * 
     * @param {Number} idCenter 
     * @returns {Species} Should return all Species available in a center
     */
    public async select_all_species_by_center(idCenter: number) {
        const query = `CALL select_all_species_by_center(${idCenter})`;
        const res = await this.request(query);
        const result = [];
        res[0].forEach(element => {
            let i = this.sqlToSpecies(JSON.parse(JSON.stringify(element)));
            result.push(i);
        });
        return result;
    }
    /**
     * 
     * @param {Species} species 
     * @returns {Species}
     */
    public async create(species: Species): Promise<Species> {
        // à régler les procédures ne renvoie pas le dernier id
        let query = `INSERT INTO Species (name) VALUES ("${species.name}");`;
        let res = await this.request(query);
        return this.selectOne(res.insertId);
    }
    /**
     * 
     * @param {Species} species 
     * @returns {Species}
     */
    public async edit(species: Species): Promise<Species> {
        let query = `CALL update_species( "${species.getName()}", ${species.getId()})`;
        await this.request(query);
        return this.selectOne(species.getId());
    }
    /**
     * 
     * @param {Number} id 
     * @returns {Boolean}
     */
    public async delete(id: number) {
        const query = `CALL delete_species(${id})`;
        const res = await this.request(query);
        const deleteSuccess = res.affectedRows;
        return deleteSuccess === 1 ? true : false;
    }
    /**
    * 
    * @param {Number} centerId 
    * @returns {Boolean} Should delete all the species that it been linked in a center
    */
    public async deleteAllSpeciesByCenter(centerId: number) {
        const query = `CALL delete_center_species(${centerId})`;
        const res = await this.request(query);
        const deleteSuccess = res.affectedRows;
        return deleteSuccess > 0 ? true : false;
    }
    /**
 * @param {Number} speciesId 
 * @param {Number} centerId 
 * @return {Boolean} allows you to create a join table between centers and species
 */
    public async linkAnSpeciesInAnCenter(speciesId: Number, centerId: Number): Promise<Boolean> {
        let query = `INSERT INTO Center_species (Center_id, Species_id) VALUES (${centerId}, ${speciesId});`;
        let res = await this.request(query);
        return res.affectedRows > 0 ? true : false;
    }
    /**
 * @param {Number} speciesId 
 * @param {Number} centerId 
 * @return {Boolean} 
 */
    public async deleteLinkedSpeciesInAnCenter(speciesId: Number, centerId: Number): Promise<Boolean> {
        let query = `CALL delete_one_center_species(${centerId}, ${speciesId});`;
        let res = await this.request(query);
        return res.affectedRows > 0 ? true : false;
    }
    /**
 * @return {[Species]} 
 */
    public async select_all_species(): Promise<Array<Species>> {
        let query = `CALL select_all_species();`;
        const res = await this.request(query);
        const result = [];
        res[0].forEach(element => {
            let i = this.sqlToSpecies(JSON.parse(JSON.stringify(element)));
            result.push(i);
        });
        return result;
    }
}
import { Request, Response } from "express";
import { AdminManager } from "../manager/AdminManager";
import { CenterManager } from "../manager/CenterManager";
import { Role } from "../models/Role";
import { UserRole } from "../models/UserRole";


const adminManager = new AdminManager();
const centerManager = new CenterManager();


export const create = async (req: Request, res: Response) => {
    const reqRole = req.body.Role;

    const result = await adminManager.createAnRole(new Role(reqRole.name));
    if (result) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to create a role"});
};


export const linkedAnRole = async (req: Request, res: Response) => {
    const reqUserRole = req.body.UserRole;
    const reqCenter = req.body.Center;
    const result = await adminManager.createAnUserRole(new UserRole(reqUserRole.RoleId, reqUserRole.UserId, reqUserRole.active, reqUserRole.expiration, reqUserRole.CenterId), reqCenter.id);
    if (result) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to linked a role"});
};


export const deletelinkedAnRole = async (req: Request, res: Response) => {
    const reqRoleId:any = req.query.RoleId;
    const reqUserId:any = req.query.UserId;

    const result = await adminManager.deleteAnUserRole(reqRoleId, reqUserId);
    if (result) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to delete a role linked"});
};


export const deleting = async (req: Request, res: Response) => {
    const reqRole:any = req.query.RoleId;

    const result = await adminManager.deleteAnRole(reqRole);
    if (result) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to delete a role"});
};

export const updatelinkedAnRole = async (req: Request, res: Response) => {
    const reqUserRole = req.body.UserRole;

    const result = await adminManager.updateAnUserRole(new UserRole(reqUserRole.RoleId, reqUserRole.UserId, reqUserRole.active, reqUserRole.expiration, reqUserRole.CenterId));
    if (result) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to update a role linked"});
};

export const update = async (req: Request, res: Response) => {
    const reqRole = req.body.Role;
    const result = await adminManager.updateAnRole(new Role(reqRole.name, reqRole.id));
    if (result) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to update a role"});
};

export const getRoles = async (req: Request, res: Response) => {
    const result = await centerManager.getRoles();
    if (result.Roles[0]) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to get roles"});
};


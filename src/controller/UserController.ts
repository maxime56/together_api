import { Request, Response } from "express";
import { UserDao } from "../dao/DaoUser";
import { UserManager } from "../manager/UserManager";
import { Candidat } from "../models/Candidat";
import { User } from "../models/User";



const userManager = new UserManager();
const userRoleHandler = require("../middleware/userRoleHandler");
const bcrypt = require('bcrypt');
const generateToken = require("../middleware/generateToken");


export const create = async (req: Request, res: Response) => {
    const reqUser = req.body.User;

    const result = await userManager.createAnUser(
        new User(reqUser.email, reqUser.firstName, reqUser.lastName, reqUser.phoneNumber, reqUser.lastConnexion, reqUser.password, null)
    );
    if (result.user !== undefined) return res.status(200).send({ Succes: "The user has been created !" });
    return res.status(400).send({Error: "Email is already used !"});
};

export const update = async (req: Request, res: Response) => {
    const reqUser = req.body.User;

    const result = await userManager.updateAnUser(
        new User(reqUser.email, reqUser.firstName, reqUser.lastName, reqUser.phoneNumber, reqUser.lastConnexion, reqUser.password, reqUser.AddressId, reqUser.id)
    );
    if (result.user !== undefined) return res.status(200).send(result);
    return res.status(400).send({ Error: "Failed to update a user" });
};

export const getOne = async (req: Request, res: Response) => {
    const result = await userManager.getAnUser(req.query.id);
    return res.status(200).send(result);
};

export const login = async (req: Request, res: Response) => {
    console.log(req.body);    
    const reqUser = req.body.User;
    const userDao = new UserDao();
    try {
        await userDao.login(reqUser.email).then(async resp => {
            if (resp.length !== 0) {
                let compare = await bcrypt.compareSync(reqUser.password, resp[0].password); // on compare le mot de passe qui est envoyé, par le mot de passe qui est stocké dans la bdd
                if (compare) {
                    let roleResult = await userRoleHandler(resp[0].id);
                    let token = await generateToken(resp[0].id, resp[0].email, roleResult);
                    res.status(200).json({ succes: token });
                } else {
                    res.status(404).json({ Error: "Password or email do not match" });
                }
            } else {
                res.status(404).json({ Error: "Password or email do not match" });
            }
        })
    } catch (e) {
        console.log(e);
        res.status(404).json({ Error: "Password or email do not match" });
    }
};

export const deleting = async (req: Request, res: Response) => {
    const reqUser = req.body.User;
    const result = await userManager.deleteAnUser(new User(reqUser.email, reqUser.firstName, reqUser.lastName, reqUser.phoneNumber, reqUser.lastConnexion, reqUser.password, reqUser.AddressId, reqUser.id));
    if (result) return res.status(200).send({Succes: "User was successfully deleted !" });
    return res.status(400).send({ Error: "The account has not been deleted, you may have adopted an animal. If this is the case, please contact the center for this action." });
};

export const offerApply = async (req: Request, res: Response) => {
    const reqCandidat = req.body.Candidat;
    const reqUser = req.body.User;
    const result = await userManager.applyForAnOffer(new Candidat(reqCandidat.UserId, reqCandidat.OfferId, reqCandidat.OfferCenterId, reqCandidat.landedDate, reqCandidat.isAccepted, reqCandidat.isActive), reqUser.id);
    if (result) return res.status(200).send(result);
    return res.status(404).send({ Error: "The apply failed" });
};


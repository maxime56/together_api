
import { Picture } from "../models/Picture";
import Database from "../models/Database"

export class DaoPicture extends Database {
    public sqlToPicture(res): Picture {
        if (res) {
            return new Picture(
                res.path,
                res.Animal_id,
                res.User_id,
                res.Offer_id,
                res.Center_id,
                res.id
            );
        }
    }
    /**
     * @param {Number} id 
     * @returns {Picture}
     */
    public async selectOne(id: number): Promise<Picture> {
        // this.getConnection();
        const query = `CALL select_picture(${id});`;
        const res = await this.request(query);
        return this.sqlToPicture(JSON.parse(JSON.stringify(res[0]))[0]);
    }
    /**
     * 
     * @param {Number} centerId 
     * @returns {Picture} return every pictures of the center
     */
    public async select_center_picture(centerId: number) {
        const query = `CALL select_center_picture(${centerId});`;
        const res = await this.request(query);
        const result = [];
        res[0].forEach(element => {
            let i = this.sqlToPicture(JSON.parse(JSON.stringify(element)));
            result.push(i);
        });
        return result;
    }
    /**
     * 
     * @param {Number} userrId 
     * @returns {Picture} return every pictures of the user
     */
    public async select_user_picture(userrId: number): Promise<Picture[]> {
        const query = `CALL select_user_picture(${userrId});`;
        const res = await this.request(query);
        const result = [];
        res[0].forEach(element => {
            let i = this.sqlToPicture(JSON.parse(JSON.stringify(element)));
            result.push(i);
        });
        return result;
    }
    /**
     * 
     * @param {Number} animalId 
     * @returns {Picture} return every pictures of the animal
     */
    public async select_animal_picture(animalId: number) {
        const query = `CALL select_animal_picture(${animalId});`;
        const res = await this.request(query);
        const result = [];
        res[0].forEach(element => {
            let i = this.sqlToPicture(JSON.parse(JSON.stringify(element)));
            result.push(i);
        });
        return result;
    }
    /**
     * 
     * @param {Number} offerId 
     * @returns {Picture} return every pictures of the Offer
     */
    public async select_offer_picture(offerId: number) {
        const query = `CALL select_offer_picture(${offerId})`;
        const res = await this.request(query);
        const result = [];
        res[0].forEach(element => {
            let i = this.sqlToPicture(JSON.parse(JSON.stringify(element)));
            result.push(i);
        });
        return result;
    }
    /**
     * 
     * @param {Picture} pic 
     * @returns {Picture}
     */
    public async create(pic: Picture): Promise<Picture> {
        // à régler les procédures ne renvoie pas le dernier id
        let query = `INSERT INTO Picture (path, Animal_id, User_id, Offer_id, Center_id) VALUES
        ("${pic.getPath()}", ${pic.getAnimalId()}, ${pic.getUserId()}, ${pic.getOfferId()}, ${pic.getCenterId()});`;
        let res = await this.request(query);
        return this.selectOne(res.insertId);
    }
    /**
     * 
     * @param {Picture} pic 
     * @returns {Picture}
     */
    public async edit(pic: Picture): Promise<Picture> {
        let query = `CALL update_picture( "${pic.getPath()}", ${pic.getAnimalId()}, ${pic.getUserId()}, ${pic.getOfferId()}, ${pic.getCenterId()}, ${pic.getId()});`;
        await this.request(query);
        return this.selectOne(pic.getId());
    }
    /**
     * @param {Number} id 
     * @returns {Boolean}
     */
    public async delete(id: number) {
        const query = `CALL delete_picutre(${id});`;
        const res = await this.request(query);
        const deleteSuccess = res.affectedRows;
        return deleteSuccess === 1 ? true : false;
    }
    /**
     * @param {Number} userid 
     * @returns {Boolean} Should delete all pictures from a user (he should have one picture)
     */
    public async deleteUserPicture(userid: number) {
        const query = `CALL delete__user_picutre(${userid});`;
        const res = await this.request(query);
        const deleteSuccess = res.affectedRows;
        return deleteSuccess > 0 ? true : false;
    }

    /**
     * @param {Number} animalId 
     * @returns {Boolean} Should delete all pictures from a animal
     */
    public async deleteAnimalPictures(animalId: number) {
        const query = `CALL delete_animal_picture(${animalId});`;
        const res = await this.request(query);
        const deleteSuccess = res.affectedRows;
        return deleteSuccess > 0 ? true : false;
    }
    /**
     * @param {Number} centerId 
     * @returns {Boolean} Should delete all pictures from a animal
     */
    public async deleteCenterPictures(centerId: number) {
        const query = `CALL delete_center_pictures(${centerId});`;
        const res = await this.request(query);
        const deleteSuccess = res.affectedRows;
        return deleteSuccess > 0 ? true : false;
    }
    /**
* @param {Number} offerId 
* @returns {Boolean} Should delete all pictures from a offerId
*/
    public async deleteOfferPictures(offerId: number) {
        const query = `CALL delete_offer_pictures(${offerId});`;
        const res = await this.request(query);
        const deleteSuccess = res.affectedRows;
        return deleteSuccess > 0 ? true : false;
    }
}
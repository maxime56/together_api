import { expect } from "chai";
import { Animal } from "../../src/models/Animal";


describe('Animal Models Testing Getter & Setter', () => {
    let animal: Animal;
    // ces dates là me permettent de faire passer mes tests unitaire, le fait de faire un new bloque mes tests car l'instance n'est pas la même
    const newDate = new Date("10/10/2020");
    const oldDate = new Date("10/10/2100");
    beforeEach(() => {
        animal = new Animal("chien", "petit mais gentil", "12", "male", newDate, true, "boe4", 1, 2, 2, 1);
    });
    describe('Getter Setter Id', () => {
        it('getId', () => {
            expect(animal.getId()).to.be.equal(1);
        });
        it("setId", () => {
            animal.setId(2);
            expect(animal.getId()).to.be.equal(2);
        });
    });
    describe('Getter Setter Name', () => {
        it('getName', () => {
            expect(animal.getName()).to.be.equal("chien");
        });
        it("setName", () => {
            animal.setName("Name");
            expect(animal.getName()).to.be.equal("Name");
        });
    });
    describe('Getter Setter Age', () => {
        it('getAge', () => {
            expect(animal.getAge()).to.be.equal("12");
        });
        it("setAge", () => {
            animal.setAge("13");
            expect(animal.getAge()).to.be.equal("13");
        });
    });
    describe('Getter Setter Description', () => {
        it('getDescription', () => {
            expect(animal.getDescription()).to.be.equal("petit mais gentil");
        });
        it("setDescription", () => {
            animal.setDescription("test");
            expect(animal.getDescription()).to.be.equal("test");
        });
    });
    describe('Getter Setter CenterId', () => {
        it('getCenterId', () => {
            expect(animal.getCenterId()).to.be.equal(1);
        });
        it("setCenterId", () => {
            animal.setCenterId(2);
            expect(animal.getCenterId()).to.be.equal(2);
        });
    });
    describe('Getter Setter Species', () => {
        it('getSpecies', () => {
            expect(animal.getSpecies()).to.be.equal(2);
        });
        it("setSpecies", () => {
            animal.setSpecies(3);
            expect(animal.getSpecies()).to.be.equal(3);
        });
    });
    describe('Getter Setter Race', () => {
        it('getRace', () => {
            expect(animal.getRace()).to.be.equal(2);
        });
        it("setRace", () => {
            animal.setRace(3);
            expect(animal.getRace()).to.be.equal(3);
        });
    });
    describe('Getter Setter Gender', () => {
        it('getGender', () => {
            expect(animal.getGender()).to.be.equal('male');
        });
        it("setGender", () => {
            animal.setGender("femelle");
            expect(animal.getGender()).to.be.equal("femelle");
        });
    });
    describe('Getter Setter BirthDate', () => {
        it('getBirthDate', () => {
            expect(animal.getBirthDate()).to.be.equal(newDate);
        });
        it("setBirthDate", () => {
            animal.setBirthDate(oldDate);
            expect(animal.getBirthDate()).to.be.equal(oldDate);
        });
    });
    describe('Getter Setter IsActive', () => {
        it('getIsActive', () => {
            expect(animal.getIsActive()).to.be.equal(true);
        });
        it("setIsActive", () => {
            animal.setIsActive(false);
            expect(animal.getIsActive()).to.be.equal(false);
        });
    });
    describe('Getter Setter Document Ref', () => {
        it('getDocumentRef', () => {
            expect(animal.getDocumentRef()).to.be.equal("boe4");
        });
        it("setDocumentRef", () => {
            animal.setDocumentRef("false");
            expect(animal.getDocumentRef()).to.be.equal("false");
        });
    });
});


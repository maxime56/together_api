const jwt = require('jsonwebtoken');
module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(' ')[1];
        const decodedToken = jwt.verify(token, process.env.TOKEN);
        const userId = decodedToken.data.id;

        if (userId == req.body.User.id) {
            next();
        } else {
            throw 'Invalid user ID';
        }
    } catch (e) {
        console.log(e);

        res.status(403).json({
            error: "Forbidden"
        });
    }
};
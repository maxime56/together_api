import * as dotenv from "dotenv"
// read .env file before everything else
dotenv.config();
export default class Database {
    util: any;
    mysql: any;
    pool: any;
    s: any;
    constructor() {
        this.util = require('util');
        this.mysql = require('mysql');
        this.config();
        this.pool.query = this.util.promisify(this.pool.query);
        this.s = require('mysql').createPool({
           // host: process.env.DB_HOST,
            port: process.env.DB_PORT,
            user: process.env.DB_USER,
            password: process.env.DB_PASSWORD,
            database: process.env.DB_NAME
        });
    }

    public config() {
        this.pool = this.mysql.createPool({
        //   host: process.env.DB_HOST,
            port: process.env.DB_PORT,
            user: process.env.DB_USER,
            password: process.env.DB_PASSWORD,
            database: process.env.DB_NAME
        });
    }
    public async request(sql: String, inserts?: Object) {
        try {
            const res = await this.pool.query(sql, inserts);
            return res;
        } catch (err) {
            console.log(err);
            await this.pool.query("ROLLBACK", inserts);
            // await this.pool.end();
            // await this.pool.close();
        }
    }

    public async test() {
        const res = await this.request(`SELECT "OK" as message`);
        return res[0]['message'];
    }
}
import { expect } from "chai";
import { AddressDao } from "../../src/dao/DaoAddress";
import { DaoPicture } from "../../src/dao/DaoPicture";
import { CenterManager } from '../../src/manager/CenterManager';
import { Address } from "../../src/models/Address";
import { Adoption } from "../../src/models/Adoption";
import { Animal } from "../../src/models/Animal";
import { Candidat } from "../../src/models/Candidat";
import { Center } from "../../src/models/Center";
import { Offer } from "../../src/models/Offer";
import { Picture } from "../../src/models/Picture";
import { Race } from "../../src/models/Race";
import { Role } from "../../src/models/Role";
import { Species } from "../../src/models/Species";
import { User } from "../../src/models/User";
import { UserRole } from "../../src/models/UserRole";
import { Vaccine } from "../../src/models/Vaccine";
import { VaccinedAnimal } from "../../src/models/VaccinedAnimal";

describe("CenterManager", () => {
    const logic = new CenterManager();

    describe("getCentersByCountry", () => {
        it("should return an center with an address and its pictures", async () => {
            const centers = await logic.getCentersByCountry(0, 10, 1);
            expect(centers.Centers[1].Center).to.be.instanceof(Center);
            expect(centers.Centers[1].Address).to.be.instanceof(Address);
            expect(centers.Centers[1].Pictures[0]).to.be.instanceof(Picture);
        });
        it("shouldn't return an center with an address and its pictures because the country don't exist", async () => {
            const centers = await logic.getCentersByCountry(0, 10, 8000);
            expect(centers.Centers[1]).to.be.equal(undefined);
        });
    });
    describe("getAnimalsByCenter", () => {
        it("should return some animals with their pictures", async () => {
            const animals = await logic.getAnimalsByCenter(1, 0, 100);
            expect(animals.Animals["1"].Animal).to.be.instanceof(Animal);
            expect(animals.Animals["1"].Pictures[0]).to.be.instanceof(Picture);
        });
        it("shouldn't return some animals with their pictures because the center don't exist", async () => {
            const animals = await logic.getAnimalsByCenter(5000, 0, 100);
            expect(animals.Animals["1"]).to.be.equal(undefined);
        });
    });
    describe("getCenterOffers", () => {
        it("should return all the offers available in a center with their pictures", async () => {
            const offers = await logic.getCenterOffers(1);
            expect(offers.Offers["1"].Offer).to.be.be.instanceof(Offer);
            expect(offers.Offers["1"].Pictures[0]).to.be.instanceof(Picture);
        });
        it("shouldn't return all the offers available in a center with their pictures because the center don't exsit", async () => {
            const offers = await logic.getCenterOffers(5000);
            expect(offers.Offers["1"]).to.be.be.equal(undefined);
        });
    });
    describe("getAdoptionByCenter", () => {
        it("should return all adoptions allowed by a center with who made the adoption", async () => {
            const adoption = await logic.getAdoptionByCenter(1);
            expect(adoption.Adoptions["1"].Adoption).to.be.be.instanceof(Adoption);
            expect(adoption.Adoptions["1"].User).to.be.instanceof(User);
        });
        it("shouldn't return all adoptions in a center because he don't exist", async () => {
            const adoption = await logic.getAdoptionByCenter(500);
            expect(adoption.Adoptions["1"]).to.be.be.equal(undefined);
        });
    });
    describe("getSpeciesAndRaceByCenter", () => {
        it("should return all races & species from one center", async () => {
            const result = await logic.getSpeciesAndRaceByCenter(1);
            expect(result.Species[0]).to.be.be.instanceof(Species);
            expect(result.Races[0]).to.be.instanceof(Race);
        });
        it("shouldn't return all races & species from one center because he don't exist", async () => {
            const result = await logic.getSpeciesAndRaceByCenter(5000);
            expect(result.Species[0]).to.be.be.equal(undefined);
            expect(result.Races[0]).to.be.equal(undefined);
        });
    });
    describe("getVaccinesByCenter", () => {
        it("should return all vaccines in a center", async () => {
            const result = await logic.getVaccinesByCenter(1);
            expect(result.Vaccines[0]).to.be.be.instanceof(Vaccine);
        });
        it("shouldn't return vaccines because the center don't exist", async () => {
            const result = await logic.getVaccinesByCenter(5000);
            expect(result.Vaccines[0]).to.be.be.equal(undefined);
        });
    });
    describe("getStaffByCenter", () => {
        it("should return UserRoles from a center, a staff should have a address ", async () => {
            const result = await logic.getStaffByCenter(1);
            expect(result.Users["1"].User).to.be.be.instanceof(User);
            expect(result.Users["1"].UserRoles["1"]).to.be.be.instanceof(UserRole);
        });
        it("shouldn't return UserRoles from a center, because the center don't exist", async () => {
            const result = await logic.getStaffByCenter(5000);
            expect(result.Users["1"]).to.be.be.equal(undefined);
        });
    });
    describe("getVaccinedAnimalsByCenter", () => {
        it("should return all animals vaccined in a center", async () => {
            const result = await logic.getVaccinedAnimalsByCenter(1);
            expect(result.VaccinedAnimals[0]).to.be.be.instanceof(VaccinedAnimal);
        });
        it("shouldn't return a vaccined animal because the center don't exist", async () => {
            const result = await logic.getVaccinedAnimalsByCenter(5000);
            expect(result.VaccinedAnimals[0]).to.be.be.equal(undefined);
        });
    });
    describe("getCandidatsFromOffer", () => {
        it("should return candidats from a offer also a user and a address", async () => {
            const result = await logic.getCandidatsFromOffer(2);
            expect(result.Candidats["1"].User).to.be.be.instanceof(User);
            expect(result.Candidats["1"].Address).to.be.be.instanceof(Address);
            expect(result.Candidats["1"].Candidats["2"].User_id).to.be.equal(1);
            expect(result.Candidats["1"].Candidats["2"].Offer_id).to.be.equal(2);
        });
        it("shouldn't return candidats because the offer don't exist", async () => {
            const result = await logic.getCandidatsFromOffer(5000);
            expect(result.Candidats["1"]).to.be.equal(undefined);
        });
    });
    describe("getRolesFromCenter", () => {
        it("should return available roles from a center", async () => {
            const result = await logic.getRolesFromCenter(1);
            expect(result.Roles[0]).to.be.be.instanceof(Role);
        });
        it("shouldn't return available roles from a center because the center don't exist", async () => {
            const result = await logic.getRolesFromCenter(800);
            expect(result.Roles[0]).to.be.be.equal(undefined);
        });
    });
    describe("getRaces", () => {
        it("should return available races from the database", async () => {
            const result = await logic.getRaces();
            expect(result.Races[0]).to.be.be.instanceof(Race);
        });
    });
    describe("getSpecies", () => {
        it("should return available races from the database", async () => {
            const result = await logic.getSpecies();
            expect(result.Species[0]).to.be.be.instanceof(Species);
        });
    });
    describe("getVaccines", () => {
        it("should return available races from the database", async () => {
            const result = await logic.getVaccines();
            expect(result.Vaccines[0]).to.be.be.instanceof(Vaccine);
        });
    });
    describe("getRoles", () => {
        it("should return available races from the database", async () => {
            const result = await logic.getRoles();
            expect(result.Roles[0]).to.be.be.instanceof(Role);
        });
    });
    describe("getOneCenter", () => {
        it("Should return a object with a center, a address and his pictures", async () => {
            const result = await logic.getOneCenter(1);            
            expect(result.Center).to.be.instanceOf(Center);
            expect(result.Center).to.not.to.be.equal(undefined);
            expect(result.Address).to.be.instanceOf(Address);
            expect(result.Address).to.not.to.be.equal(undefined);
            expect(result.Pictures).to.be.instanceOf(Array);
            expect(result.Pictures[0]).to.not.to.be.equal(undefined);
        });
    });
});
import { update, deleteCenterOffer, deleteUserOffer, getAllCandidatsFromOffer } from "../controller/CandidatController";
const express = require("express");
const router = express.Router();
const checkToken = require("../middleware/tchekToken");
const centerActionHandler = require("../middleware/centerActionHandler");

router.route("/update/").post(centerActionHandler, checkToken, update); // admin
router.route("/deleteCenterOffer/").delete(centerActionHandler, checkToken, deleteCenterOffer); //admin
router.route("/getFromOffer/").get(centerActionHandler, checkToken, getAllCandidatsFromOffer); // admin
router.route("/deleteUserOffer/").delete(checkToken, deleteUserOffer); // user

module.exports = router;
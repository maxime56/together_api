export class Adoption {
    private UserId: number;
    private AnimalId: number;
    private AnimalCenterId: number;
    private adoptionDate: Date;
    private isActive: boolean;
    private id: number;
    constructor(UserId: number, AnimalId: number, AnimalCenterId: number, isActive:boolean, adoptionDate: Date, id?: number) {
        this.UserId = UserId;
        this.AnimalId = AnimalId;
        this.adoptionDate = adoptionDate;
        this.isActive = isActive;
        this.AnimalCenterId = AnimalCenterId;
        this.id = id;
    }
    public getUserId(): number {
        return this.UserId;
    }
    public setUserId(UserId: number): void {
        this.UserId = UserId;
    }
    public getAnimalId(): number {
        return this.AnimalId;
    }
    public setAnimalId(AnimalId: number): void {
        this.AnimalId = AnimalId;
    }
    public getAdoptionDate(): Date {
        return this.adoptionDate;
    }
    public setAdoptionDate(adoptionDate: Date): void {
        this.adoptionDate = adoptionDate;
    }
    public getIsActive(): boolean {
        return this.isActive;
    }
    public setIsActive(isActive: boolean): void {
        this.isActive = isActive;
    }
    public getAnimalCenterId(): number {
        return this.AnimalCenterId;
    }
    public setAnimalCenterId(AnimalCenterId: number): void {
        this.AnimalCenterId = AnimalCenterId;
    }
    public getId(): number {
        return this.id;
    }
}

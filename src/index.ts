import * as dotenv from "dotenv"
import * as express from "express"
import * as cors from "cors"
import * as helmet from "helmet";
import Database from "./models/Database";

// IMPORT
const routing = require("./routing/route");
const bodyParser = require('body-parser');

// read .env file before everything else
dotenv.config();



const app = express();
app.use(helmet());
app.use(cors());
app.use(
    bodyParser.urlencoded({ extended: true }),
    bodyParser.json(),
);



app.use("/uploads", express.static("public/uploads"));
app.use("/", routing);

app.get("/", async(req, res) => {
    console.log("someone doesnt call");
    let result = await new Database().test();
    if (result == "OK") return res.status(200).send("DB connected !!!");
    return res.status(500).send("No connection with db");
});

process.on('uncaughtException', function (err) {
    console.log('Caught exception: ', err);
});

app.listen(process.env.PORT, () => {
    console.log(`Example app listening on port ${process.env.PORT}`);
});

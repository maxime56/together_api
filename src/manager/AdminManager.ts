import { AddressDao } from "../dao/DaoAddress";
import { AdoptionDao } from "../dao/DaoAdoption";
import { AnimalDao } from "../dao/DaoAnimal";
import { CandidatDao } from "../dao/DaoCandidat";
import { CenterDao } from "../dao/DaoCenter";
import { OfferDao } from "../dao/DaoOffer";
import { DaoPicture } from "../dao/DaoPicture";
import { RaceDao } from "../dao/DaoRace";
import { DaoRole } from "../dao/DaoRole";
import { SpeciesDao } from "../dao/DaoSpecies";
import { UserRoleDao } from "../dao/DaoUserRole";
import { VaccineDao } from "../dao/DaoVaccine";
import { VaccinedAnimalDao } from "../dao/DaoVaccinedAnimals";
import { Address } from "../models/Address";
import { Adoption } from "../models/Adoption";
import { Animal } from "../models/Animal";
import { Candidat } from "../models/Candidat";
import { Center } from "../models/Center";
import { Offer } from "../models/Offer";
import { Picture } from "../models/Picture";
import { Race } from "../models/Race";
import { Role } from "../models/Role";
import { Species } from "../models/Species";
import { UserRole } from "../models/UserRole";
import { Vaccine } from "../models/Vaccine";
import { VaccinedAnimal } from "../models/VaccinedAnimal";

const fs = require('fs');
const multer = require('multer');
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, "./public/uploads");
    },
    filename: (req, file, cb) => {
        cb(null, Date.now() + file.originalname);
    }
});
const upload = multer({ storage: storage }).single('profil');

export class AdminManager {
    private CenterDao = new CenterDao();
    private AddressDao = new AddressDao();
    private PictureDao = new DaoPicture();
    private AnimalDao = new AnimalDao();
    private VaccinedAnimalDao = new VaccinedAnimalDao();
    private VaccineDao = new VaccineDao();
    private AdoptionDao = new AdoptionDao();
    private CandidatDao = new CandidatDao();
    private OfferDao = new OfferDao();
    private RaceDao = new RaceDao();
    private UserRoleDao = new UserRoleDao();
    private SpeciesDao = new SpeciesDao();
    private RoleDao = new DaoRole();


    /**
    * @param {Center} center 
    * @returns {Center} Should return the center that have ben created
    */
    public async createAnCenter(center: Center) {
        try {
            const nCenter = await this.CenterDao.create(center);
            return { Center: nCenter };
        } catch {
            return { Center: undefined };
        }
    }


    /**
     * @param {Animal} animal 
     * @returns Should create a animal
     */
    public async createAnAnimal(animal: Animal) {
        try {
            const nAnimal = await this.AnimalDao.create(animal);
            return { Animal: nAnimal };
        } catch {
            return { Animal: undefined };
        }
    }


    /**
     * @param {Adoption} adoption 
     * @param {number} CenterId 
     * @returns {Adoption}
     */
    public async createAnAdoption(adoption: Adoption, CenterId: number) {
        if (adoption.getAnimalCenterId() == CenterId) return { Adoption: await this.AdoptionDao.create(adoption) };
        return { Adoption: undefined };
    }


    /**
    * @param {VaccinedAnimal} vaccinedAnimal 
    * @returns {VaccinedAnimal}
    */
    public async createAnVaccinedAnimal(vaccinedAnimal: VaccinedAnimal) {
        try {
            return { VaccinedAnimal: await this.VaccinedAnimalDao.create(vaccinedAnimal) };
        } catch {
            return { VaccinedAnimal: undefined };
        }
    }


    /**
     * @param {Race} race 
     * @returns {Race} allows you to create breeds that will be available in all centres
     */
    public async createAnRace(race: Race) {
        try {
            return { Race: await this.RaceDao.create(race) };
        } catch {
            return { Race: undefined };
        }
    }


    /**
     * @param {Species} Species 
     * @returns {Species} allows you to create species that will be available in all centres
     */
    public async createAnSpecies(Species: Species) {
        try {
            return { Species: await this.SpeciesDao.create(Species) };
        } catch {
            return { Species: undefined };
        }
    }


    /**
     * @param {number} centerId 
     * @param {number} raceId 
     * @returns {Boolean} allows you to create a join table between centers and races
     */
    public async createAnCenterRace(centerId: number, raceId: number) {
        try {
            return await this.RaceDao.linkAnRaceInAnCenter(raceId, centerId);
        } catch {
            return false;
        }
    }


    /**
     * @param {number} centerId 
     * @param {number} speciesId 
     * @returns {Boolean} allows you to create a join table between centers and species
     */
    public async createAnCenterSpecies(centerId: number, speciesId: number) {
        try {
            return await this.SpeciesDao.linkAnSpeciesInAnCenter(speciesId, centerId);
        } catch {
            return false;
        }
    }


    /**
     * @param {Vaccine} vaccine 
     * @returns {Vaccine}  allows you to create vaccine that will be available in all centers
     */
    public async createAnVaccine(vaccine: Vaccine) {
        try {
            return { Vaccine: await this.VaccineDao.create(vaccine) };
        } catch {
            return { Vaccine: undefined };
        }
    }


    /**
     * @param {number} vaccineId 
     * @param {number} centerId 
     * @returns {Boolean} allows you to create a join table between centers and vaccine
     */
    public async createAnCenterVaccine(vaccineId: number, centerId: number) {
        try {
            return await this.VaccineDao.linkAnVaccineInAnCenter(vaccineId, centerId);
        } catch {
            return false;
        }
    }


    /**
     * @param {Role} role 
     * @returns {Role} create a role that will be available in all centers
     */
    public async createAnRole(role: Role) {
        try {
            return { Role: await this.RoleDao.create(role) };
        } catch {
            return { Role: undefined };
        }
    }


    /**
    * @param {UserRole} userRole 
    * @param {number} CenterId 
    * @returns {UserRole}
    */
    public async createAnUserRole(userRole: UserRole, CenterId: number) {
        try {
            if (userRole.getCenterId() == CenterId) return { UserRole: await this.UserRoleDao.create(userRole) };
            return { UserRole: undefined };
        } catch {
            return { UserRole: undefined };
        }
    }


    /**
    * @param {Offer} offer 
    * @param {number} CenterId 
    * @returns {Offer}
    */
    public async createAnOffer(offer: Offer, CenterId: number) {
        try {
            if (offer.getCenterId() == CenterId) return { Offer: await this.OfferDao.create(offer) };
            return { Offer: undefined };
        } catch {
            return { Offer: undefined };
        }
    }


    /**
    * @param {Picture} picture 
    * @returns {Picture}
    */
    public async createAnUserPicture(picture: Picture, file) {
        try {
            const filtredPath = file.path.substr(6);
            return { Picture: await this.PictureDao.create(new Picture(filtredPath, null, picture.getUserId(), null, null)) };
        } catch {
            return { Picture: undefined };
        }
    }


    /**
    * @param {Picture} picture 
    * @returns {Picture}
    */
    public async createAnAnimalPicture(picture: Picture, files) {
        try {
            const allPictures = [];
            files.forEach(file => {
                const filtredPath = file.path.substr(6);
                allPictures.push(this.PictureDao.create(new Picture(filtredPath, picture.getAnimalId(), null, null, null)));
            });
            return Promise.all(allPictures);
        } catch {
            return { Picture: undefined };
        }
    }


    /**
    * @param {Picture} picture 
    * @returns {Picture}
    */
    public async createAnCenterPicture(picture: Picture, files) {
        try {
            const allPictures = [];
            files.forEach(file => {
                const filtredPath = file.path.substr(6);
                allPictures.push(this.PictureDao.create(new Picture(filtredPath, null, null, null, picture.getCenterId())));
            });
            return Promise.all(allPictures);
        } catch {
            return { Picture: undefined };
        }
    }


    /**
    * @param {Picture} picture 
    * @returns {Picture}
    */
    public async createAnOfferPicture(picture: Picture, files) {
        try {
            const allPictures = [];
            files.forEach(file => {
                const filtredPath = file.path.substr(6);
                allPictures.push(this.PictureDao.create(new Picture(filtredPath, null, null, picture.getOfferId(), null)));
            });
            return Promise.all(allPictures);
        } catch {
            return { Picture: undefined };
        }
    }

    /**
     * @param {number} address 
     * @param {number} CenterId 
     * @returns {Address} Should return a address linked to a center
     */
    public async createAnCenterAddress(address: Address, CenterId: number) {
        try {
            const center = await this.CenterDao.selectOne(CenterId);
            if (center.getAddressId() == undefined) { // tcheck if the center has already a address
                const nAddress = await this.AddressDao.create(address);
                await this.CenterDao.linkAddressId(nAddress.getId(), CenterId);
                return { Address: nAddress };
            }
            return { Address: undefined };
        } catch {
            return { Address: undefined };
        }
    }



    /**********
    ***CREATE**
    **********/
    /**

    /**
     * @param {Address} address 
     * @param {number} CenterId 
     * @returns {Address}
     */
    public async updateAnCenterAddress(address: Address, CenterId: number) {
        try {
            const center = await this.CenterDao.selectOne(CenterId);
            if (center.getAddressId() == address.getId()) {
                const nAddress = await this.AddressDao.edit(address);
                return { Address: nAddress };
            }
            return { Address: undefined };
        } catch {
            return { Address: undefined };
        }
    }


    /**
    * @param {Offer} offer 
    * @param {number} CenterId 
    * @returns {Offer}
    */
    public async updateAnOffer(offer: Offer, CenterId: number) {
        try {
            const center = await this.CenterDao.selectOne(CenterId);
            if (offer.getCenterId() == CenterId && center.getId() == CenterId) {
                const nOffer = await this.OfferDao.edit(offer)
                return { Offer: nOffer };
            }
            return { Offer: undefined };
        } catch {
            return { Offer: undefined };
        }
    }


    /**
     * @param {Candidat} candidat 
     * @param {number} CenterId 
     * @returns {Candidat} Should update a candidat for know if he is hire for the offer
     */
    public async updateAnCandidat(candidat: Candidat, CenterId: number) {
        try {
            if (candidat.getOfferCenterId() == CenterId) {
                return { Candidat: await this.CandidatDao.edit(candidat) };
            }
            return { Candidat: undefined };
        } catch {
            return { Candidat: undefined };
        }
    }


    /**
     * 
     * @param {UserRole} userRole 
     * @returns {UserRole}
     */
    public async updateAnUserRole(userRole: UserRole) {
        try {
            return { UserRole: await this.UserRoleDao.edit(userRole) };
        } catch {
            return { UserRole: undefined };
        }
    }


    /**
     * @param {Role} role 
     * @returns {Role} update a role that will be available in all centers
     */
    public async updateAnRole(role: Role) {
        try {
            return { Role: await this.RoleDao.edit(role) };
        } catch {
            return { Role: undefined };
        }
    }


    /**
     * @param {Vaccine} vaccine 
     * @returns {Vaccine}  allows you to update vaccine that will be available in all centers
     */
    public async updateAnVaccine(vaccine: Vaccine) {
        try {
            return { Vaccine: await this.VaccineDao.edit(vaccine) };
        } catch {
            return { Vaccine: undefined };
        }
    }


    /**
     * @param {Race} race 
     * @returns {Race} allows you to update breeds that will be available in all centers
     */
    public async updateAnRace(race: Race) {
        try {
            return { Race: await this.RaceDao.edit(race) };
        } catch {
            return { Race: undefined };
        }
    }


    /**
     * @param {Species} Species 
     * @returns {Species} allows you to update species that will be available in all centers
     */
    public async updateAnSpecies(Species: Species) {
        try {
            return { Species: await this.SpeciesDao.edit(Species) };
        } catch {
            return { Species: undefined };
        }
    }


    /**
    * @param {VaccinedAnimal} vaccinedAnimal 
    * @returns {VaccinedAnimal}
    */
    public async updateAnVaccinedAnimal(vaccinedAnimal: VaccinedAnimal) {
        try {
            return { VaccinedAnimal: await this.VaccinedAnimalDao.edit(vaccinedAnimal) };
        } catch {
            return { VaccinedAnimal: undefined };
        }
    }


    /**
     * @param {Adoption} adoption 
     * @param {number} CenterId 
     * @returns {Adoption}
     */
    public async updateAnAdoption(adoption: Adoption, CenterId: number) {
        if (adoption.getAnimalCenterId() == CenterId) {
            return { Adoption: await this.AdoptionDao.update(adoption) };
        }
        return { Adoption: undefined };
    }


    /**
    * @param {Center} center
    * @returns {Center}
    */
    public async updateAnCenter(center: Center) {
        try {
            return { Center: await this.CenterDao.edit(center) };
        } catch {
            return { Center: undefined };
        }
    }


    /**
    * @param {Animal} animal the animal concerned
    * @returns {Animal}  Should update a animal
    */
    public async updateAnAnimal(animal: Animal) {
        try {
            const nAnimal = await this.AnimalDao.edit(animal);
            return { Animal: nAnimal }
        } catch {
            return { Animal: undefined };
        }
    }





    /**********
    ***UPDATE**
    **********/

    /**
     * @param {number} roleId 
     * @returns {Boolean} allows you to delete a role that is available in the (wont work if one center use this role)
     */
    public async deleteAnRole(roleId: number) {
        try {
            return await this.RoleDao.delete(roleId);
        } catch {
            return false;
        }
    }


    /**
     * @param {Race} race 
     * @returns {Race} allows you to delete breeds that will be available in all centres, it will not work if the species is used in another centre
     */
    public async deleteAnRace(race: Race) {
        try {
            return await this.RaceDao.delete(race.getId());
        } catch {
            return false;
        }
    }


    /**
     * @param {Species} Species 
     * @returns {Species} allows you to delete species that will be available in all centres, it will not work if the species is used in another centre
     */
    public async deleteAnSpecies(Species: Species) {
        try {
            return await this.SpeciesDao.delete(Species.getId());
        } catch {
            return false;
        }
    }


    /** A REVOIR AVEC LES TRIGGERS
     * @param {Center} center 
     * @returns {Boolean} if the return of the center pass through that it will be mean everything that is been deleted successfully 
     */
    public async deleteAnCenter(center: Center) {
        try {
            await this.PictureDao.deleteCenterPictures(center.getId());
            await this.CandidatDao.deleteAllCandidats(center.getId());
            await this.AdoptionDao.deleteAllAdoptions(center.getId());
            await this.OfferDao.deleteAllOffersByCenter(center.getId());
            await this.VaccineDao.deleteCenterVaccine(center.getId());
            await this.UserRoleDao.deleteAllRolesInCenter(center.getId());
            await this.AnimalDao.deleteAllAnimalsInAnCenter(center.getId());
            await this.RaceDao.deleteAllRaceByCenterId(center.getId());
            await this.SpeciesDao.deleteAllSpeciesByCenter(center.getId());
            const centerResult = await this.CenterDao.delete(center.getId());
            await this.AddressDao.delete(center.getAddressId());
            return centerResult;
        } catch {
            return false;
        }
    }


    /**
     * @param animal the animal concerned 
     * @return {Boolean} Should delete from the database the information of the animal and all his pictures, his previous adoptions and his vaccines
     */
    public async deleteAnAnimal(animal: Animal) {
        try {
            await this.PictureDao.deleteAnimalPictures(animal.getId());
            await this.VaccinedAnimalDao.deleteAllVaccinations(animal.getId());
            await this.AdoptionDao.deleteAnimalAdoptions(animal.getId())
            return await this.AnimalDao.delete(animal.getId());
            // if the return of the animal pass through that it will be mean everything that is been deleted successfully 
        } catch {
            return false;
        }
    }


    /**
     * @param {number} centerId 
     * @param {number} raceId 
     * @returns {Boolean}
     */
    public async deleteCenterRace(centerId: number, raceId: number) {
        try {
            return await this.RaceDao.deleteLinkedRaceInAnCenter(raceId, centerId);
        } catch {
            return false;
        }
    }


    /**
     * @param {number} centerId 
     * @param {number} speciesId 
     * @returns {Boolean}
     */
    public async deleteCenterSpecies(centerId: number, speciesId: number) {
        try {
            return await this.SpeciesDao.deleteLinkedSpeciesInAnCenter(speciesId, centerId);
        } catch {
            return false;
        }
    }


    /**
    * @param {Vaccine} vaccine 
    * @returns {Boolean}  allows you to delete a vaccine that is available in the centers (it wont delete if one center use it)
    */
    public async deleteAnVaccine(vaccine: Vaccine) {
        try {
            return await this.VaccineDao.delete(vaccine.getId());
        } catch {
            return false;
        }
    }


    /**
     * @param {number} centerId 
     * @param {number} vaccineId 
     * @returns {Boolean} allows you to delete a join table between centers and vaccine
     */
    public async deleteAnCenterVaccine(centerId: number, vaccineId: number) {
        try {
            return await this.VaccineDao.deleteLinkedVaccineInAnCenter(vaccineId, centerId);
        } catch {
            return false;
        }
    }


    /**
     * @param {number} roleId 
     * @param {number} userId 
     * @returns {Boolean}
     */
    public async deleteAnUserRole(roleId: number, userId: number) {
        try {
            return await this.UserRoleDao.delete(roleId, userId);
        } catch {
            return false;
        }
    }


    /**
    * @param {number} userId 
    * @param {number} offerId 
    * @returns {Candidat} Should delete a candidat 
    */
    public async deleteAnCandidat(userId: number, offerId: number) {
        try {
            return await this.CandidatDao.delete(userId, offerId);
        } catch {
            return false;
        }
    }


    /**
     * @param {number} offerId 
     * @returns {Boolean} Should delete all the pictures & the actual offer
     */
    public async deleteAnOffer(offerId: number) {
        try {
            await this.PictureDao.deleteOfferPictures(offerId);
            return await this.OfferDao.delete(offerId);
        } catch {
            return false;
        }
    }


    /** 
     * @param {number} VaccineId 
    * @param {number} AnimalId 
    * @returns {VaccinedAnimal}
    */
    public async deleteAnVaccinedAnimal(VaccineId: number, AnimalId: number) {
        try {
            return await this.VaccinedAnimalDao.delete(VaccineId, AnimalId);
        } catch {
            return false;
        }
    }



    /**
    * @param {Picture} picture 
    * @returns {Picture}
    */
    public async deleteAnUserPicture(picture: Picture) {
        try {
            await this.deleteAnFile(picture.getPath());
            return this.PictureDao.delete(picture.getId());
        } catch {
            return false;
        }
    }


    /**
    * @param {Picture} picture 
    * @returns {Picture}
    */
    public async deleteAnAnimalPicture(picture: Picture) {
        try {
            await this.deleteAnFile(picture.getPath());
            return this.PictureDao.delete(picture.getId());
        } catch {
            return false;
        }
    }


    /**
    * @param {Picture} picture 
    * @returns {Picture}
    */
    public async deleteAnCenterPicture(picture: Picture) {
        try {
            await this.deleteAnFile(picture.getPath());
            return this.PictureDao.delete(picture.getId());
        } catch {
            return false;
        }
    }


    /**
    * @param {Picture} picture 
    * @returns {Picture}
    */
    public async deleteAnOfferPicture(picture: Picture) {
        try {
            await this.deleteAnFile(picture.getPath());
            return this.PictureDao.delete(picture.getId());
        } catch {
            return false;
        }
    }


    public async deleteAnFile(path) {
        if (path.substr(0, 9) !== "/uploads/") throw new Error("Bad path");
        await fs.unlinkSync("public" + path);
    }
    /**********
    ***DELETE**
    **********/
}
import { AddressDao } from "../dao/DaoAddress";
import { AdoptionDao } from "../dao/DaoAdoption";
import { CandidatDao } from "../dao/DaoCandidat";
import { CountryDao } from "../dao/DaoCountry";
import { OfferDao } from "../dao/DaoOffer";
import { DaoPicture } from "../dao/DaoPicture";
import { UserDao } from "../dao/DaoUser";
import { Address } from "../models/Address";
import { Candidat } from "../models/Candidat";
import { Country } from "../models/Country";
import { User } from "../models/User";


export class UserManager {
    private UserDao = new UserDao();
    private AddressDao = new AddressDao();
    private AdoptionDao = new AdoptionDao();
    private CandidatDao = new CandidatDao();
    private OfferDao = new OfferDao();
    private PictureDao = new DaoPicture();
    private CountryDao = new CountryDao();

    /**
     * @param {User} user 
     * @returns {User}
     */
    public async createAnUser(user: User) {
        try {
            return { user: await this.UserDao.create(user) };
        } catch (error) {
            return { user: undefined};   
        }
    }


    /**
     * @param {User} user 
     * @returns {User}
     */
    public async updateAnUser(user: User) {
        try {
            return { user: await this.UserDao.update(user) };
        } catch (error) {
            return { user: undefined};
        }
    }


    /**
    *@param {Address} address 
    * @param {number} userId 
    * @returns {Address} Should return a user address and linked to the actual user
    */
    public async createAnUserAddress(address: Address, userId: number) {
        const user = await this.UserDao.selectOne(userId); // check if the user exist and if he no address
        if (user && user.getAddressId() == undefined) {
            const nAddress = await this.AddressDao.create(address);
            await this.UserDao.linkAddressId(nAddress.getId(), userId);
            return { Address: nAddress };
        }
        return { Address: undefined };
    }


    /**
* @param {Address} address 
* @param {number} userId 
* @returns {Address}
*/
    public async updateAnUserAddress(address: Address, userId: number) {
        try {
            const user = await this.UserDao.selectOne(userId);
            if (user && user.getAddressId() !== address.getId()) return { Address: undefined };
            if (user && user.getAddressId() == address.getId()) return { Address: await this.AddressDao.edit(address) };
            return { Address: undefined };
        } catch {
            return { Address: undefined };
        }

    }


    /**
     * @param {number} userId 
     * @returns should return a object with a user his profil picture his address all his adoption if he have made one and his applied jobs historic
     */
    public async getAnUser(userId) {
        const nUser = await this.UserDao.selectOne(userId);
        if (nUser) { // check if the user exist
            const nAddress = ((nUser.getAddressId() == null || nUser.getAddressId() == undefined) ? null : await this.AddressDao.selectOne(nUser.getAddressId()));
            const nPicture = await this.PictureDao.select_user_picture(nUser.getId());
            const nAdoption = await this.AdoptionDao.getAdoptionByUser(userId);
            // the historic of the user applied
            const nOffer = await this.OfferDao.selectHistoricOffer(userId);
            return { address: nAddress, user: nUser, picture: nPicture, adoption: nAdoption, historicOffer: nOffer };
        }
        return { address: undefined, user: undefined, picture: undefined, adoption: undefined, historicOffer: undefined };
    }


    /**
     * @param {number} user 
     * @returns Should delete a user with his picture address and his applied offer. One user cant be deleted if he got one adoption in a center
     */
    public async deleteAnUser(user: User) {
        await this.CandidatDao.deleteUserAllApplications(user.getId());
        await this.PictureDao.deleteUserPicture(user.getId());
        const resp = await this.UserDao.delete(user.getId());
        ((user.getAddressId() == null || user.getAddressId() == undefined) ? null : await this.AddressDao.delete(user.getAddressId()));
        return resp;
    }


    /**
    * @param {Candidat} candidat 
    *@param {number} UserId 
    * @returns {Candidat}
    */
    public async applyForAnOffer(candidat: Candidat, UserId: number) {
        if (candidat.getUserId() == UserId) {
            candidat.setIsAccepted(false);
            candidat.setIsActive(true);
            return { Offer: await this.CandidatDao.create(candidat) };
        }
        return { Offer: undefined };
    }


    /**
    * @param {number} userId 
    * @param {number} offerId 
    * @returns {Boolean}
    */
    public async removeAppliedOffer(userId: number, offerId: number) {
        try {
            return await this.CandidatDao.delete(userId, offerId);
        } catch {
            return false;
        }
    }


    /**
    * @param {number} userId 
    * @param {number} userId 
    * @returns {Boolean} check if the address belongs to the user otherwise the deletion does not work 
    */
    public async deleteAnUserAddress(AddressId: number, UserId: number) {
        const user = await this.UserDao.selectOne(UserId);
        if (user && AddressId !== user.getAddressId()) return false;
        try {
            if (AddressId == user.getAddressId()) return await this.AddressDao.delete(user.getAddressId());
        } catch {
            // default return false;
            return false;
        }
    }

}
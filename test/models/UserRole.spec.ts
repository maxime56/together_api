import { assert, expect } from "chai";
import { UserRole } from "../../src/models/UserRole";


describe('UserRole Models Testing Getter & Setter', () => {
    let userRole: UserRole;
    const oldDate = new Date("10/10/2100");
    const newDate = new Date("10/10/2020");
    beforeEach(() => {
        userRole = new UserRole(1, 1, oldDate, oldDate, 1);
    });
    describe('Getter Setter RoleId', () => {
        it('getRoleId', () => {
            expect(userRole.getRoleId()).to.be.equal(1);
        });
        it("setRoleId", () => {
            userRole.setRoleId(2);
            expect(userRole.getRoleId()).to.be.equal(2);
        });
    });
    describe('Getter Setter UserId', () => {
        it('getUserId', () => {
            expect(userRole.getUserId()).to.be.equal(1);
        });
        it("setUserId", () => {
            userRole.setUserId(2);
            expect(userRole.getUserId()).to.be.equal(2);
        });
    });
    describe('Getter Setter Active', () => {
        it('getActive', () => {
            expect(userRole.getActive()).to.be.equal(oldDate);
        });
        it("setActive", () => {
            userRole.setActive(newDate);
            expect(userRole.getActive()).to.be.equal(newDate);
        });
    });
    describe('Getter Setter Expiration', () => {
        it('getExpiration', () => {
            expect(userRole.getExpiration()).to.be.equal(oldDate);
        });
        it("setExpiration", () => {
            userRole.setExpiration(newDate);
            expect(userRole.getExpiration()).to.be.equal(newDate);
        });
    });
    describe('Getter Setter Center_id', () => {
        it('getCenterId', () => {
            expect(userRole.getCenterId()).to.be.equal(1);
        });
        it("setCenterId", () => {
            userRole.setCenterId(2);
            expect(userRole.getCenterId()).to.be.equal(2);
        });
    });
});


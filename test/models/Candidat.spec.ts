import { expect } from "chai";
import { Candidat } from "../../src/models/Candidat";


describe('Candidat Models Testing Getter & Setter', () => {
    let candidat: Candidat;
    // ces dates là me permettent de faire passer mes tests unitaire, le fait de faire un new bloque mes tests car l'instance n'est pas la même
    const newDate = new Date("10/10/2020");
    const oldDate = new Date("10/10/2100");
    beforeEach(() => {
        candidat = new Candidat(1, 1, 1, newDate, true, false);
    });
    describe('Getter UserId', () => {
        it('getUserId', () => {
            expect(candidat.getUserId()).to.be.equal(1);
        });
    });
    describe('Getter Setter OfferId', () => {
        it('getOfferId', () => {
            expect(candidat.getOfferId()).to.be.equal(1);
        });
        it("setOfferId", () => {
            candidat.setOfferId(2);
            expect(candidat.getOfferId()).to.be.equal(2);
        });
    });
    describe('Getter Setter LandedDate', () => {
        it('getLandedDate', () => {
            expect(candidat.getLandedDate()).to.be.equal(newDate);
        });
        it("setLandedDate", () => {
            candidat.setLandedDate(oldDate);
            expect(candidat.getLandedDate()).to.be.equal(oldDate);
        });
    });
    describe('Getter Setter IsAccepted', () => {
        it('getIsAccepted', () => {
            expect(candidat.getIsAccepted()).to.be.equal(true);
        });
        it("setIsAccepted", () => {
            candidat.setIsAccepted(false);
            expect(candidat.getIsAccepted()).to.be.equal(false);
        });
    });
    describe('Getter Setter IsActive', () => {
        it('getIsActive', () => {
            expect(candidat.getIsActive()).to.be.equal(false);
        });
        it("setIsActive", () => {
            candidat.setIsActive(true);
            expect(candidat.getIsActive()).to.be.equal(true);
        });
    });
    describe('Getter Setter CenterOfferId', () => {
        it('getCenterOfferId', () => {
            expect(candidat.getOfferCenterId()).to.be.equal(1);
        });
        it("setCenterOfferId", () => {
            candidat.setOfferCenterId(2);
            expect(candidat.getOfferCenterId()).to.be.equal(2);
        });
    });
});


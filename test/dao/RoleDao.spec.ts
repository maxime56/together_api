import { expect } from "chai";
import { Role } from "../../src/models/Role";
import { DaoRole } from "../../src/dao/DaoRole";
import { DaoHandler } from "../../src/dao/DaoHandler";

describe("Role DAO", () => {
    let iduser: number;
    let idRole:number;
    // Test 1
    describe("SQL response to Role", () => {
        let raceDao = new DaoRole();
        // Données fictives renvoyés par la base de données.
        const res = {
            name: "test",
            id: 50
        };
        const race: Role = raceDao.sqlToRole(res);
        it("Name", () => {
            expect(race.getName()).to.be.equal("test");
        });
        it("id", () => {
            expect(race.getId()).to.be.equal(50);
        });
    });

    // Test de l'intégration du DAO avec la base de données.
    describe("DAO integration", () => {
        it("New", async () => {
            const newRace: Role = new Role("TDDUS",1);
            const raceDao = new DaoRole();
            const race = await raceDao.create(newRace);
            expect(race).to.be.instanceOf(Role);            
            expect(race.getName()).to.be.equal("TDDUS");
            // récupérer le dernier id pour l'edit et le delete par la suite
            iduser = race.getId();
        });
        it("Get", async () => {
            const raceDao = new DaoRole();
            const race = await raceDao.selectOne(iduser);
            expect(race).to.be.instanceOf(Role);
        });
        it("select_roles_by_center", async () => {
            const raceDao = new DaoRole();
            const race = await raceDao.select_roles_by_center(1);
            expect(race[0]).to.be.instanceOf(Role);
        });
        it("getAllRoles", async () => {
            const raceDao = new DaoRole();
            const race = await raceDao.getAllRoles();
            expect(race[0]).to.be.instanceOf(Role);
        });
        it("Edit", async () => {
            const editedRace: Role = new Role("EDDITDDUS", iduser);
            const raceDao = new DaoRole();
            const race = await raceDao.edit(editedRace);
            expect(race).to.be.instanceOf(Role);
            expect(race.getName()).to.be.equal("EDDITDDUS");
        });
        it("Delete", async () => {
            const raceDao = new DaoRole();
            const deleteSuccess = await raceDao.delete(iduser);
            expect(deleteSuccess).to.be.true;
        });
    });
});

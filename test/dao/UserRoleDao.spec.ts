import { expect } from "chai";
import { UserRole } from "../../src/models/UserRole";
import { UserRoleDao } from "../../src/dao/DaoUserRole";
import { DaoHandler } from "../../src/dao/DaoHandler";

describe("UserRole DAO", () => {
    let iduser: number;
    let idRole:number;
    // Test 1
    describe("SQL response to Role", () => {
        let raceDao = new UserRoleDao();
        // Données fictives renvoyés par la base de données.
        
        const res = {
            Role_id: 1,
            User_id: 1,
            active: new Date("2000/12/20"),  
            expiration: new Date("2000/12/20"),
            Center_id: 1
        };
        const userRole: UserRole = raceDao.sqlToUserRole(res);
        it("RoleId", () => {
            expect(userRole.getRoleId()).to.be.equal(1);
        });
        it("UserId", () => {
            expect(userRole.getUserId()).to.be.equal(1);
        });
        it("Active", () => {
            expect(userRole.getActive()).to.be.instanceof(Date);
        });
        it("Expiration", () => {
            expect(userRole.getExpiration()).to.be.instanceof(Date);
        });
        it("CenterId", () => {
            expect(userRole.getCenterId()).to.be.equal(1);
        });
    });

    // Test de l'intégration du DAO avec la base de données.
    describe("DAO integration", () => {
        it("New", async () => {
            const newUserRole: UserRole = new UserRole(2,2, new Date("2000/12/20"), new Date("2000/12/20"), 1 );
            const userRoleDao = new UserRoleDao();
            const userRole = await userRoleDao.create(newUserRole);
            expect(userRole).to.be.instanceOf(UserRole);      
            expect(userRole.getExpiration().toString().slice(0,10)).to.be.equal("2000-12-19");
            expect(userRole.getActive().toString().slice(0,10)).to.be.equal("2000-12-19");      
            expect(userRole.getUserId()).to.be.equal(2);
            expect(userRole.getRoleId()).to.be.equal(2);
            expect(userRole.getCenterId()).to.be.equal(1);
            // récupérer le dernier id pour l'edit et le delete par la suite
            iduser = userRole.getUserId();
            idRole = userRole.getRoleId();
        });
        it("Get", async () => {
            const raceDao = new UserRoleDao();
            const race = await raceDao.selectOne(idRole, iduser);
            expect(race).to.be.instanceOf(UserRole);
        });
        it("GetAll", async () => {
            const raceDao = new UserRoleDao();
            const race = await raceDao.select_user_with_role(iduser);
            expect(race[0]).to.be.instanceOf(UserRole);
        });
        it("Edit", async () => {
            const editedUserRole: UserRole = new UserRole(idRole, iduser, new Date("2000/12/10"), new Date("2000/12/15"), 2);
            const userRoleDao = new UserRoleDao();
            const userRole = await userRoleDao.edit(editedUserRole);
            expect(userRole).to.be.instanceOf(UserRole);      
            expect(userRole.getExpiration().toString().slice(0,10)).to.be.equal("2000-12-14");
            expect(userRole.getActive().toString().slice(0,10)).to.be.equal("2000-12-09");      
            expect(userRole.getUserId()).to.be.equal(2);
            expect(userRole.getRoleId()).to.be.equal(2);
            expect(userRole.getCenterId()).to.be.equal(2);
        });
        it("Delete", async () => {
            const userRoleDao = new UserRoleDao();
            const deleteSuccess = await userRoleDao.delete(idRole,iduser);
            expect(deleteSuccess).to.be.true;
        });
        it("deleteAllRolesInCenter", async () => {
            const userRoleDao = new UserRoleDao();
            const deleteSuccess = await userRoleDao.deleteAllRolesInCenter(5);
            expect(deleteSuccess).to.be.true;
        });
    });
});

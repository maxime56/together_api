
import { Vaccine } from "../models/Vaccine";
import Database from "../models/Database"

export class VaccineDao extends Database {
    public sqlToVaccine(res): Vaccine {
        if (res) {
            return new Vaccine(
                res.name,
                res.id
            );
        }
    }
    /**
     * 
     * @param {Number} id 
     * @returns {Vaccine}
     */
    public async selectOne(id: number): Promise<Vaccine> {
        // this.getConnection();
        const query = `CALL select_vaccin(${id})`;
        const res = await this.request(query);
        return this.sqlToVaccine(JSON.parse(JSON.stringify(res[0]))[0]);
    }
    /**
     * 
     * @param {Number} idCenter 
     * @returns {Vaccine} Should return all vaccines that have made been by a center
     */
    public async selectVaccineByCenter(idCenter: number) {
        const query = `CALL select_center_vaccine(${idCenter});`;
        const res = await this.request(query);
        const result = [];
        res[0].forEach(element => {
            let i = this.sqlToVaccine(JSON.parse(JSON.stringify(element)));
            result.push(i);
        });
        return result;
    }
    /**
     * @returns {Vaccine} Should return all available vaccines 
     */
    public async selectVaccines() {
        const query = `CALL select_all_vaccines();`;
        const res = await this.request(query);
        const result = [];
        res[0].forEach(element => {
            let i = this.sqlToVaccine(JSON.parse(JSON.stringify(element)));
            result.push(i);
        });
        return result;
    }
    /**
     * 
     * @param {Vaccine} vaccine 
     * @returns {Vaccine}
     */
    public async create(vaccine: Vaccine): Promise<Vaccine> {
        // à régler les procédures ne renvoie pas le dernier id
        let query = `INSERT INTO Vaccine (name) VALUES ("${vaccine.getName()}");`;
        let res = await this.request(query);
        return this.selectOne(res.insertId);
    }
    /**
   * 
   * @param {Vaccine} vaccine 
   * @returns {Vaccine}
   */
    public async edit(vaccine: Vaccine): Promise<Vaccine> {
        // à régler les procédures ne renvoie pas le dernier id
        let query = `CALL update_vaccine("${vaccine.getName()}", ${vaccine.getId()});`;
        let res = await this.request(query);
        return this.selectOne(vaccine.getId());
    }
    /**
     * @param {Number} id 
     * @returns {Boolean}
     */
    public async delete(id: number) {
        const query = `CALL delete_vaccine(${id})`;
        const res = await this.request(query);
        const deleteSuccess = res.affectedRows;
        return deleteSuccess === 1 ? true : false;
    }
    /**
    * @param {Number} centerId 
    * @returns {Boolean} Should delete all the vaccine that it been made in a center & return a boolean
    */
    public async deleteCenterVaccine(centerId: number) {
        const query = `CALL delete_center_vaccines(${centerId})`;
        const res = await this.request(query);
        const deleteSuccess = res.affectedRows;
        return deleteSuccess > 0 ? true : false;
    }
    /**
* @param {Number} vaccineId 
* @param {Number} centerId 
* @return {Boolean} allows you to create a join table between centers and Vaccine
*/
    public async linkAnVaccineInAnCenter(vaccineId: Number, centerId: Number): Promise<Boolean> {
        let query = `INSERT INTO Vaccine_center (Vaccine_id, Center_id) VALUES (${vaccineId}, ${centerId});`;
        let res = await this.request(query);
        return res.affectedRows > 0 ? true : false;
    }
    /**
* @param {Number} vaccineId 
* @param {Number} centerId 
* @return {Boolean} 
*/
    public async deleteLinkedVaccineInAnCenter(vaccineId: Number, centerId: Number): Promise<Boolean> {
        let query = `CALL delete_one_center_vaccine(${centerId}, ${vaccineId});`;
        let res = await this.request(query);
        return res.affectedRows > 0 ? true : false;
    }
}
import { expect } from "chai";
import { Picture } from "../../src/models/Picture";
import { DaoPicture } from "../../src/dao/DaoPicture";
import { DaoHandler } from "../../src/dao/DaoHandler";

describe("Picture DAO", () => {
    let deleteId: number;
    // Test 1
    describe("SQL response to picture", () => {
        let pictureDao = new DaoPicture();

        // Données fictives renvoyés par la base de données.
        const res = {
            path: "patth",
            Animal_id: 1,
            User_id: 1,
            Offer_id: 1,
            Center_id: 1,
            id:1 
        };
        const picture: Picture = pictureDao.sqlToPicture(res);
        it("Id", () => {
            expect(picture.getId()).to.be.equal(1);
        });
        it("Path", () => {
            expect(picture.getPath()).to.be.equal("patth");
        });
        it("AnimalId", () => {
            expect(picture.getAnimalId()).to.be.equal(1);
        });
        it("UserId", () => {
            expect(picture.getUserId()).to.be.equal(1);
        });
        it("OfferId", () => {
            expect(picture.getOfferId()).to.be.equal(1);
        });
        it("CenterId", () => {
            expect(picture.getCenterId()).to.be.equal(1);
        });
    });
    // Test de l'intégration du DAO avec la base de données.
    describe("DAO integration", () => {
        it("New", async () => {
            const newOffer: Picture = new Picture("ddd", 1, null, null, null, null);
            const offerDao = new DaoPicture();
            const offer = await offerDao.create(newOffer);
            // récupération de l'id qui vient d'être créé pour le supprimer
            deleteId = offer.getId();
            expect(offer).to.be.instanceOf(Picture);
            expect(offer.getPath()).to.be.equal("ddd");
            expect(offer.getAnimalId()).to.be.equal(1);
            expect(offer.getCenterId()).to.be.equal(null);  
            expect(offer.getUserId()).to.be.equal(null);
            expect(offer.getOfferId()).to.be.equal(null);
        });
        it("Edit", async () => {
            const edit: Picture = new Picture("edited", 1, null, null, null, deleteId);
            const offerDao = new DaoPicture();
            const offer = await offerDao.edit(edit);
            // récupération de l'id qui vient d'être créé pour le supprimer
            deleteId = offer.getId();
            expect(offer).to.be.instanceOf(Picture);
            expect(offer.getPath()).to.be.equal("edited");
            expect(offer.getAnimalId()).to.be.equal(1);
            expect(offer.getCenterId()).to.be.equal(null);  
            expect(offer.getUserId()).to.be.equal(null);
            expect(offer.getOfferId()).to.be.equal(null);
        });
        it("Get", async () => {
            const offerDao = new DaoPicture();
            const offer = await offerDao.selectOne(deleteId);
            expect(offer).to.be.instanceOf(Picture);
        });
        it("select_center_picture", async () => {
            const offerDao = new DaoPicture();
            const offer = await offerDao.select_center_picture(1);
            expect(offer[0]).to.be.instanceOf(Picture);
        });
        it("select_user_picture", async () => {
            const offerDao = new DaoPicture();
            const offer = await offerDao.select_user_picture(1);
            expect(offer[0]).to.be.instanceOf(Picture);
        });
        it("select_animal_picture", async () => {
            const offerDao = new DaoPicture();
            const offer = await offerDao.select_animal_picture(1);
            expect(offer[0]).to.be.instanceOf(Picture);
        });
        it("select_offer_picture", async () => {
            const offerDao = new DaoPicture();
            const offer = await offerDao.select_offer_picture(1);
            expect(offer[0]).to.be.instanceOf(Picture);
        });
        it("delete", async () => {
            const offerDao = new DaoPicture();
            const deleteSuccess = await offerDao.delete(deleteId);
            expect(deleteSuccess).to.be.true;
        });
        it("deleteAnimalPictures", async () => {
            const offerDao = new DaoPicture();
            const deleteSuccess = await offerDao.deleteAnimalPictures(10);
            expect(deleteSuccess).to.be.true;
        });
        it("deleteUserPictures", async () => {
            const offerDao = new DaoPicture();
            const deleteSuccess = await offerDao.deleteUserPicture(9);
            expect(deleteSuccess).to.be.true;
        });
        it("deleteCenterPictures", async () => {
            const offerDao = new DaoPicture();
            const deleteSuccess = await offerDao.deleteCenterPictures(3);
            expect(deleteSuccess).to.be.true;
        });
        it("deleteOfferPictures", async () => {
            const offerDao = new DaoPicture();
            const deleteSuccess = await offerDao.deleteOfferPictures(2);
            expect(deleteSuccess).to.be.true;
        });
    });
});


import { Role } from "../models/Role";
import Database from "../models/Database"

export class DaoRole extends Database {
    public sqlToRole(res): Role {
        if (res) {
            return new Role(
                res.name,
                res.id
            );
        }
    }
    /**
     * 
     * @param {Number} id 
     * @returns {Role}
     */
    public async selectOne(id: number): Promise<Role> {
        // this.getConnection();
        const query = `CALL select_role(${id})`;
        const res = await this.request(query);
        return this.sqlToRole(JSON.parse(JSON.stringify(res[0]))[0]);
    }
    /**
     * 
     * @param {Number} idCenter 
     * @returns {Role} Should return all roles that have been made by a center
     */
    public async select_roles_by_center(idCenter: number) {
        const query = `CALL select_roles_by_center(${idCenter})`;
        const res = await this.request(query);
        const result = [];
        res[0].forEach(element => {
            let i = this.sqlToRole(JSON.parse(JSON.stringify(element)));
            result.push(i);
        });
        return result;
    }
    /**
     * 
     * @param {Role} role 
     * @returns {Role}
     */
    public async create(role: Role): Promise<Role> {
        // à régler les procédures ne renvoie pas le dernier id
        let query = `INSERT INTO Role (name) VALUES ("${role.getName()}");`;
        let res = await this.request(query);
        return this.selectOne(res.insertId);
    }
    /**
     * 
     * @param {Role} role 
     * @returns {Role}
     */
    public async edit(role: Role): Promise<Role> {
        let query = `CALL update_role( "${role.getName()}", ${role.getId()})`;
        await this.request(query);
        return this.selectOne(role.getId());
    }
    /**
     * 
     * @param {Number} id 
     * @returns {Boolean}
     */
    public async delete(id: number) {
        const query = `CALL delete_role(${id})`;
        const res = await this.request(query);
        const deleteSuccess = res.affectedRows;
        return deleteSuccess === 1 ? true : false;
    }
    /**
 * 

 * @returns {Role} Should return all roles that is available in the databases
 */
    public async getAllRoles() {
        const query = `CALL select_all_roles()`;
        const res = await this.request(query);
        const result = [];
        res[0].forEach(element => {
            let i = this.sqlToRole(JSON.parse(JSON.stringify(element)));
            result.push(i);
        });
        return result;
    }
}
export class Picture {
    private path:string;
    private AnimalId: number;
    private UserId: number;
    private CenterId:number;
    private OfferId:number;
    private id:number;
    public constructor (path:string, Animal_id:number, User_id:number, OfferId:number, Center_id:number ,id?:number) {
        this.path = path;
        this.AnimalId = Animal_id;
        this.UserId = User_id;
        this.CenterId = Center_id;
        this.OfferId = OfferId;
        this.id = id;
    }
    public getPath(): string {
        return this.path;
    }
    public setPath(path:string):void{
        this.path = path;
    }
    public getAnimalId():number{
        return this.AnimalId;
    }
    public setAnimalId(path:number):void{
        this.AnimalId = path;
    }
    public getUserId():number{
        return this.UserId;
    }
    public setUserId(UserId:number):void{
        this.UserId = UserId;
    }
    public getCenterId():number{
        return this.CenterId;
    }
    public setCenterId(CenterId:number):void{
        this.CenterId = CenterId;
    }
    public getOfferId():number{
        return this.OfferId;
    }
    public setOfferId(OfferId:number):void{
        this.OfferId = OfferId;
    }
    public getId():number {
        return this.id;
    }
}

// permet de vérifier le format de l'adresse email
module.exports = async (req, res, next) => {
    const password = req.body.User.password || req.body.Center.password;
    try {
        let regexEmail = /^(?=.*\d)(?=.*[!@#$%^&.*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
        if (password.match(regexEmail)) {
            next();
        } else {
            throw "invalid password format";
            
        }
    } catch (e) {
        res.status(400).json({
            Error: "the password does not have the right format"
        });
    }
};  
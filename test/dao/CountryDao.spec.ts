import { expect } from "chai";
import { Country } from "../../src/models/Country";
import { CountryDao } from "../../src/dao/DaoCountry";

describe("Country DAO", () => {
    let idCountry: number;
    // Test 1
    describe("SQL response to Country", () => {
        let countryDao = new CountryDao();
        // Données fictives renvoyés par la base de données.
        const res = {
            name: "test",
            id: 50
        };
        const country: Country = countryDao.sqlToCountry(res);
        it("Name", () => {
            expect(country.getName()).to.be.equal("test");
        });
        it("id", () => {
            expect(country.getId()).to.be.equal(50);
        });
    });

    // Test de l'intégration du DAO avec la base de données.
    describe("DAO integration", () => {

        it("GetCountrys", async () => {
            const countryDao = new CountryDao();
            const country = await countryDao.selectAll();            
            expect(country[0]).to.be.instanceOf(Country);
        });

        it("GetCenterCountrys", async () => {
            const countryDao = new CountryDao();
            const country = await countryDao.selectCenterCountrys();            
            expect(country[0]).to.be.instanceOf(Country);
        });
    });
});

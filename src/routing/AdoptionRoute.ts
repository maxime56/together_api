import { create, update } from "../controller/AdoptionController";
const express = require("express");
const router = express.Router();
const checkToken = require("../middleware/tchekToken");
const centerActionHandler = require("../middleware/centerActionHandler");

router.route("/create/").post(centerActionHandler, checkToken, create); // admin
router.route("/update/").post(centerActionHandler, checkToken, update);  // admin

module.exports = router;
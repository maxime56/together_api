import { Request, Response } from "express";
import { AdminManager } from "../manager/AdminManager";
import { CenterManager } from "../manager/CenterManager";
import { Species } from "../models/Species";

const adminManager = new AdminManager();
const centerManager = new CenterManager();

export const create = async (req: Request, res: Response) => {
    const reqSpecies = req.body.Species;

    const result = await adminManager.createAnSpecies(new Species(reqSpecies.name));
    if (result) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to create vaccines"});
};

export const linkCenterSpecies = async (req: Request, res: Response) => {
    const reqCenterId: any = req.query.CenterId;
    const reqSpeciesId: any = req.query.SpeciesId;

    const result = await adminManager.createAnCenterSpecies(reqCenterId, reqSpeciesId);
    if (result) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to create a linked vaccines"});
};

export const deleteLinkedCenterSpecies = async (req: Request, res: Response) => {
    const reqCenterId: any = req.query.CenterId;
    const reqSpeciesId: any = req.query.SpeciesId;
    const result = await adminManager.deleteCenterSpecies(reqCenterId, reqSpeciesId);
    if (result) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to delete vaccine linked"});
};

export const deleting = async (req: Request, res: Response) => {
    const reqSpeciesId: any = req.query.SpeciesId;

    const result = await adminManager.deleteAnSpecies(new Species("randomFill",reqSpeciesId));
    if (result) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to delete vaccine"});
};

export const update = async (req: Request, res: Response) => {
    const result = await adminManager.updateAnSpecies(new Species(req.body.Species.name, req.body.Species.id));
    if (result) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to update vaccine"});
};

export const getAllSpecies = async (req: Request, res: Response) => {
    const result = await centerManager.getSpecies();
    if (result[0]) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to get all species"});
};
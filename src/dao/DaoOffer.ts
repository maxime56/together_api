import { Offer } from "../models/Offer";
import Database from "../models/Database"

export class OfferDao extends Database {
    public sqlToOffer(res): Offer {
        if (res) {
            return new Offer(
                res.name,
                res.description,
                res.startDate,
                res.endDate,
                res.place,
                res.Center_id,
                res.id,
            );
        }
    }
    /**
     * @param {Number} offerId 
     * @returns {Center}
     */
    public async selectOne(offerId: number): Promise<Offer> {
        // this.getConnection();
        const query = `CALL select_offer(${offerId})`;
        const res = await this.request(query);
        return this.sqlToOffer(JSON.parse(JSON.stringify(res[0]))[0]);
    }
    /**
     * @param {Number} centerId 
     * @returns {Offer}
     */
    public async selectAll(centerId: number) {
        const query = `CALL select_center_offer(${centerId})`;
        const res = await this.request(query);
        const result = [];
        res[0].forEach(element => {
            let i = this.sqlToOffer(JSON.parse(JSON.stringify(element)));
            result.push(i);
        });
        return result;
    }
    /**
     * @param {Number} idCenter 
     * @returns {Offer} Should return all offers that have been made in a center
     */
    public async selectCenterOffers(idCenter: number) {
        const query = `CALL select_center_offer(${idCenter})`;
        const res = await this.request(query);
        const result = [];
        res[0].forEach(element => {
            let i = this.sqlToOffer(JSON.parse(JSON.stringify(element)));
            result.push(i);
        });
        return result;
    }
    /**
     * @param {Offer} offer 
     * @returns {Offer}
     */
    public async create(offer: Offer): Promise<Offer> {
        let y = offer.getStartingDate().toLocaleString("FR").slice(6, 10);
        let m = offer.getStartingDate().toLocaleString("FR").slice(3, 5);
        let d = offer.getStartingDate().toLocaleString("FR").slice(0, 2);

        let y_ending = offer.getEndingDate().toLocaleString("FR").slice(6, 10);
        let m_ending = offer.getEndingDate().toLocaleString("FR").slice(3, 5);
        let d_ending = offer.getEndingDate().toLocaleString("FR").slice(0, 2);

        let query = `INSERT INTO Offer (name, description, startDate, endDate, place, Center_id) VALUES
        ("${offer.getName()}", "${offer.getDescription()}", ("${y}-${m}-${d}"), ("${y_ending}-${m_ending}-${d_ending}"), 
        ${offer.getAvailablePlace()}, ${offer.getCenterId()});`;
        let res = await this.request(query);
        return this.selectOne(res.insertId);
    }
    /**
     * @param {Number} offerId 
     * @returns {Boolean}
     */
    public async delete(offerId: number) {
        const query = `CALL delete_offer(${offerId})`;
        const res = await this.request(query);
        const deleteSuccess = res.affectedRows;
        return deleteSuccess === 1 ? true : false;
    }
    /**
     * @param {Offer} offer 
     * @returns {Offer}
     */
    public async edit(offer: Offer): Promise<Offer> {
        let y = offer.getStartingDate().toLocaleString("FR").slice(6, 10);
        let m = offer.getStartingDate().toLocaleString("FR").slice(3, 5);
        let d = offer.getStartingDate().toLocaleString("FR").slice(0, 2);

        let y_ending = offer.getEndingDate().toLocaleString("FR").slice(6, 10);
        let m_ending = offer.getEndingDate().toLocaleString("FR").slice(3, 5);
        let d_ending = offer.getEndingDate().toLocaleString("FR").slice(0, 2);
        let query = `CALL update_offer("${offer.getName()}", "${offer.getDescription()}", ("${y}-${m}-${d}"), ("${y_ending}-${m_ending}-${d_ending}"), 
        ${offer.getAvailablePlace()}, ${offer.getCenterId()}, ${offer.getId()})`;
        await this.request(query);
        return this.selectOne(offer.getId());
    }
    /**
     * @param {Number} userId 
     * @returns {Offer} Should return all the offer that have been applied by a user 
     */
    public async selectHistoricOffer(userId: number) {
        const query = `CALL select_historic_offer(${userId})`;
        const res = await this.request(query);
        const result = [];
        res[0].forEach(element => {
            let i = this.sqlToOffer(JSON.parse(JSON.stringify(element)));
            result.push(i);
        });
        return result;
    }
    /**
     * @param {Number} centerId 
     * @returns {Boolean} Should delete all the offer that it made by a center and all their pictures
     */
    public async deleteAllOffersByCenter(centerId: number) {
        const query = `CALL delete_center_offers(${centerId})`;
        const res = await this.request(query);
        const deleteSuccess = res.affectedRows;
        return deleteSuccess > 0 ? true : false;
    }
}
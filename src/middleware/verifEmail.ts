
// permet de vérifier le format de l'adresse email
module.exports = async (req, res, next) => {
    const email = req.body.User.email || req.body.Center.email;
    try {
        let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (email.match(regexEmail)) {
            next();
        } else {
            throw "invalid email format";
            
        }
    } catch (e) {
        res.status(400).json({
            Error: "the email does not have the right format"
        });
    }
};  
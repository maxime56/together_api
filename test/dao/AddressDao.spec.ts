import { expect } from "chai";
import { Address } from "../../src/models/Address";
import { AddressDao } from "../../src/dao/DaoAddress";
import { DaoHandler } from "../../src/dao/DaoHandler";

describe("Address DAO", () => {
    let deleteId: number;
    // Test 1
    describe("SQL response to address", () => {
        let addressDao = new AddressDao();

        // Données fictives renvoyés par la base de données.
        const res = {
            id: 1,
            postalCode: "69006",
            streetName: "7 rue rue leclerc",
            Country_id: 3,
            city: "Lyon"
        };

        const address: Address = addressDao.sqlToAddress(res);
        it("Id", () => {
            expect(address.getId()).to.be.equal(1);
        });
        it("Name", () => {
            expect(address.getPostalCode()).to.be.equal("69006");
        });
        it("Date", () => {
            expect(address.getStreetName()).to.be.equal("7 rue rue leclerc");
        });
        it("Descritpion", () => {
            expect(address.getCity()).to.be.equal("Lyon");
        });
    });

    // Test de l'intégration du DAO avec la base de données.
    describe("DAO integration", () => {
        it("New", async () => {
            const newAddress: Address = new Address("5555", "7 rue macron", "Paris", 1);
            const addressDao = new AddressDao();
            const address = await addressDao.create(newAddress);
            // récupération de l'id qui vient d'être créé pour le supprimer
            deleteId = address.getId();
            expect(address).to.be.instanceOf(Address);
            expect(address.getPostalCode()).to.be.equal("5555");
            expect(address.getStreetName()).to.be.equal("7 rue macron");
            expect(address.getCity()).to.be.equal("Paris");
            expect(address.getCountryId()).to.be.equal(1);
        });

        it("selectOne", async () => {
            const addressDao = new AddressDao();
            const address = await addressDao.selectOne(1);
            expect(address).to.be.instanceOf(Address);
        });

        it("Edit", async () => {
            const editedAddress: Address = new Address("66666", "6 rue macron", "Lyon", 12, deleteId);
            const addressDao = new AddressDao();
            const address = await addressDao.edit(editedAddress);
            expect(address).to.be.instanceOf(Address);
            expect(address.getPostalCode()).to.be.equal("66666");
            expect(address.getStreetName()).to.be.equal("6 rue macron");
            expect(address.getCity()).to.be.equal("Lyon");
            expect(address.getCountryId()).to.be.equal(12);
        });

        it("Delete", async () => {
            const addressDao = new AddressDao();
            const deleteSuccess = await addressDao.delete(deleteId);
            expect(deleteSuccess).to.be.true;
        });
    });
});

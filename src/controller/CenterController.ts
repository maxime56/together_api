import { Request, Response } from "express";
import { AdminManager } from "../manager/AdminManager";
import { CenterManager } from "../manager/CenterManager";
import { Center } from "../models/Center";


const centerManager = new CenterManager();
const adminManager = new AdminManager();

export const create = async (req: Request, res: Response) => {
    const reqCenter = req.body.Center;

    const result = await adminManager.createAnCenter(
        new Center(reqCenter.name, reqCenter.email, reqCenter.phoneNumber, reqCenter.isActive, reqCenter.schedule, null, reqCenter.CountryId)
    );
    if (result.Center.getId() > 0) return res.status(200).send(result);
    return res.status(503).send({Error: "Failed to create a center"});
}

export const getCenters = async (req: Request, res: Response) => {
    const offset: any = req.query.offset;
    const limit: any = req.query.limit;
    const CountryId: any = req.query.CountryId;
    const centers = await centerManager.getCentersByCountry(offset, limit, CountryId);
    return res.status(200).send(centers);
}

export const getAllCenters = async (req: Request, res: Response) => {
    const offset: any = req.query.offset;
    const limit: any = req.query.limit;
    const centers = await centerManager.getEveryCenters(offset, limit);
    return res.status(200).send(centers);
}

export const update = async (req: Request, res: Response) => {
    const reqCenter = req.body.Center;

    const result = await adminManager.updateAnCenter(
        new Center(reqCenter.name, reqCenter.email, reqCenter.phoneNumber, reqCenter.isActive, reqCenter.schedule, reqCenter.AddressId, reqCenter.CountryId, reqCenter.id)
    );
    if (result.Center.getId() > 0) return res.status(200).send(result);
    return res.status(503).send({Error: "Failed to update a center"});
}

export const deleting = async (req: Request, res: Response) => {
    const reqCenter = req.body.Center;
    const result = await adminManager.deleteAnCenter(new Center("ddd", "d", "d", false, "dd", reqCenter.AddressId, 1, reqCenter.id));
    if (result) return res.status(200).send({Succes: "The center has been deleted"})
    return res.status(503).send({Error: "Failed to delete a center"});
}

export const getRacesAndSpeciesByCenter = async (req: Request, res: Response) => {
    const reqCenterId:any = req.query.CenterId;
    const result = await centerManager.getSpeciesAndRaceByCenter(reqCenterId);
    if (result) return res.status(200).send(result);
    return res.status(503).send({Error: "Failed to get the races & species"});
}

export const getVaccineByCenter = async (req: Request, res: Response) => {
    const reqCenterId:any = req.query.CenterId;
    const result = await centerManager.getVaccinesByCenter(reqCenterId);
    if (result) return res.status(200).send(result);
    return res.status(503).send({Error: "Failed to get vaccines"});
}

export const getAllRoles = async (req: Request, res: Response) => {
    const idcenter: any = req.query.CenterId;
    const result = await centerManager.getRolesFromCenter(idcenter);
    if (result.Roles[0]) return res.status(200).send(result);
    return res.status(503).send({Error: "Failed to get the roles"});
};

export const getOneCenter = async (req: Request, res: Response) => {
    const idCenter: any = req.query.CenterId;
    const result = await centerManager.getOneCenter(idCenter);
    if (result.Center) return res.status(200).send(result);
    return res.status(503).send({Error: "Failed to create the roles"});
}

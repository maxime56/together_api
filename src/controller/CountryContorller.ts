import { Request, Response } from "express";

import { CenterManager } from "../manager/CenterManager";

const centerManager = new CenterManager();


export const getCountrys = async (req: Request, res: Response) => {
    const result =  await centerManager.getAllCountrys();
    if (result) res.status(200).send(result);
    return res.status(503).send({Error: "Server error"}); 
}

export const getCenterCountrys = async (req: Request, res: Response) => {
    const result =  await centerManager.getCenterCountrys();
    if (result) res.status(200).send(result);
    return res.status(503).send({Error: "Server error"}); 
}
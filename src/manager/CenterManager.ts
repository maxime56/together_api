import { AddressDao } from "../dao/DaoAddress";
import { AdoptionDao } from "../dao/DaoAdoption";
import { AnimalDao } from "../dao/DaoAnimal";
import { CandidatDao } from "../dao/DaoCandidat";
import { CenterDao } from "../dao/DaoCenter";
import { CountryDao } from "../dao/DaoCountry";
import { OfferDao } from "../dao/DaoOffer";
import { DaoPicture } from "../dao/DaoPicture";
import { RaceDao } from "../dao/DaoRace";
import { DaoRole } from "../dao/DaoRole";
import { SpeciesDao } from "../dao/DaoSpecies";
import { UserDao } from "../dao/DaoUser";
import { UserRoleDao } from "../dao/DaoUserRole";
import { VaccineDao } from "../dao/DaoVaccine";
import { VaccinedAnimalDao } from "../dao/DaoVaccinedAnimals";
import { Center } from "../models/Center";

export class CenterManager {
    private CenterDao = new CenterDao();
    private PictureDao = new DaoPicture();
    private RaceDao = new RaceDao();
    private SpeciesDao = new SpeciesDao();
    private OfferDao = new OfferDao();
    private AnimalDao = new AnimalDao();
    private CandidatDao = new CandidatDao();
    private UserRole = new UserRoleDao();
    private VaccinedAnimal = new VaccinedAnimalDao();
    private AdoptionDao = new AdoptionDao();
    private AddressDao = new AddressDao();
    private UserDao = new UserDao();
    private VaccineDao = new VaccineDao();
    private RoleDao = new DaoRole();
    private CountryDao = new CountryDao();

    /**
     * @param {number} offset 
     * @param {number} limit 
     * @param {number} countryId 
     * @returns should return a object with a centerId as index will contain a object with a center, his address and his pictures
     */
    public async getCentersByCountry(offset: number, limit: number, countryId: number) {
        try {
            const centers = await this.CenterDao.selectCenterByCountry(limit, offset, countryId);
            let addressArr = [];
            let picArry = [];
            centers.forEach(center => {
                if (center.AddressId) addressArr.push(this.AddressDao.selectOne(center.AddressId));
                picArry.push(this.PictureDao.select_center_picture(center.id));
            });
            let address = await Promise.all(addressArr);
            let picture = await Promise.all(picArry);
            let filtredPictures = picture.filter(value => Object.keys(value).length !== 0); // je retire les arrays vide
            let t = {};
            for (const id in centers) {
                for (const i in address) {
                    // je veux récuperer les centres dans un objet qui va être sous la forme de leur id en tant qu'index
                    // et dans cet objet il va contenir le centre, l'adresse du centre ainsi que les photos qu'il possède
                    if (centers[id].AddressId == address[i].id) {
                        t = {
                            ...t,
                            [centers[id].id]: {
                                Center: centers[id],
                                Address: address[i]
                            }
                        };
                    }
                }

                for (const idP in filtredPictures) {
                    if (filtredPictures[idP][0].CenterId == centers[id].id) {
                        t = {
                            ...t,
                            [centers[id].id]: {
                                ...t[centers[id].id],
                                Pictures: filtredPictures[idP]
                            }
                        };
                    }
                }
            }
            return { Centers: t };
        } catch {
            return { Centers: undefined };
        }
    }


    /**
 * @param {number} offset 
 * @param {number} limit 
 * @returns should return a object with a centerId as index will contain a object with a center, his address and his pictures
 */
    public async getEveryCenters(offset: number, limit: number) {
        try {
            const centers = await this.CenterDao.selectAll(limit, offset);
            let addressArr = [];
            let picArry = [];

            centers.forEach(center => {
                if (center.AddressId) addressArr.push(this.AddressDao.selectOne(center.AddressId));
                picArry.push(this.PictureDao.select_center_picture(center.id));
            });

            let address = await Promise.all(addressArr);
            let picture = await Promise.all(picArry);
            let filtredPictures = picture.filter(value => Object.keys(value).length !== 0); // je retire les arrays vide
            let t = {};
            for (const id in centers) {
                for (const i in address) {
                    // je veux récuperer les centres dans un objet qui va être sous la forme de leur id en tant qu'index
                    // et dans cet objet il va contenir le centre, l'adresse du centre ainsi que les photos qu'il possède
                    if (centers[id].AddressId == address[i].id) {
                        t = {
                            ...t,
                            [centers[id].id]: {
                                Center: centers[id],
                                Address: address[i]
                            }
                        };
                    }
                }

                for (const idP in filtredPictures) {
                    if (filtredPictures[idP][0].CenterId == centers[id].id) {
                        t = {
                            ...t,
                            [centers[id].id]: {
                                ...t[centers[id].id],
                                Pictures: filtredPictures[idP]
                            }
                        };
                    }
                }
            }
            return { Centers: t };
        } catch {
            return { Centers: undefined };
        }
    }

    /**
     * @param {number} centerId 
     * @returns return a object with the center and his pictures and the address
     */
    public async getOneCenter(centerId: number) {
        try {
            const center = await this.CenterDao.selectOne(centerId);
            const address = await this.AddressDao.selectOne(center.getAddressId());
            const pictures = await this.PictureDao.select_center_picture(center.getId());
            return {Center: center, Address: address, Pictures: pictures};
        } catch (error) {
            return { Center: undefined };
        }
    }

    /**
     * @param {number} idCenter 
     * @param {number} offset 
     * @param {null} limit 
     * @returns {Animal, Picture} Should return a object with a AnimalId as index and a object that contain a Animal and his pictures
     */
    public async getAnimalsByCenter(idCenter, offset, limit) {
        try {
            const animals = await this.AnimalDao.getAnimalsByCenter(idCenter, limit, offset);
            let picArr = [];
            let o = {};
            animals.forEach(animal => {
                picArr.push(this.PictureDao.select_animal_picture(animal.id));
            });
            let picture = await Promise.all(picArr);
            let filtredPictures = picture.filter(value => Object.keys(value).length !== 0);
            for (const id in animals) {
                o = {
                    ...o,
                    [animals[id].id]: {
                        Animal: animals[id]
                    }
                };
                for (const idP in filtredPictures) {
                    if (animals[id].id == filtredPictures[idP][0].AnimalId) {
                        o = {
                            ...o,
                            [animals[id].id]: {
                                ...o[animals[id].id],
                                Pictures: filtredPictures[idP]
                            }
                        };
                    }
                }
            }
            return { Animals: o };
        } catch {
            return { Animals: undefined };
        }
    }


    /**
     * @param {number} centerId 
     * @returns {Picture, Offer} Should return a object with a offer Id as index and inside this object you should have the offer information and the pictures of this offer
     */
    public async getCenterOffers(centerId: number,) {
        try {
            const offers = await this.OfferDao.selectCenterOffers(centerId);
            let picArr = [];
            let o = {};
            offers.forEach(offer => {
                picArr.push(this.PictureDao.select_offer_picture(offer.id));
            });
            let picture = await Promise.all(picArr);
            let filtredPictures = picture.filter(value => Object.keys(value).length !== 0);
            for (const id in offers) {
                o = {
                    ...o,
                    [offers[id].id]: {
                        Offer: offers[id]
                    }
                };
                for (const idP in filtredPictures) {
                    if (offers[id].id == filtredPictures[idP][0].OfferId) {
                        o = {
                            ...o,
                            [offers[id].id]: {
                                ...o[offers[id].id],
                                Pictures: filtredPictures[idP]
                            }
                        };
                    }
                }
            }
            return { Offers: o };
        } catch {
            return { Offers: undefined };
        }
    }


    /**
     * @param {number} idCenter 
     * @returns {Adoption, User} Should return with a object with a userId as index and a another object with the user and the adoption that it made been by the user
     */
    public async getAdoptionByCenter(idCenter: number) {
        try {
            const adoptions = await this.AdoptionDao.getAdoptionByCenter(idCenter);
            let usersArr = [];
            let t = {};
            adoptions.forEach(adoption => {
                usersArr.push(this.UserDao.selectOne(adoption.UserId));
            });
            let users = await Promise.all(usersArr);
            for (const id in adoptions) {
                t = {
                    ...t,
                    [adoptions[id].id]: {
                        Adoption: adoptions[id]
                    }
                };
                for (const idU in users) {
                    if (adoptions[id].UserId == users[idU].id) {
                        delete users[idU].password;
                        t = {
                            ...t,
                            [adoptions[id].id]: {
                                ...t[users[id].id],
                                User: users[idU]
                            }
                        };
                    }
                }
            }
            return { Adoptions: t };
        } catch {
            return { Adoptions: undefined };
        }
    }


    /**
     * @param {number} centerId 
     * @returns {Race, Species} Should return a object with a array of all the races and the Species that have made been in a center
     */
    public async getSpeciesAndRaceByCenter(centerId: number) {
        try {
            const races = await this.RaceDao.select_all_races_by_center(centerId);
            const species = await this.SpeciesDao.select_all_species_by_center(centerId);
            return { Races: races, Species: species };
        } catch {
            return { Races: undefined, Species: undefined };
        }
    }


    /**
     * @param {Number} idCenter 
     * @returns {Array<Vaccine>} Should return a array of Vaccines that have made been in a center
     */
    public async getVaccinesByCenter(idCenter: number) {
        try {
            const vaccines = await this.VaccineDao.selectVaccineByCenter(idCenter);
            return { Vaccines: vaccines };
        } catch {
            return { Vaccines: undefined };
        }
    }


    /**
     * @param {Number} offerId 
     * @returns {User,Picture,Address} Should return a object with a user his picture and his address, each index of the object is the id of the user
     */
    public async getStaffByCenter(idCenter: number) {
        try {
            const userRoles = await this.UserRole.select_user_with_role_by_center(idCenter);
            let usersArr = [];
            let picturesArr = [];
            let address = [];

            userRoles.forEach(userRole => {
                usersArr.push(this.UserDao.selectOne(userRole.User_id));
                picturesArr.push(this.PictureDao.select_user_picture(userRole.User_id));
            });

            let users = await Promise.all(usersArr);
            let picture = await Promise.all(picturesArr);
            let filtredPictures = picture.filter(value => Object.keys(value).length !== 0);

            users.forEach(user => {
                if (user.addressId == null || user.addressId == undefined) return;
                address.push(this.AddressDao.selectOne(user.addressId));
            });
            let addressResp = await Promise.all(address);

            let t = {}; // l'object qui va contenir mes utilisateurs et leur rôles dans le centre, ainsi que leur adresse
            for (const i in users) {
                // supprimation du password pour une question de sécurité
                delete users[i].password;
                t = {
                    ...t,
                    [users[i].id]: { // l'index de nos utilisateurs est l'id de ces derniers
                        User: users[i]
                    }
                };
                for (const k in addressResp) {
                    if (addressResp[k].id == users[i].addressId) {
                        t = {
                            ...t,
                            [users[i].id]: { // l'index de nos utilisateurs est l'id de ces derniers
                                User: users[i],
                                Address: addressResp[k]
                            },
                        }
                    }
                }
            }
            // la boucle va insérer les UserRoles pour chaque user
            for (const id in userRoles) {
                if (userRoles[id].User_id == t[userRoles[id].User_id].User.id) { // on cherche à faire matcher les utilisateurs et les rôles
                    t[t[userRoles[id].User_id].User.id].UserRoles = {
                        ...t[t[userRoles[id].User_id].User.id].UserRoles,
                        [userRoles[id].Role_id]: userRoles[id]
                    };
                }
            }
            // "injection" des photos d'utilisateur dans l'object
            for (const idP in filtredPictures) {
                if (t[filtredPictures[idP][0].UserId].User.id == filtredPictures[idP][0].UserId) {
                    t = {
                        ...t,
                        [t[filtredPictures[idP][0].UserId].User.id]: {
                            ...t[t[filtredPictures[idP][0].UserId].User.id],
                            Pictures: filtredPictures[idP]
                        }
                    };
                }
            }
            return { Users: t };
        } catch {
            return { Users: undefined };
        }
    }


    /**
     * @param centerId 
     * @returns {VaccinedAnimal} Should return all animals that made vaccined in a center (we only need to turn over the link table as the front is already fetching the animals)
     */
    public async getVaccinedAnimalsByCenter(centerId: number) {
        try {
            const VaccinedAnimals = await this.VaccinedAnimal.selectAnimalsVaccinedByCenter(centerId);
            return { VaccinedAnimals: VaccinedAnimals };
        } catch {
            return { VaccinedAnimals: undefined };
        }
    }


    /**
     * @param {Number} offerId 
     * @returns {User,Picture,Address} Should return a object with a user his picture and his address, each index of the object is the id of the user
     */
    public async getCandidatsFromOffer(offerId: number) {
        try {
            const Candidats = await this.CandidatDao.selectCandidatByOffer(offerId);
            let usersArr = [];
            let picArr = [];
            let t = {};
            let address = [];
            Candidats.forEach(candidat => {
                usersArr.push(this.UserDao.selectOne(candidat.User_id));
                picArr.push(this.PictureDao.select_user_picture(candidat.User_id));
            });
            let users = await Promise.all(usersArr);
            let picture = await Promise.all(picArr);
            let filtredPictures = picture.filter(value => Object.keys(value).length !== 0);

            users.forEach(user => {
                if (user.addressId == null || user.addressId == undefined) return;
                address.push(this.AddressDao.selectOne(user.addressId));
            });
            let addressResp = await Promise.all(address);

            for (const i in users) {
                // supprimation du password pour une question de sécurité
                delete users[i].password;
                t = {
                    ...t,
                    [users[i].id]: { // l'index de nos utilisateurs est l'id de ces derniers
                        User: users[i]
                    },
                };
                // "injection" des adresses des candidats
                for (const k in addressResp) {
                    if (addressResp[k].id == users[i].addressId) {
                        t = {
                            ...t,
                            [users[i].id]: { // l'index de nos utilisateurs est l'id de ces derniers
                                User: users[i],
                                Address: addressResp[k]
                            },
                        }
                    }
                }
            }
            // "injection" des candiats dans l'object des utilisateurs
            for (const id in Candidats) {
                if (Candidats[id].User_id == t[Candidats[id].User_id].User.id) {
                    t[t[Candidats[id].User_id].User.id].Candidats = {
                        [Candidats[id].Offer_id]: Candidats[id]
                    };
                }
            }
            // "injection" des photos d'utilisateur dans l'object
            for (const idP in filtredPictures) {
                if (t[filtredPictures[idP][0].UserId].User.id == filtredPictures[idP][0].UserId) {
                    t = {
                        ...t,
                        [t[filtredPictures[idP][0].UserId].User.id]: {
                            ...t[t[filtredPictures[idP][0].UserId].User.id],
                            Pictures: filtredPictures[idP]
                        }
                    };
                }
            }

            return { Candidats: t };
        } catch {
            return { Candidats: undefined };
        }
    }


    /**
     * @param centerId 
     * @returns {Role} Should return a array of Role that have made been in a center
     */
    public async getRolesFromCenter(centerId: number) {
        try {
            const Roles = await this.RoleDao.select_roles_by_center(centerId);
            return { Roles: Roles };
        } catch {
            return { Roles: undefined };
        }
    }


    /**
    * @returns {Race} Should return a array of all the races that is available in the database (not linked to a center)
    */
    public async getRaces() {
        const Races = await this.RaceDao.select_all_races();
        return { Races: Races };
    }


    /**
    * @returns {Species} Should return a array of all the species that is available in the database (not linked to a center)
    */
    public async getSpecies() {
        const species = await this.SpeciesDao.select_all_species();
        return { Species: species };
    }


    /**
    * @returns {Species} Should return a array of all the species that is available in the database (not linked to a center)
    */
    public async getVaccines() {
        try {
            const vaccines = await this.VaccineDao.selectVaccines();
            return { Vaccines: vaccines };
        } catch {
            return { Vaccines: undefined };
        }
    }


    /**
    * @returns {Role} Should return a array of all the species that is available in the database (not linked to a center)
    */
    public async getRoles() {
        try {
            const roles = await this.RoleDao.getAllRoles();
            return { Roles: roles };
        } catch {
            return { Roles: undefined };
        }
    }


    /**
     * @returns {Country}
     */
    public async getAllCountrys() {
        try {
            return this.CountryDao.selectAll();
        } catch (error) {
            return { Countrys: undefined }
        }
    }

    /**
    * @returns {Country}
    */
    public async getCenterCountrys() {
        try {
            let countryArr = await this.CountryDao.selectCenterCountrys();
            const uniqueIds = new Set();
            const unique = countryArr.filter(element => {
                const isDuplicate = uniqueIds.has(element.id);

                uniqueIds.add(element.id);

                if (!isDuplicate) {
                    return true;
                }

                return false;
            });

            return unique;
        } catch (error) {
            return { Countrys: undefined }
        }
    }
}
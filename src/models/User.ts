export class User {
    private id: number;
    private email: string;
    private firstName: string;
    private lastName: string;
    private phoneNumber: string;
    private password: string;
    private addressId: number;
    private lastConnexion: Date;
    public constructor(email: string, firstName: string, lastName: string, phoneNumber: string, lastConnexion:Date ,password?: string, addressId?: number, id?: number) {
        this.email = email;
        this.firstName = firstName;
        this.lastName = lastName;
        this.phoneNumber = phoneNumber;
        this.password = password;
        this.addressId = addressId
        this.id = id;
        this.lastConnexion = lastConnexion;
    }
    public getEmail(): string {
        return this.email;
    }
    public setEmail(email: string): void {
        this.email = email;
    }
    public getFirstName(): string {
        return this.firstName;
    }
    public setFirstName(firstName: string): void {
        this.firstName = firstName;
    }
    public getLastName(): string {
        return this.lastName;
    }
    public setLastName(lastName: string): void {
        this.lastName = lastName;
    }
    public getPhoneNumber(): string {
        return this.phoneNumber;
    }
    public setPhoneNumber(phoneNumber: string): void {
        this.phoneNumber = phoneNumber;
    }
    public getPassword(): string {
        return this.password;
    }
    public setPassword(password: string): void {
        this.password = password;
    }
    public getAddressId(): number {
        return this.addressId;
    }
    public setAddressId(addressId: number): void {
        this.addressId = addressId;
    }
    public getId(): number {
        return this.id;
    }
    public setLastConnexion(lastConnexion: Date): void {
        this.lastConnexion = lastConnexion;
    }
    public getLastConnexion(): Date {
        return this.lastConnexion;
    }
}

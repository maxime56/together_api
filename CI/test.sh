#!/bin/bash

#le script va réinitialiser notre bdd avec un dataset qui se trouve dans le dossier test

# path du .env
ENV='../.env';
dataset='../test/dataset/truncateTable.sql';
# vérification de l'environnement, j'effectue seulement la réinitialisation en local
while IFS= read -r line; do
    echo $line;
    if [[ "$line" = *ENV=local* ]]; then
        mysql -h 127.0.0.1 -u simplon --password=1234 < $dataset;
        echo "wsh";
    fi
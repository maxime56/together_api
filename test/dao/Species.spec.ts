import { expect } from "chai";
import { Species } from "../../src/models/Species";
import { SpeciesDao } from "../../src/dao/DaoSpecies";

describe("Species DAO", () => {
    let idSpecie: number;
    // Test 1
    describe("SQL response to Species", () => {
        let speciesDao = new SpeciesDao();
        // Données fictives renvoyés par la base de données.
        const res = {
            name: "test",
            id: 50
        };
        const species: Species = speciesDao.sqlToSpecies(res);
        it("Name", () => {
            expect(species.getName()).to.be.equal("test");
        });
        it("id", () => {
            expect(species.getId()).to.be.equal(50);
        });
    });

    // Test de l'intégration du DAO avec la base de données.
    describe("DAO integration", () => {
        it("New", async () => {
            const newSpecies: Species = new Species("TDDUS");
            const speciesDao = new SpeciesDao();
            const species = await speciesDao.create(newSpecies);
            expect(species).to.be.instanceOf(Species);            
            expect(species.getName()).to.be.equal("TDDUS");
            // récupérer le dernier id pour l'edit et le delete par la suite
            idSpecie = species.getId();
        });
        it("Get", async () => {
            const speciesDao = new SpeciesDao();
            const species = await speciesDao.selectOne(1);
            expect(species).to.be.instanceOf(Species);
        });
        it("select_all_species_by_center", async () => {
            const speciesDao = new SpeciesDao();
            const species = await speciesDao.select_all_species_by_center(1);
            expect(species[0]).to.be.instanceOf(Species);
        });
        it("Edit", async () => {
            const editedspecies: Species = new Species("EDDITDDUS", idSpecie);
            const speciesDao = new SpeciesDao();
            const species = await speciesDao.edit(editedspecies);
            expect(species).to.be.instanceOf(Species);
            expect(species.getName()).to.be.equal("EDDITDDUS");
        });
        it("Delete", async () => {
            const speciesDao = new SpeciesDao();
            const deleteSuccess = await speciesDao.delete(idSpecie);
            expect(deleteSuccess).to.be.true;
        });
        it("deleteAllSpeciesByCenter", async () => {
            const speciesDao = new SpeciesDao();
            const deleteSuccess = await speciesDao.deleteAllSpeciesByCenter(5);
            expect(deleteSuccess).to.be.true;
        });
        it("linkAnSpeciesInAnCenter", async () => {
            const speciesDao = new SpeciesDao();
            const deleteSuccess = await speciesDao.linkAnSpeciesInAnCenter(3, 2);
            expect(deleteSuccess).to.be.true;
        });
        it("deleteLinkedSpeciesInAnCenter", async () => {
            const speciesDao = new SpeciesDao();
            const deleteSuccess = await speciesDao.deleteLinkedSpeciesInAnCenter(3, 2);
            expect(deleteSuccess).to.be.true;
        });
        it("select_all_species", async () => {
            const speciesDao = new SpeciesDao();
            const result = await speciesDao.select_all_species();
            expect(result[0]).to.be.instanceOf(Species);
        });
    });
});

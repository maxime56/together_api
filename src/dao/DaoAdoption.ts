import { Adoption } from "../models/Adoption";
import Database from "../models/Database";

export class AdoptionDao extends Database {
    public sqlToAdoption(res): Adoption {
        if (res) {
            return new Adoption(
                res.User_id,
                res.Animal_id,
                res.Animal_Center_id,
                res.isActive,
                res.adoptionDate,
                res.id
            );
        }
    }
    /**
     * @param {Number} id 
     * @returns {Adoption}
     */
    public async selectOne(id: number): Promise<Adoption> {
        // this.getConnection();
        const query = `CALL select_adoption(${id})`;
        const res = await this.request(query);
        return this.sqlToAdoption(JSON.parse(JSON.stringify(res[0]))[0]);
    }
    /**
     * @param {Number} centerId 
     * @returns {Adoption} Should return adoptions that have been made in a center
     */
    public async getAdoptionByCenter(centerId: number) {
        const query = `CALL select_adoption_by_center(${centerId})`;
        const res = await this.request(query);
        const result = [];
        res[0].forEach(element => {
            let i = this.sqlToAdoption(JSON.parse(JSON.stringify(element)));
            result.push(i);
        });
        return result;
    }
    /**
     * @param {Number} userId 
     * @returns {Adoption} Should return adoptions that have been made by a user
     */
    public async getAdoptionByUser(userId: number) {
        const query = `CALL select_adoption_by_user(${userId})`;
        const res = await this.request(query);
        const result = [];
        res[0].forEach(element => {
            let i = this.sqlToAdoption(JSON.parse(JSON.stringify(element)));
            result.push(i);
        });
        return result;
    }
    /**
     * @param {Adoption} adoption 
     * @returns {Adoption}
     */
    public async create(adoption: Adoption): Promise<Adoption> {
        // parsing de la date pour le mettre au bon format
        let y = adoption.getAdoptionDate().toLocaleString("FR").slice(6, 10);
        let m = adoption.getAdoptionDate().toLocaleString("FR").slice(3, 5);
        let d = adoption.getAdoptionDate().toLocaleString("FR").slice(0, 2);

        let query = `INSERT INTO Adoption (User_id, Animal_id, Animal_Center_id, isActive, adoptionDate) VALUES (${adoption.getUserId()}, ${adoption.getAnimalId()}, ${adoption.getAnimalCenterId()}, ${adoption.getIsActive()}, ("${y}-${m}-${d}") );`;
        let res = await this.request(query);
        return this.selectOne(res.insertId);
    }
    /**
     * @param {Adoption} adoption 
     * @returns {Adoption}
     */
    public async update(adoption: Adoption): Promise<Adoption> {
        // parsing de la date pour le mettre au bon format
        let y = adoption.getAdoptionDate().toLocaleString("FR").slice(6, 10);
        let m = adoption.getAdoptionDate().toLocaleString("FR").slice(3, 5);
        let d = adoption.getAdoptionDate().toLocaleString("FR").slice(0, 2);
        let query = `CALL update_adoption(${adoption.getUserId()}, ${adoption.getAnimalId()}, ${adoption.getAnimalCenterId()}, ${adoption.getIsActive()}, ("${y}-${m}-${d}"), ${adoption.getId()})`;
        await this.request(query);

        return this.selectOne(adoption.getId());
    }
    // public async delete(AdoptionId: number) {
    //     const query = `CALL delete_adoption(${AdoptionId})`;
    //     const res = await this.request(query);
    //     const deleteSuccess = res.affectedRows;
    //     return deleteSuccess === 1 ? true : false;
    // }

    /**
     * @param {Number} animalId 
     * @returns {Boolean} should return a boolean & delete all the adoptions that it been made from a animal id
     */
    public async deleteAnimalAdoptions(animalId: number) {
        const query = `CALL delete_animal_adoption(${animalId})`;
        const res = await this.request(query);
        const deleteSuccess = res.affectedRows;
        return deleteSuccess > 0 ? true : false;
    }
    /**
     * @param {Number} centerId 
     * @returns {Boolean} should return a boolean & delete all the adoptions that it been made from a center id
     */
    public async deleteAllAdoptions(centerId: number) {
        const query = `CALL delete_center_adoptions(${centerId})`;
        const res = await this.request(query);
        const deleteSuccess = res.affectedRows;
        return deleteSuccess > 0 ? true : false;
    }
}
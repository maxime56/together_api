import { create, deletelinkedAnRole, deleting, getRoles, linkedAnRole, update, updatelinkedAnRole } from "../controller/RoleController";

const express = require("express");
const router = express.Router();
const checkToken = require("../middleware/tchekToken");
const centerActionHandler = require("../middleware/centerActionHandler");

router.route("/create/").post(centerActionHandler, checkToken, create); // admin
router.route("/linkedAnRole/").post(centerActionHandler, checkToken, linkedAnRole); // admin
router.route("/updatelinkedAnRole/").post(centerActionHandler, checkToken, updatelinkedAnRole); // admin
router.route("/deletelinkedAnRole/").delete(centerActionHandler, checkToken, deletelinkedAnRole); // admin
router.route("/delete/").delete(centerActionHandler, checkToken, deleting); // admin
router.route("/update/").post(centerActionHandler, checkToken, update); // admin
router.route("/getRoles/").get(centerActionHandler, checkToken, getRoles); // admin


module.exports = router;
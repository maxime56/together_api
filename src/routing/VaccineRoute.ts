import { create, deleteLinkedVaccinedAnimal, deleting, getAllVaccines, linkVaccinedAnimal, update, updateLinkedVaccinedAnimal } from "../controller/VaccineController";

const express = require("express");
const router = express.Router();
const checkToken = require("../middleware/tchekToken");
const centerActionHandler = require("../middleware/centerActionHandler");

router.route("/create/").post(centerActionHandler, checkToken, create); // admin
router.route("/linkVaccinedAnimal/").post(centerActionHandler, checkToken, linkVaccinedAnimal); // admin
router.route("/updateLinkedVaccinedAnimal/").post(centerActionHandler, checkToken, updateLinkedVaccinedAnimal); // admin
router.route("/deleteLinkedVaccinedAnimal/").delete(centerActionHandler, checkToken, deleteLinkedVaccinedAnimal); // admin
router.route("/delete/").delete(centerActionHandler, checkToken, deleting); // admin
router.route("/update/").post(centerActionHandler, checkToken, update); // admin
router.route("/getVaccines/").get(centerActionHandler, checkToken, getAllVaccines); // admin


module.exports = router;
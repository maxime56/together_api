import { Request, Response } from "express";
import { AdminManager } from "../manager/AdminManager";
import { Adoption } from "../models/Adoption";

const centerManager = new AdminManager();

export const create = async (req: Request, res: Response) => {
    const reqAdoption = req.body.Adoption;
    const reqCenter = req.body.Center;
    const result = await centerManager.createAnAdoption(
        new Adoption(reqAdoption.UserId, reqAdoption.AnimalId, reqAdoption.AnimalCenterId, reqAdoption.isActive, reqAdoption.adoptionDate),
        reqCenter.id
    );
    if (result) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to create a adoption"});
};

export const update = async (req: Request, res: Response) => {
    const reqAdoption = req.body.Adoption;
    const reqCenter = req.body.Center;
    const result = await centerManager.updateAnAdoption(
        new Adoption(reqAdoption.UserId, reqAdoption.AnimalId, reqAdoption.AnimalCenterId, reqAdoption.isActive, reqAdoption.adoptionDate, reqAdoption.id),
        reqCenter.id
    );
    if (result) return res.status(200).send(result);
    return res.status(400).send({Error: "Failed to update a adoption"});
};




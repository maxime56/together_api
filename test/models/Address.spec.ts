import { expect } from "chai";
import { Address } from "../../src/models/Address";


describe('Address Models Testing Getter & Setter', () => {
    let address: Address;
    beforeEach(() => {
        address = new Address("69008", "6 rue du chene", "Lyon", 1,1);
    });
    describe('Getter Setter Id', () => {
        it('getIdEvent', () => {
            expect(address.getId()).to.be.equal(1);
        });
        it("setIdEvent", () => {
            address.setId(2)
            expect(address.getId()).to.be.equal(2);
        });
    });
    describe('Getter Setter PostalCode', () => {
        it('getIdEvent', () => {
            expect(address.getPostalCode()).to.be.equal("69008");
        });
        it("setIdEvent", () => {
            address.setPostalCode("69007")
            expect(address.getPostalCode()).to.be.equal("69007");
        });
    });
    describe('Getter Setter StreetName', () => {
        it('getIdEvent', () => {
            expect(address.getStreetName()).to.be.equal("6 rue du chene");
        });
        it("setIdEvent", () => {
            address.setStreetName("7 rue du chene");
            expect(address.getStreetName()).to.be.equal("7 rue du chene");
        });
    });
    describe('Getter Setter CityName', () => {
        it('getIdEvent', () => {
            expect(address.getCity()).to.be.equal("Lyon");
        });
        it("setIdEvent", () => {
            address.setCity("Paris")
            expect(address.getCity()).to.be.equal("Paris");
        });
    });
    describe('Getter Setter Country', () => {
        it('getCountry', () => {
            expect(address.getCountryId()).to.be.equal(1);
        });
        it("setCity", () => {
            address.setCountryId(2)
            expect(address.getCountryId()).to.be.equal(2);
        });
    });
});


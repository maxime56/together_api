export class Offer {
    private id: number;
    private name: string;
    private description: string;
    private startingDate: Date;
    private endingDate: Date;
    private availablePlace: number;
    private CenterId: number;
    constructor(name: string, description: string, startingDate: Date, endingDate: Date, availablePlace: number, CenterId: number, id?: number) {
        this.id = id;
        this.name = name;
        this.description = description;
        this.startingDate = startingDate;
        this.endingDate = endingDate;
        this.availablePlace = availablePlace;
        this.CenterId = CenterId;
    }
    public getId(): number {
        return this.id;
    }
    public setId(id: number): void {
        this.id = id;
    }
    public getName(): string {
        return this.name;
    }
    public setName(name: string): void {
        this.name = name;
    }
    public getDescription(): string {
        return this.description;
    }
    public setDescription(description: string): void {
        this.description = description;
    }
    public getStartingDate(): Date {
        return this.startingDate;
    }
    public setStartingDate(startingDate: Date): void {
        this.startingDate = startingDate;
    }
    public getEndingDate(): Date {
        return this.endingDate;
    }
    public setEndingDate(endingDate: Date): void {
        this.endingDate = endingDate;
    }
    public getAvailablePlace(): number {
        return this.availablePlace;
    }
    public setAvailablePlace(availablePlace: number): void {
        this.availablePlace = availablePlace;
    }
    public getCenterId(): number {
        return this.CenterId;
    }
    public setCenterId(CenterId: number): void {
        this.CenterId = CenterId;
    }
}
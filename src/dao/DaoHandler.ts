import * as dotenv from "dotenv"
// read .env file before everything else
dotenv.config();
const mysql = require('mysql2/promise');

export class DaoHandler {
    /** See documentation from original answer */
    
    public async transaction(queries) {
        console.log("test -> ", process.env);
        const connection = await mysql.createConnection({
            host: process.env.DB_HOST,
            port: process.env.DB_PORT,
            user: process.env.DB_USER,
            password: process.env.DB_PASSWORD,
            database: process.env.DB_NAME
        });
        try {
            await connection.beginTransaction();
            const queryPromises = [];
            queryPromises.forEach( () => {
                // queryPromises.push("START TRANSACTION;");
                queryPromises.push(queries);
            });
            const results = await Promise.all(queryPromises);
            await connection.commit();
            await connection.end();
            return results;
        } catch (err) {
            await connection.query('ROLLBACK');
            await connection.end();
        }
    }
    public async rollback() {
        const connection = await mysql.createConnection({
            host: process.env.DB_HOST,
            port: process.env.DB_PORT,
            user: process.env.DB_USER,
            password: process.env.DB_PASSWORD,
            database: process.env.DB_NAME
        });
        await connection.query('ROLLBACK');
        await connection.end();
    }
}
import { VaccinedAnimal } from "../models/VaccinedAnimal";
import Database from "../models/Database";

export class VaccinedAnimalDao extends Database {
    public sqlToVaccinedAnimal(res): VaccinedAnimal {
        if (res) {
            return new VaccinedAnimal(
                res.Vaccine_id,
                res.Animal_id,
                res.vaccinationDate,
                res.nextVaccination
            );
        }
    }
    /**
     * @param {VaccinedAnimal} A 
     * @returns {VaccinedAnimal}
     */
    public async create(A: VaccinedAnimal): Promise<VaccinedAnimal> {
        let y = A.getVaccinedDate().toLocaleString("FR").slice(6, 10);
        let m = A.getVaccinedDate().toLocaleString("FR").slice(3, 5);
        let d = A.getVaccinedDate().toLocaleString("FR").slice(0, 2);

        let ye = A.getNextVaccination().toLocaleString("FR").slice(6, 10);
        let mo = A.getNextVaccination().toLocaleString("FR").slice(3, 5);
        let da = A.getNextVaccination().toLocaleString("FR").slice(0, 2);

        let query = `INSERT INTO Vaccine_animal (Vaccine_id, Animal_id, vaccinationDate, nextVaccination) VALUES
        (${A.getVaccineId()}, ${A.getAnimalId()}, ("${y}-${m}-${d}"), ("${ye}-${mo}-${da}") )`;
        await this.request(query);
        return this.selectOne(A.getVaccineId(), A.getAnimalId());
    }
    /**
     * @param {VaccinedAnimal} A 
     * @returns {VaccinedAnimal}
     */
    public async edit(A: VaccinedAnimal): Promise<VaccinedAnimal> {
        let y = A.getVaccinedDate().toLocaleString("FR").slice(6, 10);
        let m = A.getVaccinedDate().toLocaleString("FR").slice(3, 5);
        let d = A.getVaccinedDate().toLocaleString("FR").slice(0, 2);

        let ye = A.getNextVaccination().toLocaleString("FR").slice(6, 10);
        let mo = A.getNextVaccination().toLocaleString("FR").slice(3, 5);
        let da = A.getNextVaccination().toLocaleString("FR").slice(0, 2);

        let query = `CALL update_vaccined_animal(${A.getVaccineId()}, ${A.getAnimalId()}, ("${y}-${m}-${d}"), ("${ye}-${mo}-${da}") )`;

        await this.request(query);
        return this.selectOne(A.getVaccineId(), A.getAnimalId());
    }
    /**
     * 
     * @param {Number} vaccineId 
     * @param {Number} animalId 
     * @returns {VaccinedAnimal}
     */
    public async selectOne(vaccineId: number, animalId: number): Promise<VaccinedAnimal> {
        const query = `CALL select_animal_vaccined(${vaccineId}, ${animalId})`;
        const res = await this.request(query);
        return this.sqlToVaccinedAnimal(JSON.parse(JSON.stringify(res[0]))[0]);
    }
    /**
     * 
     * @param {Number} centerId 
     * @returns should return all animals that made vaccined in a center
     */
    public async selectAnimalsVaccinedByCenter(centerId: number) {
        const query = `CALL select_animals_vaccined_by_center(${centerId})`;
        const res = await this.request(query);
        const result = [];
        res[0].forEach(element => {
            let i = this.sqlToVaccinedAnimal(JSON.parse(JSON.stringify(element)));
            result.push(i);
        });
        return result;
    }
    /**
     * @param {Number} vaccineId 
     * @param {number} animalId 
     * @returns {Boolean} should delete the currect vaccined animal information with the id of the vaccine and the animal id
     */
    public async delete(vaccineId: number, animalId: number) {
        const query = `CALL delete_animal_vaccined(${vaccineId}, ${animalId})`;
        const res = await this.request(query);
        const deleteSuccess = res.affectedRows;
        return deleteSuccess === 1 ? true : false;
    }
    /**
     * @param {number} animalId 
     * @returns {Boolean} should remove vaccinations from an animal
     */
    public async deleteAllVaccinations(animalId: number) {
        const query = `CALL delete_animal_vaccines(${animalId})`;
        const res = await this.request(query);
        const deleteSuccess = res.affectedRows;
        return deleteSuccess > 0 ? true : false;
    }
}



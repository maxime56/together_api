const express = require("express");
const router = express.Router();
const CenterRouting = require("./CenterRoute");
const UserRouting = require("./UserRoute");
const AddressRouting = require("./AddressRoute");
const AdoptionRouting = require("./AdoptionRoute");
const AnimalRouting = require("./AnimalRoute");
const CandidatRouting = require("./CandidatRoute");
const RaceRouting = require("./RaceRoute");
const SpeciesRouting = require("./SpeciesRoute");
const PictureRouting = require("./PictureRoute");
const VaccineRouting = require("./VaccineRoute");
const RoleRouting = require("./RoleRoute");
const OfferRouting = require("./OfferRoute");
const CountryRouting = require("./CountryRoute");

router.use("/center/", CenterRouting);
router.use("/user/", UserRouting);
router.use("/address/", AddressRouting);
router.use("/adoption/", AdoptionRouting);
router.use("/animal/", AnimalRouting);
router.use("/candidat/", CandidatRouting);
router.use("/race/", RaceRouting);
router.use("/species/", SpeciesRouting);
router.use("/picture/", PictureRouting);
router.use("/vaccine/", VaccineRouting);
router.use("/offer/", OfferRouting);
router.use("/role/", RoleRouting); // admin
router.use("/country/", CountryRouting);

module.exports = router;